#include <stdio.h>
#include <stdlib.h>
//#include "labyrinth.c"
#include "part.c"
//#include "tas.c"
#include "lchbis.c"
#include "lch.c"
#include "graph_liste.c"
#include <time.h>

#define N 10

int main(int argc, char **argv) {

    (void)argc;
    (void)argv;

    /*
    graph_liste_t * graph = init_graph_liste_alea(N,0.3);
    part_t * maPart = kruskal(graph,"mon_kruskal",1);
    afficher_partition(maPart);
    free_part(maPart);
    afficheliste_bis(graph -> liste);
    dot_graph_liste(graph, "mon_dot",-1,1);
    dot_composante_connexe_graph_liste(graph,1);
    */

    // printf("ok2\n");
    // SDL_version nb;
    // SDL_VERSION(&nb);

    // printf("Version de la SDL : %d.%d.%d\n", nb.major, nb.minor, nb.patch);
    /*
    graph_liste_t * monLaby = init_laby_liste(5,4);
    dot_graph_liste(monLaby,"mon_test",-1,1);
    part_t * maPart2 =  kruskal(monLaby,"test_kru",1);
    */

    graph_liste_t * graph = init_laby_liste(5,3);
    kruskal(graph,"mon_kruskal",1);
    printf("ok\n");
    //afficheliste_bis(graph ->liste);
    graph -> liste = Fisher_Yate(&(graph -> liste));
    //afficheliste_bis(graph ->liste);
    part_t * maPart = kruskal(graph,"mon_kru2",1);
    return 0;
}
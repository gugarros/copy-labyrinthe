#include "graph_liste.h"

// graph_liste : couple (nmbr de noeuds, liste d'arêtes (plus petit noeud, plus gros noeud))

graph_liste_t * init_graph_liste_alea(int n, float p){
    time_t seed=time(0);
    srand(seed);
    int i,j;
    float a;
    
    graph_liste_t * graph = (graph_liste_t *) malloc(sizeof(graph_liste_t));
    if (graph != NULL) {
        graph ->n=n;
        graph-> liste = NULL;
        for (i=0;i<n;i++){
            for(j=i+1;j<n;j++){
                if ((((float) rand()) / ((float) RAND_MAX)) <p){
                    printf("%d %d ||\n",i,j);
                    a=100*((float) rand()/ (float) RAND_MAX);
                    insertion_trie_bis(&(graph -> liste), creCellule_bis(i,j,a));
                }
            }
        }
    }else{
        printf("Erreur\n");
        free(graph);
        graph = NULL;
    }
    return graph;
}

void dot_graph_liste(graph_liste_t * monGraph,char *nom,int connexe,int generation){
    //connexe = - 1 affiche tout
    //connexe = i >= 0 affiche la composante si besoin
    //generation == 1 -> genere automatique les dot.png
    //generation == 0 -> affiche la commande
    char * nom_dot = (char *) malloc(sizeof(char)* (strlen(nom)+4));
    strcpy(nom_dot,nom);
    strcat(nom_dot,".dot");
    FILE * fichier = fopen(nom_dot,"w");
    fprintf(fichier,"graph {\n");
    int i = 0;
    int a,b;
    element_t * cour = monGraph -> liste;
    
    if (connexe>=0){
        fprintf(fichier, "  %d;\n",connexe);
    } else{
        for(i=0;i<monGraph ->n;++i){
            fprintf(fichier, "  %d;\n",i);
        }
    }
    while (cour !=NULL){
        a = cour -> couple.noeud1;
        b = cour -> couple.noeud2;
        fprintf(fichier, "  %d -- %d[label=%f];\n",a,b,cour->couple.val);
        cour = cour -> suivant;
        //i+=1;    
    }
    fprintf(fichier,"}");
   
    fclose(fichier);

    if (generation){
        generation_dot(nom);
    } else {
        printf("Faite :\ndot -Tpng %s.dot -o %s.png\n",nom,nom);
    }
    free(nom_dot);
}


part_t * composante_connexe_graph_liste(graph_liste_t * monGraph){
    part_t * maPart = init_part(monGraph->n);
    element_t * cour = monGraph -> liste;
    while (cour != NULL){
        fusioner_part(maPart,cour->couple.noeud1,cour->couple.noeud2);
        cour = cour -> suivant;
    }
    return maPart;
}


graph_liste_t* copie_graph_liste(graph_liste_t * original, ele_t * listek){
    graph_liste_t * maCopie = (graph_liste_t*) malloc(sizeof(graph_liste_t));
    element_t * cour;
    if (maCopie != NULL){
        maCopie ->n = original ->n;
        while(listek != NULL){
            cour = original->liste;
            while (cour != NULL){
                if (cour->couple.noeud1 == listek ->cout){
                    printf("inser%d %d %f\n",cour->couple.noeud1,cour->couple.noeud2,cour->couple.val);
                    insertion_trie_bis(&(maCopie->liste),creCellule_bis(cour->couple.noeud1,cour->couple.noeud2,cour->couple.val));
                    printf("inser ok\n");
                }
                cour = cour ->suivant;
            }    
            listek = listek ->suivant;
        }
    }
    return maCopie;
}

void dot_composante_connexe_graph_liste(graph_liste_t *monGraph,int generation){
    //generation == 1 -> genere automatique les dot.png
    //generation == 0 -> affiche la commande
    int k;
    part_t * maPart = composante_connexe_graph_liste(monGraph);
    ele_t ** maListe = lister_partition(maPart,&k);
    afficher_partition(maPart);
    char vrai_nom[] = "mesPart";
    char nom[12];
    strcpy(nom,vrai_nom);
    char nombre[4];

    graph_liste_t * copie;
    for(k=0;k<(monGraph->n);++k){
        if (maListe[k] !=NULL){
            copie = copie_graph_liste(monGraph,maListe[k]);
            //printf("fluuuuute\n");
            //afficheliste_bis(copie -> liste);
            //printf("okkkk\n");
            sprintf(nombre, "%d", maListe[k]->cout);
            strcpy(nom,vrai_nom);
            strcat(nom,nombre);
            //printf("okkkk2\n");
            dot_graph_liste(copie,nom,k,generation);
            //printf("okkkk3\n");
            if (copie ->liste != NULL){
                freeliste_bis(&(copie->liste));
            }
            copie ->liste = NULL;
        }
    }
}

part_t * kruskal(graph_liste_t * monGraph,char *nom,int generation){
    char * nom_dot = (char *) malloc(sizeof(char)* (strlen(nom)+4));
    strcpy(nom_dot,nom);
    strcat(nom_dot,".dot");
    FILE * fichier = fopen(nom_dot,"w");
    fprintf(fichier,"graph {\n");
    
    part_t * maPart = init_part(monGraph ->n);
    element_t ** adprec = &(monGraph ->liste);
    element_t * cour = *adprec;
    int root_i;
    int root_j; 
    for(int i=0;i<monGraph ->n;++i){
        fprintf(fichier, "  %d;\n",i);
    }

    //int longueur = longueur_bis(cour);
    while (cour !=NULL){
        root_i = racine_part(maPart, cour ->couple.noeud1);
        root_j = racine_part(maPart, cour ->couple.noeud2);
        if (root_i != root_j){
            fusioner_part(maPart,root_i,root_j);
            fprintf(fichier, "  %d -- %d[color=red,penwidth=3.0,label=%f];\n",cour->couple.noeud1,cour->couple.noeud2,cour->couple.val);
        } else {
            fprintf(fichier, "  %d -- %d[label=%f];\n",cour->couple.noeud1,cour->couple.noeud2,cour->couple.val);
        }
        adprec=&((*adprec)->suivant);
        cour=*adprec;
    }        
    
    fprintf(fichier,"}");
   
    fclose(fichier);

    if (generation){
        generation_dot(nom);
    } else {
        printf("Faite :\ndot -Tpng %s.dot -o %s.png\n",nom,nom);
    }
    free(nom_dot);
    return maPart;
}


void free_graph_mat(graph_liste_t * monGraph){
    
    freeliste_bis(&monGraph ->liste);
    free(monGraph);
}


void k_to_np(int k,int *i,int *j,int n){
    *i = k % n;
    *j = (int) (k / n);
}

int np_to_k(int i,int j,int n){
    return i*n +j;
}


graph_liste_t * init_laby_liste(int n, int p){
    time_t seed=time(0);
    srand(seed);
    int i,j;
    int k;
    float a;
    
    graph_liste_t * graph = (graph_liste_t *) malloc(sizeof(graph_liste_t));
    if (graph != NULL) {
        graph ->n = n*p;
        printf("%d||||\n",graph ->n);
        graph-> liste = NULL;
        for (k=0;k<graph->n;k++){
            k_to_np(k,&i,&j,n);
            if (i+1<n && j+1<p){
                //a=100*((float) rand()/ (float) RAND_MAX);
                a=1;
                insercellule_bis(&(graph -> liste), creCellule_bis(k,k+1,a));
                //a=100*((float) rand()/ (float) RAND_MAX);
                a=1;
                insercellule_bis(&(graph -> liste), creCellule_bis(k,k+n,a));
            } else {
                if (i+1<n){
                    //a=100*((float) rand()/ (float) RAND_MAX);
                    a=1;
                    insercellule_bis(&(graph -> liste), creCellule_bis(k,k+1,a));
                } else {
                    if (j +1< p){
                        //a=100*((float) rand()/ (float) RAND_MAX);
                        a=1;
                        insertion_trie_bis(&(graph -> liste), creCellule_bis(k,k+n,a));
                    } else {
                        printf("VIDE \n");
                    }
                }
            }
            //printf("%d %d %d ||\n",k,i,j);
        }
    }else{
        printf("Erreur\n");
        free(graph);
        graph = NULL;
    }
    return graph;
}

element_t * Fisher_Yate(element_t ** aretes){
    time_t seed= time(0);
    srand(seed);
    int l = longueur_bis(*aretes);
    element_t * res = NULL;
    int i,j;
    element_t * temp = NULL;
    element_t ** cour = &temp;
    printf("ok2\n");
    for (i=l;i>=1;i--){
        j = (int) (i * ((float) rand())/ ((float) RAND_MAX));
        cour = va_chercher(aretes,j);
        insercellule_bis(&res,creCellule_bis((*cour) -> couple.noeud1, (*cour) -> couple.noeud2, (*cour) -> couple.val));
        suppcellule_bis(cour);
    }
    return res;
}


void dijkstra(int origine, int arrivee, graph_liste_t * G){
    int * P1 = (int*) malloc(sizeof(int)*(G->n)); // liste des distances du point initial par sommet (-1 pour ceux non traversé) 
    int * P2 = (int*) malloc(sizeof(int)*(G->n)); // liste des prédécesseurs
    int i;
    for(i=0;i< G->n ;i++){
        P1[i]=-1;
    }
    P1[origine]=0;
    P2[origine]=origine;
    int l = 1; //nombres de sommmets traversé
    noeud_tas_t * a_mini = (noeud_tas_t *) malloc(sizeof(noeud_tas_t));
    noeud_tas_t * neu = (noeud_tas_t *) malloc(sizeof(noeud_tas_t));
    neu->noeud = origine;
    neu->poids = 0;
    tasBis_t * monTas = init_tasBis((G->n)*(G->n));
    insere_tasBis(monTas,*neu);
    element_t * cour = (element_t *) malloc(sizeof(element_t));
    while (l<G -> n){
        *a_mini = extraitminBis(monTas);
        l+=1;
        cour = G -> liste;
        while ((cour!=NULL)&&(a_mini->noeud!=arrivee)){
            if ((cour -> couple.noeud1)==a_mini->noeud){
                if ((P1[cour -> couple.noeud2]==-1)||(P1[cour -> couple.noeud1]>P1[a_mini->noeud]+(cour -> couple.val))){
                    neu->noeud = (cour -> couple.noeud2);
                    neu->poids = a_mini->poids +(cour -> couple.val);
                    P1[neu->noeud]=neu->poids;
                    P2[cour -> couple.noeud2]=a_mini->noeud;
                    insere_tasBis(monTas,*neu);
                }
            }
            else{
                if ((cour -> couple.noeud2)==a_mini->noeud){
                    if ((P1[cour -> couple.noeud1]==-1)||(P1[cour -> couple.noeud2]>P1[a_mini->noeud]+(cour -> couple.val))){
                        neu->noeud = (cour -> couple.noeud1);
                        neu->poids = a_mini->poids +(cour -> couple.val);;
                        P1[neu->noeud]=neu->poids;
                        P2[cour -> couple.noeud1]=a_mini->noeud;
                        insere_tasBis(monTas,*neu);
                    }
                }
            }
            
            cour = cour -> suivant;
        }

    }
    affiche_tab(P2,G->n);
    free(a_mini);
    free(neu);
    free(cour);
    free_tasBis(monTas);
    free(P1);
    free(P2);
}




graph_liste_t * kruskal_graph(graph_liste_t * monGraph){
    graph_liste_t * copie = (graph_liste_t*) malloc(sizeof(graph_liste_t));
    if (copie != NULL) {
        copie->n = monGraph ->n;
        copie->liste =NULL;
        monGraph ->liste = Fisher_Yate(&(monGraph ->liste));
        
        part_t * maPart = init_part(monGraph ->n);
        element_t ** adprec = &(monGraph ->liste);
        element_t * cour = *adprec;
        int root_i;
        int root_j;

        //int longueur = longueur_bis(cour);
        while (cour !=NULL){
            root_i = racine_part(maPart, cour ->couple.noeud1);
            root_j = racine_part(maPart, cour ->couple.noeud2);
            if (root_i != root_j){
                fusioner_part(maPart,root_i,root_j);
                insercellule_bis(&copie ->liste,creCellule_bis(cour ->couple.noeud1,cour ->couple.noeud2,cour ->couple.val));
            }
            adprec=&((*adprec)->suivant);
            cour=*adprec;
        }
    }
    return copie;
}


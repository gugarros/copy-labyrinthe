#include <stdlib.h>
#include <stdio.h>
#include "tasBis.h"


tasBis_t*  init_tasBis(int tailleMax){
    tasBis_t * monTas = (tasBis_t *) malloc(sizeof(tasBis_t));
    if (monTas != NULL) {
        monTas -> tailleMax = tailleMax;
        monTas -> taille = 0;
        monTas -> tab = (noeud_tas_t*) malloc(sizeof(noeud_tas_t)*tailleMax);
        if (monTas -> tab != NULL) {
            //printf("ok alloc tas\n");
        } else {
            printf("Erreur alloc tas ( tab)\n");
            free(monTas);
        }
        
    } else {
        printf("Erreur alloc tas\n");
    }
    return monTas;
}

void echange_tasBis(tasBis_t * monTas,int i,int j){
    noeud_tas_t temp = monTas -> tab[i];
    monTas -> tab[i] = monTas-> tab[j];
    monTas-> tab[j] = temp;
}


void insere_tasBis(tasBis_t * monTas, noeud_tas_t x){
    //printf("insere : %d\n",x);
    if (monTas -> taille < monTas ->tailleMax){
        int k = monTas -> taille;
        monTas -> tab[k] = x;
        while ((k > 0) && (monTas -> tab[(k-1)/2].poids > monTas -> tab[k].poids )){
            echange_tasBis(monTas,(k-1)/2,k);
            k = (k-1)/2;
        }
        monTas -> taille = monTas -> taille + 1;
    } else {
        printf("Erreur tas trop petit lors insere (%d - %f)\n",x.noeud,x.poids);
    }
}


void entasse_tasBis(tasBis_t * monTas, int i){
    int fg = 2*i+1;
    int fd = fg + 1;
    int mini = i;
    if ((fg < monTas ->taille) && (monTas -> tab[fg].poids < monTas ->tab[i].poids)){
        mini =fg;
    }
    if ((fd < monTas -> taille) && (monTas -> tab[fd].poids < monTas->tab[mini].poids)){
        mini = fd;
    }

    if (i != mini){
        echange_tasBis(monTas,i,mini);
        entasse_tasBis(monTas,mini);
    }
}

noeud_tas_t extraitminBis(tasBis_t * monTas){
    noeud_tas_t mini = monTas -> tab[0];
    if (monTas ->taille >0){
        echange_tasBis(monTas,0,monTas -> taille-1);
        monTas -> taille = monTas ->taille -1;
        entasse_tasBis(monTas,0);
    } else {
        printf("Erreur extrai min\n");
    }
    return mini;
}


void tri_par_tasBis(noeud_tas_t * tab,int taille){
    tasBis_t * monTas = init_tasBis(taille);
    for(int i =0;i<taille;++i){
        insere_tasBis(monTas,tab[i]);
    }
    for(int i =0;i<taille;++i){
        tab[i] = extraitminBis(monTas);
    }
    free(monTas);
}



void free_tasBis(tasBis_t * monTas){
    free(monTas -> tab);
    free(monTas);
}


void affiche_tasBis(tasBis_t * monTas){
    for(int i = 0;i<monTas->taille;++i){
        printf("%d - %f\n",(monTas->tab[i].noeud),(monTas->tab[i].poids));
    }
    printf("\n");
}


void affiche_tabBis(noeud_tas_t * tab, int taille){
    for(int i = 0;i<taille;++i){
        printf("%d - %f\n",(tab[i].noeud),(tab[i].poids));
    }
    printf("\n");
}
#include <stdio.h>
#include <stdlib.h>
#include "part.c"
#include "lch.c"
#include <time.h>

/*
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
*/


int main(int argc, char **argv) {

    (void)argc;
    (void)argv;

    // SDL_version nb;
    // SDL_VERSION(&nb);

    // printf("Version de la SDL : %d.%d.%d\n", nb.major, nb.minor, nb.patch);

    part_t * maPart = init_part(11);
    fusioner_part(maPart,0,1);
    afficher_partition(maPart);
    fusioner_part(maPart,2,3);
    afficher_partition(maPart);
    fusioner_part(maPart,10,3);
    afficher_partition(maPart);
    fusioner_part(maPart,5,9);
    afficher_partition(maPart);
    fusioner_part(maPart,4,6);
    afficher_partition(maPart);
    fusioner_part(maPart,8,7);
    afficher_partition(maPart);
    fusioner_part(maPart,7,9);
    afficher_partition(maPart);
    fusioner_part(maPart,6,8);
    printf("fusion de 0,1|2,3|10,3|5,9|4,6|8,7|7,9|6,8|\n");
    afficher_partition(maPart);
    dot_partition(maPart,"ma_part",1);

    ele_t * liste1 = lister_classe(maPart,1);
    afficheliste(liste1);
    ele_t * liste2 = lister_classe(maPart,6);
    afficheliste(liste2);
    freeliste(&liste1);
    freeliste(&liste2);
    int n;
    int i;
    printf("Affichage partition\n");
    ele_t ** tab = lister_partition(maPart,&n);
    for(i = 0;i<n;++i){
        if (tab[i] !=NULL){
            afficheliste(tab[i]);
        }
    }
    
    for(i = 0;i<n;++i){
        if (tab[i] !=NULL){
            freeliste(&tab[i]);
        }
    }
    free(tab);
    free_part(maPart);
}
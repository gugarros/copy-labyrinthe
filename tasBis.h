#ifndef __GARROS_HANOCQ_BOUDIN_TASBIS_H
#define __GARROS_HANOCQ_BOUDIN_TASBIS_H

#include <stdlib.h>

typedef struct noeud_tas
{
    int noeud;
    float poids;
}noeud_tas_t;

typedef struct tasBis
{
    int tailleMax;
    int taille;
    noeud_tas_t * tab;
}tasBis_t;


tasBis_t*  init_tasBis(int tailleMax);

void echange_tasBis(tasBis_t * monTas,int i,int j);

void insere_tasBis(tasBis_t * monTas, noeud_tas_t x);

void entasse_tasBis(tasBis_t * monTas, int i);

noeud_tas_t extraitminBis(tasBis_t * monTas);

void affiche_tasBis(tasBis_t * monTas);

void tri_par_tasBis(noeud_tas_t * tab,int taille);

void free_tasBis(tasBis_t * monTas);

void affiche_tabBis(noeud_tas_t * tab, int taille);


#endif

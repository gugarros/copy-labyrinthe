#include "ennemis.h"

int mouv_ennemis(int tab_ennemis[][2], int taille,SDL_Window* window){
    int i;
    int over = 0;
    SDL_Rect window_dimensions = {0};
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    for(i=0;i<taille;i++){
        tab_ennemis[i][1]+=2;
        if (tab_ennemis[i][1] >= 0.9*window_dimensions.h) over = 1;
    }
    return over;
}

void affiche_ennemis(SDL_Texture* my_texture,
                            SDL_Renderer* renderer,
                            int tab[][2],
                            int taille,
                            float zoom) {
     SDL_Rect 
           source = {0},                      // Rectangle définissant la zone de la texture à récupérer
           destination = {0};                 // Rectangle définissant où la zone_source doit être déposée dans le renderer

     
     SDL_QueryTexture(my_texture, NULL, NULL,
                      &source.w, &source.h);  // Récupération des dimensions de l'image

                             // Facteur de zoom à appliquer   
    int i;
    for (i=0;i<taille;++i){ 
        destination.w = (int) source.w * zoom;         // La destination est un zoom de la source
        destination.h = (int) source.h * zoom;         // La destination est un zoom de la source
        destination.x = tab[i][0] - destination.w/2;     // La destination est au milieu de la largeur de la fenêtre
        destination.y = tab[i][1] + destination.h / 2;  // La destination est au milieu de la hauteur de la fenêtre

        SDL_RenderCopy(renderer, my_texture,     // Préparation de l'affichage  
                    &source,
                    &destination); 
    }           
}

void generer_ennemis(int tab_ennemis[][2], SDL_Window* window){
    time_t seed = time(0);
    srand(seed);
    int i;
    int x,y;
    SDL_Rect window_dimensions = {0};
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    for (i=0;i<ENEMIE_N;i++){

        x = (int) (window_dimensions.w * (rand() / (float) RAND_MAX)) ;
        y = (int) (window_dimensions.h/2 * (rand() / (float) RAND_MAX)) ;
        tab_ennemis[i][0]=x;
        tab_ennemis[i][1]=y;
    }
}
void afficher_score(int score, int * combo,int * taille, SDL_Window* window,SDL_Renderer* renderer){
    
    char score_text[10];
    int taille_min = 65;
    int taille_max = 130;

    *taille=*taille+*combo;
    if (*taille<taille_min){
        *taille=taille_min;
    }
    if (*taille>taille_max){
      *taille=taille_max;
    }
    if (*combo>-5){
        *combo=*combo-4;
    }

    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre

    SDL_Color color = {(int)(255*(*taille-taille_min)/(taille_max-taille_min)), 0, 0, 255};                                  // la couleur du texte
    SDL_Surface* text_surface = NULL;                                    // la surface  (uniquement transitoire)
    sprintf(score_text,"%d",score);
    text_surface = TTF_RenderText_Blended(font, score_text, color); // création du texte dans la surface 
    if (text_surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

    SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    pos.x = 0.05*window_dimensions.w;
    pos.y = window_dimensions.h-pos.h;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
}

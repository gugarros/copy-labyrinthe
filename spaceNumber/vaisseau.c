
void bouger(int * position, int * sens, int * frame, SDL_Texture *vaisseau, SDL_Renderer *renderer, SDL_Window *window,float zoom){
  
  int vitesse = 10;                              // Vitesse de deplacement du vaisseau 

  SDL_Rect 
          source = {0},                         // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(vaisseau, NULL, NULL,
                   &source.w, &source.h);       // Récupération des dimensions de l'image

  if (*sens==-1 && *position>0){                // Mise a jour de la frame du vaisseau a afficher
    *position-=vitesse;
    if (*frame>0){*frame=*frame-1;}
  }
  else{
    if (*sens==1 && *position<window_dimensions.w){
      *position+=vitesse;
      if (*frame<8){*frame=*frame+1;}
    }
    else {
      if (*frame<5){*frame=*frame+1;};
      if (*frame>5){*frame=*frame-1;}
    }
  }
  //printf("f%d\n",*frame);
  destination.w=zoom*source.w/9;
  destination.h=zoom*source.h;
  destination.x=*position-destination.w/2;
  destination.y=window_dimensions.h*0.8;          // Position verticale du vaisseau
  


  source.x = *frame*343;
  source.y=0;
  source.w=source.w/9;
  source.h=source.h;

  SDL_RenderCopy(renderer, vaisseau,
                 &source,
                 &destination);                // Création de l'élément à afficher
  
}

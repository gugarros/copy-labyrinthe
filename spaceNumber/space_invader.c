//gcc space_invader.c -o prog -lSDL2 -lSDL2_gfx -lSDL2_image -lSDL2_ttf -lSDL2_mixer -lSDL2_net
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include "tirer.c"
#include "vaisseau.c"
#include "ennemis.c"


void afficherEcran(SDL_Renderer *renderer){
  SDL_RenderPresent(renderer);
}


void charge_fond(SDL_Renderer * renderer,int img,
              SDL_Surface *fond_img0,
              SDL_Surface *fond_img1,
              SDL_Surface *fond_img2){
  
  SDL_Texture  *image;
  if (img ==0 ) image = SDL_CreateTextureFromSurface(renderer, fond_img0);
  if (img ==1 ) image = SDL_CreateTextureFromSurface(renderer, fond_img1);
  if (img ==2 ) image = SDL_CreateTextureFromSurface(renderer, fond_img2);
  
  //couleur de fond 
  // SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
  // SDL_RenderClear(renderer);
  
  SDL_Rect rect;
  rect.x = 0;
  rect.y = 0;
  rect.w = 400;
  rect.h = 800;
  SDL_RenderCopy(renderer, image, NULL, &rect); 
  /* L'image a ete copiee dans le renderer qui sera plus tard affiche a l'ecran */
}

void affiche_alea_etoile(SDL_Renderer * renderer,
              SDL_Surface *fond_img0,
              SDL_Surface *fond_img1,
              SDL_Surface *fond_img2,
              SDL_Surface *fond_img3){

  SDL_Texture  *image;
  int img = rand() % 4;
  if (img ==0 ) image = SDL_CreateTextureFromSurface(renderer, fond_img0);
  if (img ==1 ) image = SDL_CreateTextureFromSurface(renderer, fond_img1);
  if (img ==2 ) image = SDL_CreateTextureFromSurface(renderer, fond_img2);
  if (img ==3 ) image = SDL_CreateTextureFromSurface(renderer, fond_img3);

  SDL_Rect rect;
  rect.x = 0;
  rect.y = 0;
  rect.w = 400;
  rect.h = 800;
  SDL_RenderCopy(renderer, image, NULL, &rect); 
}



void win(SDL_Renderer * renderer,TTF_Font * font,int largeur, int hauteur){
  int i;
  for (i = 0;i<20;++i){
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    SDL_Color color = {rand()%256, rand()%256, rand()%256, 255};                                  // la couleur du texte
    SDL_Surface* text_surface = NULL;                                    // la surface  (uniquement transitoire)
    text_surface = TTF_RenderText_Blended(font, "GG !", color); // création du texte dans la surface 
    //if (text_surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    //if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

    SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    pos.x = largeur/2- pos.w/2;
    pos.y = hauteur/2 - pos.h/2;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
    afficherEcran(renderer);
    SDL_Delay(50);
  }
}

void lose(SDL_Renderer * renderer,TTF_Font * font,int largeur, int hauteur){
  int i;
  for (i = 0;i<20;++i){
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    SDL_Color color = {rand()%256, rand()%256, rand()%256, 255};                                  // la couleur du texte
    SDL_Surface* text_surface = NULL;                                    // la surface  (uniquement transitoire)
    text_surface = TTF_RenderText_Blended(font, "LOSE !", color); // création du texte dans la surface 
    //if (text_surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    //if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

    SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    pos.x = largeur/2- pos.w/2;
    pos.y = hauteur/2 - pos.h/2;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
    afficherEcran(renderer);
    SDL_Delay(50);
  }
}


void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer,                             // renderer à fermer
                  TTF_Font * font) {                                  // font à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }

  if (renderer != NULL) SDL_DestroyRenderer(renderer);
  if (window != NULL)   SDL_DestroyWindow(window);
  if (font != NULL) TTF_CloseFont(font);

  IMG_Quit();
  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}



void afficher_score(int score, int * combo,int * taille, SDL_Window* window,SDL_Renderer* renderer){
    
    char score_text[10];
    int taille_min = 65;
    int taille_max = 130;

    *taille=*taille+*combo;
    if (*taille<taille_min){
        *taille=taille_min;
    }
    if (*taille>taille_max){
      *taille=taille_max;
    }
    if (*combo>-5){
        *combo=*combo-4;
    }

    TTF_Font * font = NULL;  
    font = TTF_OpenFont("./img1/Pacifico.ttf", *taille );


    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre

    SDL_Color color = {255, 255 -(int)(255*(*taille-taille_min)/(taille_max-taille_min)), 255 -(int)(255*(*taille-taille_min)/(taille_max-taille_min)), 255};                                  // la couleur du texte
    SDL_Surface* text_surface = NULL;                                    // la surface  (uniquement transitoire)
    sprintf(score_text,"%d",score);
    text_surface = TTF_RenderText_Blended(font, score_text, color); // création du texte dans la surface 
    if (text_surface == NULL) end_sdl(0, "Can't create text surface", window, renderer,font);

    SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer,font);
    SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

    SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    pos.x = 0.05*window_dimensions.w;
    pos.y = window_dimensions.h-pos.h;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
    TTF_CloseFont(font);
}



int main(int argc, char **argv) {
  (void)argc;
  (void)argv;



  int largeur =  400;
  int hauteur = 800;
  SDL_Window *window = NULL;               // Future fenêtre
  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
  }

  /* Création de la fenêtre */
  window = SDL_CreateWindow(
      "Space_invader",                    // codage en utf8, donc accents possibles
      0, 0,                                  // coin haut gauche en haut gauche de l'écran
      400, 800,                              // largeur , hauteur
      SDL_WINDOW_RESIZABLE);                 // redimensionnable
  if (window == NULL) {
    SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
    SDL_Quit();
    exit(EXIT_FAILURE);
  }

  if (TTF_Init() != 0){
    fprintf(stderr, "Erreur d'initialisation TTF : %s\n", TTF_GetError()); 
  }
  
  TTF_Font * font = NULL;
  
  font = TTF_OpenFont("./img1/Pacifico.ttf", 65 );
  //TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);           // en italique, gras
  

  int flags=IMG_INIT_JPG|IMG_INIT_PNG;
  int initted= 0;

  initted = IMG_Init(flags);

  if((initted&flags) != flags) 
  {
      printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
      printf("IMG_Init: %s\n", IMG_GetError());
  }
  
  //------------------------------------------------------------------
  /* Création du renderer */
  SDL_Renderer* renderer = NULL;
  renderer = SDL_CreateRenderer(
           window, -1, 0);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer,font);

  //------------------------------------------------------------------
  /* gestion du fond de l'image */
  int img = 0;
  SDL_Surface *fond_img0 = NULL;
  SDL_Surface *fond_img1 = NULL;
  SDL_Surface *fond_img2 = NULL;
  fond_img0 = IMG_Load("./img1/Nebula Aqua-Pink - copie.png");
  fond_img1 = IMG_Load("./img1/Nebula Blue - copie.png");
  fond_img2 = IMG_Load("./img1/Nebula Red - copie.png");
  
  /* image=SDL_LoadBMP("loic.bmp"); fonction standard de la SDL2 */
  if(!fond_img0) {
      printf("IMG_Load:0 %s\n", IMG_GetError());
  }
  if(!fond_img1) {
      printf("IMG_Load:1 %s\n", IMG_GetError());
  }
  if(!fond_img2) {
      printf("IMG_Load:2 %s\n", IMG_GetError());
  }
  //------------------------------------------------------------------
  /* fond d'etoiles */
  SDL_Surface *fond_img_etoile_0 = NULL;
  SDL_Surface *fond_img_etoile_1 = NULL;
  SDL_Surface *fond_img_etoile_2 = NULL;
  SDL_Surface *fond_img_etoile_3 = NULL;
  fond_img_etoile_0 = IMG_Load("./img1/Stars Small_1 - copie.png");
  fond_img_etoile_1 = IMG_Load("./img1/Stars Small_2 - copie.png");
  fond_img_etoile_2 = IMG_Load("./img1/Stars-Big_1_1_PC - copie.png");
  fond_img_etoile_3 = IMG_Load("./img1/Stars-Big_1_2_PC - copie.png");
  
  if(!fond_img_etoile_0) {
      printf("IMG_Load:0 %s\n", IMG_GetError());
  }
  if(!fond_img_etoile_1) {
      printf("IMG_Load:1 %s\n", IMG_GetError());
  }
  if(!fond_img_etoile_2) {
      printf("IMG_Load:2 %s\n", IMG_GetError());
  }
  if(!fond_img_etoile_3) {
      printf("IMG_Load:2 %s\n", IMG_GetError());
  }
  //------------------------------------------------------------------
  /* gestion enemis et tirs*/
  int tab_tirs [TIRS_MAX][2];
  int tab_ennemis [ENEMIE_N][2];

  generer_ennemis(tab_ennemis,window);

  int game_over = 0;

  int nombre_actuel_tirs = 0;
  int nombre_actuel_enemis = ENEMIE_N;

  float zoom_enemie = 0.8;
  float zoom_tir = 1;
  SDL_Surface *enemie = NULL;
  enemie = IMG_Load("./img1/enemyBlue1.png");
  if(!enemie) {
      printf("IMG_Load:enemie %s\n", IMG_GetError());
  }
  SDL_Texture* my_enemie = SDL_CreateTextureFromSurface(renderer, enemie);
 
  SDL_Surface *tir = NULL;
  tir = IMG_Load("./img1/laserRed01.png");
  if(!tir) {
      printf("IMG_Load:tir %s\n", IMG_GetError());
  }
  SDL_Texture* my_tir = SDL_CreateTextureFromSurface(renderer, tir);
  
  SDL_Rect tir_dim={0};
  SDL_Rect ennemis_dim={0};
  SDL_QueryTexture(my_tir, NULL,NULL, &tir_dim.w, &tir_dim.h);
  SDL_QueryTexture(my_enemie, NULL,NULL, &ennemis_dim.w, &ennemis_dim.h);

  //------------------------------------------------------------------
  /* vaisseau */
  int position = largeur /2;
  int sens = 0;
  int frame = 5;
  float zoom_vaisseau = 0.4;

  SDL_Surface *vaisseau = NULL;
  vaisseau = IMG_Load("./img1/vaisseau_animationxcf.png");
  if(!vaisseau) {
      printf("IMG_Load:vaisseau %s\n", IMG_GetError());
  }
  SDL_Texture* my_vaisseau = SDL_CreateTextureFromSurface(renderer, vaisseau);
  //------------------------------------------------------------------
  /* score */
  int score = 0;
  int combo = 0;
  int taille = 65;

  //------------------------------------------------------------------
  
  SDL_bool
  program_on = SDL_TRUE,                          // Booléen pour dire que le programme doit continuer
  paused = SDL_FALSE;                             // Booléen pour dire que le programme est en pause
  while (program_on) {                              // La boucle des évènements
    SDL_Event event;                                // Evènement à traiter

    while (program_on && SDL_PollEvent(&event)) {   // Tant que la file des évènements stockés n'est pas vide et qu'on n'a pas
                                                    // terminé le programme Défiler l'élément en tête de file dans 'event'
      switch (event.type) {                         // En fonction de la valeur du type de cet évènement
      case SDL_QUIT:                                // Un évènement simple, on a cliqué sur la x de la // fenêtre
        program_on = SDL_FALSE;                     // Il est temps d'arrêter le programme
        break;
      case SDL_KEYDOWN:                             // Le type de event est : une touche appuyée
                                                    // comme la valeur du type est SDL_Keydown, dans la pratie 'union' de
                                                    // l'event, plusieurs champs deviennent pertinents   
        switch (event.key.keysym.sym) {             // la touche appuyée est ...
        case SDLK_p:                                // 'p'
          paused = !paused;
          //printf("paused\n");
          break;
        case SDLK_SPACE:                            // 'SPC'
          tirer(tab_tirs,&nombre_actuel_tirs,position,0.8*hauteur - (383/2)*zoom_vaisseau);
          //printf("touche space\n");
          break;
        case SDLK_ESCAPE:                           // 'ESCAPE'  
        case SDLK_q:                                // 'q'
          program_on = 0;                           // 'escape' ou 'q', d'autres façons de quitter le programme                                     
          break;
        case SDLK_LEFT:
          sens = -1;
          //bouger(&position, &sens, &frame, my_vaisseau, renderer, window,zoom_vaisseau);
          //printf("touche gauche\n");
          break;
        case SDLK_RIGHT:
          sens = 1;
          //bouger(&position, &sens, &frame, my_vaisseau, renderer, window,zoom_vaisseau);
          //printf("touche droite\n");
          break;
        case SDLK_f:
          img = (img +1) %3;
          printf("font change\n");
          break;
        default:                                    // Une touche appuyée qu'on ne traite pas
          break;
        }
      case SDL_MOUSEBUTTONDOWN:                     // Click souris
        if (SDL_GetMouseState(NULL, NULL) & 
            SDL_BUTTON(SDL_BUTTON_LEFT) ) {         // Si c'est un click gauche
          //printf("clickgauche\n");
          tirer(tab_tirs,&nombre_actuel_tirs,position,0.8*hauteur - (383/2)*zoom_vaisseau);
        }
        break;
      default:                                      // Les évènements qu'on n'a pas envisagé
        sens = 0;
        break;
      }
    }
    //action
    if (!paused) {                                  // Si on n'est pas en pause
      // on peux stack les tirs en pause, c'est rigolo
      game_over = mouv_ennemis(tab_ennemis,nombre_actuel_enemis,window);
      avance_tirs(tab_tirs,tab_ennemis,zoom_tir,zoom_enemie,&nombre_actuel_tirs,&nombre_actuel_enemis,&score,tir_dim,ennemis_dim,&combo);
      charge_fond(renderer,img,fond_img0,fond_img1,fond_img2);
      affiche_alea_etoile(renderer,fond_img_etoile_0,fond_img_etoile_1,fond_img_etoile_2,fond_img_etoile_3);
      charge_tirs(my_tir,renderer,tab_tirs,nombre_actuel_tirs,1);
      bouger(&position, &sens, &frame, my_vaisseau, renderer, window,zoom_vaisseau);
      affiche_ennemis(my_enemie,renderer,tab_ennemis,nombre_actuel_enemis,zoom_enemie);
      afficher_score(score,&combo,&taille,window,renderer);
      afficherEcran(renderer);

      if (nombre_actuel_enemis == 0){
        program_on = SDL_FALSE;
        win(renderer,font,largeur,hauteur);
      } else {
        if (game_over == 1){
          program_on = SDL_FALSE;
          lose(renderer,font,largeur,hauteur);
        }
      }
    }
    SDL_Delay(30);                                  // Petite pause
  }


  

  SDL_FreeSurface(fond_img0);
  SDL_FreeSurface(fond_img1);
  SDL_FreeSurface(fond_img2);

  SDL_FreeSurface(fond_img_etoile_0);
  SDL_FreeSurface(fond_img_etoile_1);
  SDL_FreeSurface(fond_img_etoile_2);
  SDL_FreeSurface(fond_img_etoile_3);
  
  end_sdl(1, "Normal ending", window, renderer,font);//renderer
  SDL_Quit();
  return 0;
}
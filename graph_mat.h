#ifndef __GARROS_HANOCQ_BOUDIN_GRAPH_MAT_H
#define __GARROS_HANOCQ_BOUDIN_GRAPH_MAT_H

#include <stdlib.h>
#include "lch.h"

typedef struct graph_mat
{
    int n;
    int ** tab;
} graph_mat_t;

#endif

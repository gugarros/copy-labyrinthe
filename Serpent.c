#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

typedef struct point{
    float x;
    float y;
} point;

typedef struct chaine{
    float taille;
    unsigned char r;
    unsigned char g;
    unsigned char b;
    float angle;
    struct chaine * suivant;
} chaine;

point rotation(float x, float y, float angle);
void rectangle(SDL_Renderer* renderer,point centre, float longueur, float hauteur, float angle);
void end_sdl(char ok,char const* msg,SDL_Window* window,SDL_Renderer* renderer);

point rotation(float x, float y, float angle){
    point point_tourne;
    point_tourne.x = x*cos(angle)-y*sin(angle);
    point_tourne.y = x*sin(angle)+y*cos(angle);

    return point_tourne;
}

void affiche_serpent(chaine * serpent){

  chaine * cour = serpent;
  chaine * prec;

  do {
    prec = cour;
    cour = cour->suivant;
    printf("%f  ",prec->taille);
  } while (cour!=NULL);
  printf("\n");
}

void rectangle(SDL_Renderer* renderer,point centre, float longueur, float hauteur, float angle){
    point sommet_hg = rotation(-longueur/2,hauteur/2,angle);
    point sommet_hd = rotation(longueur/2,hauteur/2,angle);
    point sommet_bg = rotation(-longueur/2,-hauteur/2,angle);
    point sommet_bd = rotation(longueur/2,-hauteur/2,angle);
    SDL_RenderDrawLine(renderer,centre.x+sommet_hg.x,centre.y+sommet_hg.y,centre.x+sommet_hd.x,centre.y+sommet_hd.y);
    SDL_RenderDrawLine(renderer,centre.x+sommet_hd.x,centre.y+sommet_hd.y,centre.x+sommet_bd.x,centre.y+sommet_bd.y);
    SDL_RenderDrawLine(renderer,centre.x+sommet_bd.x,centre.y+sommet_bd.y,centre.x+sommet_bg.x,centre.y+sommet_bg.y);
    SDL_RenderDrawLine(renderer,centre.x+sommet_bg.x,centre.y+sommet_bg.y,centre.x+sommet_hg.x,centre.y+sommet_hg.y);

}

void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}                                                                 

int main(int argc, char** argv) {
  (void)argc;
  (void)argv;

  SDL_Window* window = NULL;
  SDL_Renderer* renderer = NULL;

  SDL_DisplayMode screen;

  time_t seed = time(0);
  srand(seed);
  float vitesse=4;

/*********************************************************************************************************************/  
/*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);

  SDL_GetCurrentDisplayMode(0, &screen);
  printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w,
              screen.h);

  /* Création de la fenêtre */
  float largeur = screen.w * 0.66;
  float longueur = screen.h * 0.66;

  window = SDL_CreateWindow("Premier dessin",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, largeur,
                            longueur,
                            SDL_WINDOW_OPENGL);
  if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

  /* Création du renderer */
  renderer = SDL_CreateRenderer(
           window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

  /*********************************************************************************************************************/
  /*                                     On dessine dans le renderer                                                   */
  float rotation=0.0;

  /* Creation et initialisation des serpents*/
  point point;
  chaine * nouveau;
  int N_serpent = 10;
  int Longueurs[N_serpent];
  struct point Tetes[N_serpent];
  chaine * Serpents[N_serpent];

  int i;
  for (i=0;i<N_serpent;i++){

    nouveau = malloc(sizeof(chaine));
    nouveau->taille = 40;
    nouveau->r=0;
    nouveau->g=0;
    nouveau->b=255;
    nouveau->angle=30;
    nouveau->suivant=NULL;

    Serpents[i]=nouveau;
    Longueurs[i]=20+(int)(rand()/RAND_MAX)*20;
    Tetes[i].x = largeur/2;
    Tetes[i].y = longueur/2;
  }

  SDL_bool program_on = SDL_TRUE;               // Booléen pour dire que le programme doit continuer
  while (program_on){                           // Voilà la boucle des évènements
    SDL_Event event;                            // c'est le type IMPORTANT !!

    while(program_on && SDL_PollEvent(&event)){ // Tant que la file des évènements stockés n'est
                                                // pas vide et qu'on n'a pas terminé le programme :
                                                //    Défiler l'élément en tête de file dans 'event'
      switch(event.type){                       // En fonction de la valeur du type de cet évènement
      case SDL_QUIT :                           // Un évènement simple, on a cliqué sur la x de la fenêtre
        program_on = SDL_FALSE;                 // Il est temps d'arrêter le programme
        printf("femer la fenetre");
        break;

      default:                                  // L'évènement défilé ne nous intéresse pas
        break;
      }
    }

    //dessinae du fond
    SDL_Rect fond;                                             

    SDL_SetRenderDrawColor(renderer,                                
                              0, 0, 0,                               // mode Red, Green, Blue (tous dans 0..255)
                                          255);                                   // 0 = transparent ; 255 = opaque
    fond.x = 0;                                                    // x haut gauche du rectangle
    fond.y = 0;                                                    // y haut gauche du rectangle
    fond.w = largeur;                                                  // sa largeur (w = width)
    fond.h = longueur;                                                  // sa hauteur (h = height)

    SDL_RenderFillRect(renderer, &fond);



    //affichage du serpent
    for (i=0;i<N_serpent;i++){
      point = Tetes[i];
      int supression = 0;
      int k = 0;
      int n = Longueurs[i];
      chaine * cour = Serpents[i];
      chaine * temp;
      do {
      
      /* affichage du rectangle*/
      SDL_SetRenderDrawColor(renderer, cour->r, cour->g, cour->b, 255);
      rectangle(renderer,point,0.66*cour->taille,0.33*cour->taille,cour->angle);
      
      point.x=point.x-0.8*0.66*cour->taille*cos(cour->angle)/2;
      point.y=point.y-0.8*0.66*cour->taille*sin(cour->angle)/2;

      SDL_SetRenderDrawColor(renderer, 0,0,0, 255);
      rectangle(renderer,point,10,10,0);

      temp = cour;
      cour = cour->suivant;
      
      if (k==n && cour!=NULL){
        supression = 1;
        temp->suivant=NULL;  //Nouvelle fin de liste, sinon le serpent grandit
      }

      /* Calcule du prochain centre de rectangle*/
      if (cour!=NULL){
        point.x=point.x-0.66*cour->taille*cos(cour->angle)/2;
        point.y=point.y-0.66*cour->taille*sin(cour->angle)/2;
      }
    
      k+=1;

      } while (cour!=NULL);
      
      /* Supression du dernier element si besoin, ajout d'une nouvelle tete et deplacement de la tete*/

      if (supression){
        free(temp);
      }

      Tetes[i].x += vitesse*cos(Serpents[i]->angle);
      Tetes[i].y += vitesse*sin(Serpents[i]->angle);
      nouveau = malloc(sizeof(chaine));
      nouveau->taille = fmaxf(20,fminf(100,Serpents[i]->taille+10*(((float) rand() / (float) RAND_MAX)-0.5)));
      nouveau->r=255*((float) rand() / (float) RAND_MAX);
      nouveau->g=255*((float) rand() / (float) RAND_MAX);
      nouveau->b=255*((float) rand() / (float) RAND_MAX);
      nouveau->angle=Serpents[i]->angle+0.8*(((float) rand() / (float) RAND_MAX)-0.5);
      nouveau->suivant=Serpents[i];

      Serpents[i]=nouveau;
    }
      
    /* Affichage */
    SDL_Delay(50);
    SDL_RenderPresent(renderer);
    rotation+=0.1;
    }

  /* Liberation des liste chainées des serpents */
  for (i=0;i<N_serpent;i++){
    chaine * cour = Serpents[i];
    chaine * temp;

    do {
      temp = cour;
      cour = cour->suivant;
      free(temp);
    } while (cour!=NULL);
  }
  

  /*********************************************************************************************************************/
  /* on referme proprement la SDL */

  end_sdl(1, "Normal ending", window, renderer);
  return EXIT_SUCCESS;
}

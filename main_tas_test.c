#include <stdio.h>
#include <stdlib.h>
//#include "labyrinth.c"
#include "tas.c"
#include <time.h>

static int infi(void const * a,void const * b){
    int const *pa = a;
    int const *pb = b;
    return *pa < *pb;
}

int main(int argc, char **argv) {

    (void)argc;
    (void)argv;




    tas_t * monTas = init_tas(10);
    insere_tas(monTas,5);
    insere_tas(monTas,7);
    insere_tas(monTas,3);
    insere_tas(monTas,10);
    insere_tas(monTas,6);
    printf("insere de 5,7,3,10,6\n");
    affiche_tas(monTas);
    printf("extrait min : %d\n",extraitmin(monTas));
    affiche_tas(monTas);
    free_tas(monTas);



    int n = 100;//taille des tableaux
    int nombre_tri =1;
    time_t seed = time(0);
    printf("seed :%ld\n",seed);
    srand(seed);
    int tab1[n];
    int tab2[n];
    int i;
    for(i = 0;i<n;++i){
        tab1[i] = (int)  ( 1000*(( (float) rand() )/ (float) RAND_MAX));
        tab2[i] = tab1[i];
    }
    



    int nombre;
    //affiche_tab(tab1,n);

    time_t t1 = time(0);
    for(nombre=0;nombre<nombre_tri;++nombre){
        
        tri_par_tas(tab1,n);
        printf("Tri par tas :\n");
        affiche_tab(tab1,n);
        for(i = 0;i<n;++i){
            tab1[i] = tab2[i];
        }
    }
    //affiche_tab(tab1,n);
    time_t t2 = time(0);
    //printf("HeapSort %ld\n",t2-t1);
    

    t1 = time(0);
    for(nombre=0;nombre<nombre_tri;++nombre){
        qsort(tab1,n, sizeof *tab1,infi);
        printf("Tri par heapsort\n");
        affiche_tab(tab1,n);
        for(i = 0;i<n;++i){
            tab1[i] = tab2[i];
        }
    }
    t2 = time(0);
    //printf("qsort %ld\n",t2-t1);
    //affiche_tab(tab2,n);
    return 0;
}

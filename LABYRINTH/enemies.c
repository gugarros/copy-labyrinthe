#include "enemies.h"
#include "labyrinth.h"

#include <stdio.h>


void ajoute_enemies(enemies_t ** ptr_liste_enemies, int x, int y){
    enemies_t * nouveau_enemies = malloc(sizeof(enemies_t));
    nouveau_enemies->type = rand()%3;
    nouveau_enemies->frame = 0;
    nouveau_enemies->x = x;
    nouveau_enemies->y = y;
    nouveau_enemies->x_reel = x;
    nouveau_enemies->y_reel = y;
    nouveau_enemies->dir = -1; // 0=SUD, 1=NORD, 2=EST, 3=OUEST
    nouveau_enemies->suivant = *ptr_liste_enemies;
    *ptr_liste_enemies = nouveau_enemies;
}

void supprime_enemies(enemies_t ** ptr_liste_enemies, float x_reel, float y_reel){
    enemies_t * cour = *ptr_liste_enemies;
    enemies_t ** prec = ptr_liste_enemies;
    while(cour!=NULL){
        if (cour->x_reel==x_reel && cour->y_reel==y_reel){
            *prec=cour->suivant; 
            free(cour);
            cour=NULL;
        }
        else {
            prec = &(cour->suivant);
            cour = cour->suivant;
        }
    }
}

void libere_enemies(enemies_t ** adprec){
    //libere la liste des ennemis
    enemies_t * tmp;
    while ((*adprec) != NULL)
    { 
        tmp = (*adprec) -> suivant;
        free(*adprec);
        *adprec = tmp;
    }
}

void nuke_enemies(enemies_t ** liste_enemies, explosion_t **liste_explosion, int pos_final[2], int n, int p){
    //libere la liste des ennemis
    libere_enemies(liste_enemies);

    //ajoute un eexplosion sur chaque case du labyrinthe
    int i,j;
    for(i=0;i<n;i++){
        for(j=0;j<p;j++){
            if (i!=pos_final[0] || j!=pos_final[1]){ajoute_explosion(liste_explosion,i,j);}
        }
    }
}

enemies_t * genere_liste_enemies(int n_enemies, int n, int p, int ** laby){
    int tab[n][p];
    enemies_t * liste_enemies = NULL;
    int i,j,k,position,position_cour;
    for(i=0;i<n;i++){
        for(j=0;j<p;j++){
            tab[i][j]=0;
        }
    }
    for(k=0;k<n_enemies;k++){
        position=rand()%((n*p)-k-1);
        position_cour=0;
        i=0;
        j=0;
        while(position_cour<position){
            while ((tab[i][j]==1)||(laby[i][j] & DEPART)){
                i++;
                if (i>=n){i=0;j++;}
            }
            position_cour++;
            i++;
            if (i>=n){i=0;j++;}
        }
        while ((tab[i][j]==1)||(laby[i][j] & DEPART)){
                i++;
                if (i>=n){i=0;j++;}
            }
        tab[i][j]=1;
        ajoute_enemies(&liste_enemies,i,j);
    }
    return liste_enemies;
}

void afficher_enemies(enemies_t * liste_enemies, float zoom_ennemies, float pos_reel[2], int taille_case[2], int hauteur, int longueur, SDL_Texture* enemies, SDL_Renderer* renderer){
    SDL_Rect source = {0},                         // Rectangle définissant la zone de la texture à récupérer
            destination = {0},                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
            dimension_source = {0};

    SDL_QueryTexture(enemies, NULL, NULL,
                    &dimension_source.w, &dimension_source.h);       // Récupération des dimensions de l'image
    source.w=dimension_source.w/9;
    source.h=dimension_source.h/4;
    enemies_t * cour = liste_enemies;
    while(cour!=NULL) {
        
        source.x=(((int)cour->frame) + 3*cour->type)*source.w;
        if (cour->dir==-1){
            source.y=0;
        }
        else {source.y=cour->dir*source.h;}

        destination.x=(cour->x_reel-pos_reel[0])*taille_case[0]+longueur/2-zoom_ennemies*source.w/2;
        destination.y=(cour->y_reel-pos_reel[1])*taille_case[1]+hauteur/2-zoom_ennemies*source.h/2;
        destination.w=zoom_ennemies*source.w;
        destination.h=zoom_ennemies*source.h;

        SDL_RenderCopy(renderer, enemies, &source, &destination);

        cour=cour->suivant;

    }
}

float distance2(float x1, float x2, float y1, float y2){
    return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
}

int mise_a_jour_enemies(enemies_t * liste_enemies, int pos_final[2], float pos_reel[2], int n, int p, int ** mon_laby){
    int tuer = 0;
    float pas_frame = 0.1;
    float pas_deplacement = 0.01;
    float prec = 0.05;
    int i,x,y;
    int temp = 0;
    int *chemin = NULL;
    enemies_t * cour = liste_enemies;
    float vitesse;
    float d_enemie_joueur;
    while(cour!=NULL){
        if (1 && distance2(cour->x,cour->x_reel,cour->y,cour->y_reel)<prec*1.5){
            /*l'enemie est arrive a sa destination finale, il faut determiné sa nouvelle destination en fonction de son type */
            if (cour->x!=pos_final[0] || cour->y!=pos_final[1]){

                /*Si l'enemie n'est pas sur le personnage, on calcule une nouvelle direction*/
                d_enemie_joueur = distance2(pos_reel[0],cour->x_reel,pos_reel[1],cour->y_reel);
                if (cour->type==2){
                    cour->dir = nouvelle_direction_aleatoire(mon_laby[cour->x][cour->y], cour->dir);
                }
                if (cour->type==0 || cour->type==1){//0 ET 1
                    if  ((cour->type==0 && d_enemie_joueur<8) || (cour->type==1 && d_enemie_joueur<16)){
			            if (cour->type==0){
                            chemin = astar_mat(mon_laby, p, n, np_to_k(cour->x, cour->y, n), np_to_k(pos_final[0], pos_final[1], n), 3, &i);
                        }
                        if (cour->type==1){
                            chemin = astar_mat(mon_laby, p, n, np_to_k(cour->x, cour->y, n), np_to_k(pos_final[0], pos_final[1], n), 1, &i);
                        }
			
			            if (i < (N_LABY * P_LABY)+1){
                            k_to_np(chemin[i+1],&x,&y,n);
                            nettoyage(mon_laby,p,n,&temp,&temp);
                            free(chemin);
                            chemin = NULL;
                            if (cour->x<x){cour->dir = 2;}
                            if (cour->x>x){cour->dir = 3;}
                            if (cour->y<y){cour->dir = 0;}
                            if (cour->y>y){cour->dir = 1;}
                        } else {
                            printf("Erreor\n");
                        } 
                    }
                    else {
                        cour->dir = nouvelle_direction_aleatoire(mon_laby[cour->x][cour->y], cour->dir);
                    }
                    
                }
            }
            else {
                cour->dir = -1;
            }
            /*mise a jour de la position d'arrivee de l'enemie en fonction de la nouvelle direction*/
            // 0=SUD, 1=NORD, 2=EST, 3=OUEST
            if (cour->dir==0){cour->y = cour->y + 1;}
            if (cour->dir==1){cour->y = cour->y - 1;}
            if (cour->dir==2){cour->x = cour->x + 1;}
            if (cour->dir==3){cour->x = cour->x -1;}
        }
         /*detection de colision avec le personnage */
        if (distance2(pos_reel[0],cour->x_reel,pos_reel[1],cour->y_reel)<0.35){
            tuer = 1;
        }
        /*mise a jour de la position reel de l'enemie et de sa frame d'animation */
        if (cour->dir==-1){
            cour->frame = 0;
        }
        else {
            cour->frame = cour->frame + pas_frame;
            if (cour->frame>=3){cour->frame -=3;};
        }
        
        if (cour->type==0){vitesse=0.8;}
        if (cour->type==1){vitesse=1.1;}
        if (cour->type==2){vitesse=1.6;}

        if (cour->x_reel > cour->x + prec){cour->x_reel = cour->x_reel -  pas_deplacement*vitesse;}
        if (cour->x_reel < cour->x - prec){cour->x_reel = cour->x_reel +  pas_deplacement*vitesse;}
        if (cour->y_reel > cour->y + prec){cour->y_reel = cour->y_reel -  pas_deplacement*vitesse;}
        if (cour->y_reel < cour->y - prec){cour->y_reel = cour->y_reel +  pas_deplacement*vitesse;}
        cour=cour->suivant;
        }
        return tuer;
}

int nouvelle_direction_aleatoire(int mur, int dir){
    int nouv_dir = -1;
    int n_directions_libres;
    int de = 0;
    n_directions_libres = 4 -((mur & NORD)!=0) -((mur & EST)!=0)- ((mur & SUD)!=0) - ((mur & OUEST)!=0);
    if (n_directions_libres>=2){
        de = rand()%(n_directions_libres-1);
        if (de==0 && (!(mur&NORD)) && dir!=0){
            nouv_dir = 1;
        }
        else {
            if ((!(mur&NORD)) && dir!=0){de--;}
            if (de==0 && (!(mur&EST)) && dir!=3){
                nouv_dir = 2;
            }
            else {
                if ((!(mur&EST)) && dir!=3){de--;}
                if (de==0 && (!(mur&SUD)) && dir!=1){
                    nouv_dir = 0;
                }
                else {
                    nouv_dir = 3;
                    }
            }
        }
    }
    else {
        if (n_directions_libres==1){
            if (dir==1){nouv_dir = 0;}
            if (dir==2){nouv_dir = 3;}
            if (dir==0){nouv_dir = 1;}
            if (dir==3){nouv_dir = 2;}
            if (dir==-1){
                if (!(mur&NORD)){nouv_dir = 1;}
                if (!(mur&EST)){nouv_dir = 2;}
                if (!(mur&SUD)){nouv_dir = 0;} 
                if (!(mur&OUEST)){nouv_dir = 3;} 
            }
        }
    }
    return nouv_dir;
}


void tire_laser_detruit_enemies(enemies_t **ptr_liste_enemies, explosion_t **liste_explosion, tirs_t *ptr_mes_tirs){
    enemies_t *cour_enemie = *ptr_liste_enemies;
    int i;
    float prec = 0.5;
    while (cour_enemie!=NULL){
        for(i = 0;i < ptr_mes_tirs->droite ;i++){
            if (distance2(cour_enemie->x_reel,ptr_mes_tirs->tab[i].posx,cour_enemie->y_reel,ptr_mes_tirs->tab[i].posy)<prec){
                supprime_enemies(ptr_liste_enemies,cour_enemie->x_reel,cour_enemie->y_reel);
                ajoute_explosion(liste_explosion,cour_enemie->x_reel,cour_enemie->y_reel);
                supprime_tir(ptr_mes_tirs,i);
                cour_enemie = NULL;
                break;
            }
        }
        if (cour_enemie!=NULL){cour_enemie = cour_enemie->suivant;}
    }
}

void mise_a_jour_explosion(explosion_t ** liste_explosion){
    explosion_t *cour = *liste_explosion;
    explosion_t ** prec = liste_explosion;
    while (cour!=NULL){
        if (cour->frame>=9){
            *prec=cour->suivant; 
            free(cour);
            cour=*prec;
        }
        else {
            cour->frame=cour->frame+0.3;
            prec=&(cour->suivant);
            cour=*prec;
        }
    }
}

void ajoute_explosion(explosion_t ** liste_explosion, float x, float y){
    explosion_t * nouvelle_explosion = malloc(sizeof(explosion_t));
    nouvelle_explosion->x_reel = x;
    nouvelle_explosion->y_reel = y;
    nouvelle_explosion->suivant = *liste_explosion;
    *liste_explosion = nouvelle_explosion;
}

void afficher_explosion(explosion_t * liste_explosion, float zoom_explosion, float pos_reel[2], int taille_case[2], int hauteur, int longueur, SDL_Texture* explosion, SDL_Renderer* renderer){
    SDL_Rect source = {0},                         // Rectangle définissant la zone de la texture à récupérer
            destination = {0},                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
            dimension_source = {0};

    SDL_QueryTexture(explosion, NULL, NULL,
                    &dimension_source.w, &dimension_source.h);       // Récupération des dimensions de l'image
    source.w=dimension_source.w/10;
    source.h=dimension_source.h;
    explosion_t * cour = liste_explosion;
    while(cour!=NULL) {
        
        source.x=((int)(cour->frame))*source.w;
        source.y=0;
        destination.x=(cour->x_reel-pos_reel[0])*taille_case[0]+longueur/2-zoom_explosion*source.w/2;
        destination.y=(cour->y_reel-pos_reel[1])*taille_case[1]+hauteur/2-zoom_explosion*source.h/2;
        destination.w=zoom_explosion*source.w;
        destination.h=zoom_explosion*source.h;

        SDL_RenderCopy(renderer, explosion, &source, &destination);

        cour=cour->suivant;

    }
}

void libere_liste_explostion(explosion_t ** adprec){
    explosion_t * tmp;
    while ((*adprec) != NULL)
    { 
        tmp = (*adprec) -> suivant;
        free(*adprec);
        *adprec = tmp;
    }
}
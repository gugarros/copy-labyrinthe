#ifndef __GARROS_HANOCQ_PILE_C
#define __GARROS_HANOCQ_PILE_C

#include "pile.h"



pile_t * initPile(int taille) {
    pile_t * p = (pile_t *) malloc(sizeof(pile_t));
    if (p != NULL)
    {
        p -> taille = taille;
        p -> sommet = -1;
        element_pile_t * base = (element_pile_t*) malloc(sizeof(element_pile_t) * taille);
        p -> base = base;
        if (base == NULL) { // si erreur lors de l'alloc
            free(p);
            p = NULL;
        }
    }
    return p;
}

int estVidePile(pile_t * p) {
    return (p -> sommet == -1);
}

void empiler(pile_t * p, element_pile_t val ,int * code) {
    *code = 1;
    if (p->sommet < p -> taille -1) // si y a de la place
    {
        p -> sommet += 1;
        (p -> base)[p->sommet].nbr = val.nbr;
        *code = 0;
    }
}

void depiler (pile_t * p, element_pile_t *val ,int * code) {
    *code = 1;
    //*val = 0;
    if (! estVidePile(p)) // si pas vide
    {
        val-> nbr = (p -> base)[p->sommet].nbr;
        p -> sommet -= 1;
        *code = 0;
    }
}

void libererPile(pile_t * p) {
    free(p->base);
    free(p);
    p = NULL;
}

void afficherPile(pile_t * p) {
    int sommet = p -> sommet;
    printf("Affichage de la Pile\n");
    while (sommet > -1)
    {
        printf("%d\n",(p -> base)[sommet].nbr );
        sommet--;
    }
}

#endif

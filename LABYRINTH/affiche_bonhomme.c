void affiche_bonhomme(int frame_bonhomme, int direction_bonhomme,int hauteur, int longueur, float zoom, SDL_Texture* bonhomme, SDL_Renderer* renderer){
    SDL_Rect source = {0},                         // Rectangle définissant la zone de la texture à récupérer
            destination = {0},                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
            dimension_source = {0};

    SDL_QueryTexture(tuiles, NULL, NULL,
                    &dimension_source.w, &dimension_source.h);       // Récupération des dimensions de l'image

    source.x=direction*dimension_source.w/4;
    source.y=frame_bonhomme*dimension_source.h/4;
    source.w=dimension_source.w/4;
    source.h=dimension_source.h/4;

    destination.x=(-zoom*source.w + largeur)/2;
    destination.y=(-zoom*source.h + longueur)/2;
    destination.w=zoom*source.w;
    destination.h=zoom*source.h;

    SDL_RenderCopy(renderer, bonhomme,
                    &source,
                    &destination); 
}

#ifndef __GARROS_HANOCQ_BOUDIN_DISTANCE_C
#define __GARROS_HANOCQ_BOUDIN_DISTANCE_C

int arrondir(float x){
    int res=(int) x ;
    float y = x - res;
    if( y>0.5) res += 1;
    return res;
}


int min(int a,int b){
    int res=b;
    if (b>a){
        res=a;
    }
    return res;
}

int max(int a,int b){
    int res=b;
    if (b<a){
        res=a;
    }
    return res;
}


float distance(int x1, int x2, int y1, int y2, int distance){
    //distance == 1 -> distance euclidienne (  au carree)
    //distance == 2 -> distance Tchebychev
    //distance == 3 ou 0 (enfin autre ) -> distance Manhattan
    float res;
    if (distance==1){
        res=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
    }
    else{
        if (distance==2){
            res=max(abs(x1-x2),abs(y1-y2));
        }
        else{
            res=abs(x1-x2)+abs(y1-y2);
        }
    }
    return res;
}



#endif
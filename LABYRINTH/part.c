#include "part.h"



part_t * init_part(int taille){
    int i;
    part_t * maPart = (part_t *) malloc(sizeof(part_t));
    if (maPart != NULL){
        maPart -> taille = taille;
        maPart -> tab = (int *) malloc(sizeof(int) * taille);
        if (maPart -> tab != NULL){
            for( i = 0;i<taille;++i){
                maPart ->tab[i] = i;
            }
            maPart -> haut = (int *) malloc(sizeof(int) * taille);
            if (maPart -> tab != NULL){
                for( i = 0;i<taille;++i){
                    maPart ->haut[i] = 0;
                }
            } else{
                printf("Erreur alloc part haut\n");
                free(maPart -> tab);
                free(maPart);
                maPart = NULL;
            }
        } else {
            printf("Erreur alloc part tab\n");
            free(maPart);
            maPart = NULL;
        } 
    } else {
        printf("Erreur alloc part\n");
    }
    return maPart;
}

void free_part(part_t * maPart){
    free(maPart ->tab);
    free(maPart ->haut);
    free(maPart);
}

int racine_part(part_t * maPart, int i){
    while (i != maPart->tab[i]){
        i = maPart -> tab[i];
    }
    return i;
}



void fusioner_part(part_t *maPart,int i,int j){
    int root_i = racine_part(maPart, i);
    int root_j = racine_part(maPart, j);
    if (root_i != root_j){
        if (maPart ->haut [root_i] < maPart ->haut [root_j]){
            maPart -> tab [root_i] = root_j;
        } else{
            maPart -> tab [root_j] = root_i;
            if (maPart ->haut [root_i] == maPart ->haut [root_j]){
                maPart ->haut[root_i] = maPart ->haut[root_i] + 1;
            }
        }
    }
}

void dot_partition(part_t * maPart,char*nom,int generation){
    // generation == 1 -> genere  le dot.png
    // generation == 0 -> on le fais nous meme
    char * nom_dot = (char *) malloc(sizeof(char)* (strlen(nom)+4));
    strcpy(nom_dot,nom);
    strcat(nom_dot,".dot");
    FILE * fichier = fopen(nom_dot,"w");
    fprintf(fichier,"digraph {\n");
    for(int i = 0;i<maPart->taille;++i){
        fprintf(fichier, "  %d -> %d;\n",i,maPart->tab[i]);
    }
    fprintf(fichier,"}");
   
    fclose(fichier);


    if (generation){
        generation_dot(nom);
    } else {
        printf("Faite :\ndot -Tpng %s.dot -o %s.png\n",nom,nom);
    }
    free(nom_dot);
}

void afficher_partition(part_t *maPart){
    printf("Part :");
    for(int i = 0;i<maPart->taille ;++i){
        printf("%d ",maPart ->tab[i]);
    }
    printf("\nHaut :");
    for(int i = 0;i<maPart-> taille;++i){
        printf("%d ",maPart->haut[i]);
    }
    printf("\n");
}

ele_t * lister_classe(part_t *maPart,int x){
    int root_x = racine_part(maPart,x);
    ele_t * maList = NULL;
    printf("classe %d:",x);
    for(int i=0;i<maPart->taille;++i){
        if(racine_part(maPart,i)==root_x){
            insercellule(&maList,creCellule(i));
            //printf("%d ",i);
        }
    }
    printf("\n");
    return maList;
    //PENSER A LIBERER
}


ele_t ** lister_partition(part_t *maPart,int *n){
    *n = maPart ->taille;
    ele_t ** tab = (ele_t**) malloc(sizeof(ele_t*)* maPart->taille);
    int i;
    if (tab != NULL){
        for(i=0;i<*n;++i){
            tab[i] = NULL;
        }
        for(i=0;i<*n;++i){
            insercellule(&tab[racine_part(maPart,i)],creCellule(i));
        }
    }
    return tab;
}

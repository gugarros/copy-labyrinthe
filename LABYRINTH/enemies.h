#ifndef __GARROS_HANOCQ_BOUDIN_ENEMIES_H
#define __GARROS_HANOCQ_BOUDIN_ENEMIES_H

#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "labyrinth.h"
#include "tirer.h"


typedef struct enemies {
    int type;
    float frame;
    int x;
    int y;
    float x_reel;
    float y_reel;
    int dir;
    struct enemies * suivant;
}enemies_t;

typedef struct explosion {
    float x_reel;
    float y_reel;
    float frame;
    struct explosion * suivant;
} explosion_t;


void ajoute_enemies(enemies_t ** ptr_liste_enemies, int x, int y);

void supprime_enemies(enemies_t ** ptr_liste_enemies, float x_reel, float y_reel);

void libere_enemies(enemies_t ** adprec);

void nuke_enemies(enemies_t ** adprec, explosion_t **liste_explosion, int pos_final[2], int n, int p);

enemies_t * genere_liste_enemies(int n_enemies, int n, int p, int ** mon_laby);

void affiche_enemies(enemies_t * liste_enemies, float pos_reel[2], int hauteur, int longueur, float zoom, SDL_Texture* enemies, SDL_Renderer* renderer);

int mise_a_jour_enemies(enemies_t * liste_enemies, int pos_final[2], float pos_reel[2], int n, int p, int ** mon_laby);

float distance2(float x1, float x2, float y1, float y2);

int nouvelle_direction_aleatoire(int mur, int dir);

void tire_laser_detruit_enemies(enemies_t **ptr_liste_enemies, explosion_t **liste_explosion, tirs_t *ptr_mes_tirs);

void mise_a_jour_explosion(explosion_t ** liste_explosion);

void afficher_explosion(explosion_t * liste_explosion, float zoom_explosion, float pos_reel[2], int taille_case[2], int hauteur, int longueur, SDL_Texture* explosion, SDL_Renderer* renderer);

void ajoute_explosion(explosion_t ** liste_explosion, float x, float y);

void libere_liste_explostion(explosion_t ** adprec);

#endif

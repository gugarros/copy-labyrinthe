/*******************************************************/
/* Gestion de la liste chainee: operations elementaires*/
/*******************************************************/

#include "lchbis.h"

/* adjonction d'une cellule*/
void insercellule_bis(element_t **prec, element_t *nouv)
{nouv->suivant=*prec;
  *prec=nouv;
}

/*suppression d'une cellule*/
void suppcellule_bis(element_t **prec)
{element_t *cour=*prec;
  *prec=(*prec)->suivant; 
  free(cour);
}


/* recherche du precedent sur le cout pour liste triee en decroissant*/
element_t ** rechprec_bis(element_t **adtete, int val)
{ element_t *cour, **prec;
  cour=*adtete;
  prec=adtete;
  while (cour!=NULL && (cour->couple.val < val))
    {prec=&cour->suivant;
     cour=*prec;
    }
  return prec;
}

void insertion_trie_bis(element_t **l,element_t * cour){
  element_t ** prec = rechprec_bis(l,cour ->couple.val);
  insercellule_bis(prec,cour);
}

/*allocation d'une cellule et initialisation*/
element_t * creCellule_bis(int n1, int n2, float v)
{
	element_t * nouv=(element_t*) malloc(sizeof(element_t));
	if (nouv)
	{
	nouv->couple.noeud1=n1;
    nouv->couple.noeud2=n2;
    nouv->couple.val=v;
	nouv->suivant=NULL;
	}
	return nouv;
}


/* affichage liste chainee*/

void afficheliste_bis(element_t * tete)
{element_t * cour;
  cour=tete;
  if (cour!=NULL)
    {
      while(cour!=NULL)
	{
	  printf("(%d,%d) poids : %f\n",cour->couple.noeud1, cour->couple.noeud2, cour->couple.val);
	  cour=cour->suivant;
	}
	  printf("fin de la liste\n");
    }
  else printf("liste vide\n");
}




void freeliste_bis(element_t ** l){
  element_t * tmp;
  while ((*l) != NULL)
  { 
    tmp = (*l) -> suivant;
    free(*l);
    *l = tmp;
  }
}


int longueur_bis(element_t * liste){
  int s=0;
  element_t * cour = liste;
  while (cour != NULL){
    s+=1;
    cour = cour -> suivant;
  }
  return s;
}

element_t **va_chercher(element_t **adtete,int n){
  element_t *cour, **prec;
  cour=*adtete;
  prec=adtete;
  while (cour!=NULL && n>0){
      prec=&cour->suivant;
      cour=*prec;
      n-=1;
    }
  return prec;
}

// element_t ** va_chercher(element_t * liste, int n){
//   printf("cour : %d",n);
//   while (n!=0){
//     (*liste) = (*liste) -> suivant;
//     n-=1;
//   }
//   return liste;
// }
/*recherche d un element sans critere de tri (ici pour l'usine)*/

/*
ele_t ** rechusine( ele_t ** adtete, int usine)
{ ele_t *cour, **prec;
  cour=*adtete;
  prec=adtete;
  while (cour!=NULL && cour->usine != usine)
    {prec=&cour->suivant;
      cour=*prec;
    }
  return prec;
}
*/

/* suvegarde dans un fichier*/
/*
void sauv ( ele_t * tete, char * nomfic)
{
ele_t * cour;
FILE * fic;
fic = fopen(nomfic, "w");
cour=tete;
while (cour!= NULL)
{
fprintf(fic,"%d %d %d\n",cour->cout, cour->usine, cour->periode);
cour=cour->suivant;
}
fclose(fic);
}
*/
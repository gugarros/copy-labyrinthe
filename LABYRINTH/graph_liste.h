#ifndef __GARROS_HANOCQ_BOUDIN_GRAPH_LISTE_H
#define __GARROS_HANOCQ_BOUDIN_GRAPH_LISTE_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lchbis.h"
#include "distance.c"
#include "dot_generation.c"
#include "part.h"
#include "tasBis.h"
#include "tas.h"

typedef struct graph_liste
{
    int n; //nb de noeud
    element_t * liste; //liste d'arêtes
} graph_liste_t;

graph_liste_t * init_graph_liste_alea(int n, float p);

void dot_graph_liste(graph_liste_t * monGraph,char *nom,int connexe,int generation);

part_t * composante_connexe_graph_liste(graph_liste_t * monGraph);

graph_liste_t* copie_graph_liste(graph_liste_t * original, ele_t * listek);

void dot_composante_connexe_graph_liste(graph_liste_t *monGraph,int generation);

part_t * kruskal(graph_liste_t * monGraph,char *nom,int generation);

void free_graph_liste(graph_liste_t * monGraph);

void k_to_np(int k,int *i,int *j,int n);

int np_to_k(int i,int j,int n);

graph_liste_t * init_laby_liste(int n, int p);

element_t * Fisher_Yate(element_t ** aretes);

int mini(int * P, int n);

graph_liste_t * kruskal_graph(graph_liste_t * monGraph,float proba);

graph_liste_t * dijkstra(int origine, int arrivee, graph_liste_t * G);

graph_liste_t *  astar(int origine, int arrivee, graph_liste_t * G, int n, int dist);

#endif

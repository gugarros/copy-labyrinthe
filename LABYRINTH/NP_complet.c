#include "labyrinth.h"
#include "enemies.h"

#define N_TEST_MAX 10000000


void swap(int *tab,int i, int j)
{
    int temp = tab[i];
    tab[i]=tab[j];
    tab[j] = temp;
}
  

int * find_all_coins(int **laby,int n,int p,int depart, int arrivee, int * nb){
    int i,j;
    int nb_pieces =1;
    int pos_coins[NBR_COINS+2];
    pos_coins[0] = depart;
    for(j=0;j<p;++j){
        for(i=0;i<n;++i){
            if (laby[i][j]& COINS){
                pos_coins[nb_pieces] = np_to_k(i,j,n);
                //printf("pos %d\n",pos_coins[nb_pieces]);
                nb_pieces++;
                
            }
        }
    }
    pos_coins[nb_pieces] = arrivee;
    //printf("Il y a %d pieces\n",nb_pieces-1);
    nb_pieces+=1;
    *nb = nb_pieces;
    float distance_pieces[NBR_COINS+2][NBR_COINS+2];
    int *chemin = NULL;
    int indice;
    int numDij = 0;
    int numA = 0;
    for(i=0;i<nb_pieces;++i){
        for(j=i+1;j<nb_pieces;++j){
            chemin = astar_mat(laby,p,n,pos_coins[i],pos_coins[j],1,&indice);
            distance_pieces[i][j] = n*p-indice;
            distance_pieces[j][i] = distance_pieces[i][j];
            nettoyage(laby,p,n,&numDij,&numA);
            numDij=0;
            numA=0;
            free(chemin);
            chemin = NULL;
        }
    }
    for(i=0;i<nb_pieces;++i){
        for(j=0;j<nb_pieces;++j){
            //printf("%d ",(int) distance_pieces[i][j]);
        }
        //printf("\n");
    }
    int minii[NBR_COINS+2];
    int cour[NBR_COINS+2];
    int k;
    float dist=0;
    float dist_min=0;
    for(k=0;k<nb_pieces;++k){
        minii[k]=k;
        cour[k]=k;
    }
    for(k=0;k<nb_pieces-1;++k){
        dist_min += distance_pieces[minii[k]][minii[k+1]];
    }
    int boucle_cour;
    for(k=0;k<N_TEST_MAX;++k){
        for(boucle_cour=0;boucle_cour < NBR_COINS;boucle_cour++){
            swap(cour,1+rand()%(nb_pieces-2),1+rand()%(nb_pieces-2));
        }
        dist = 0;
        for(i=0;i<nb_pieces-1;++i){
            dist += distance_pieces[cour[i]][cour[i+1]];
        }
        if (dist < dist_min){
            for(i=0;i<nb_pieces;++i){
                minii[i]=cour[i];
            }
            dist_min =dist;
        }
    }
    int * ordre = (int*) malloc(sizeof(int)*nb_pieces);
    for(i=0;i<nb_pieces;++i){
        ordre[i] = pos_coins[minii[i]];
        printf("%d\n",ordre[i]);
    }
    return ordre;
}

int astar_mat_ia(int ** laby, int lignes, int colonnes, int origine, int arrivee, int dist,int *indice_noeud,enemies_t * liste_enemies, float * distance_chemin){
    int nbr_noeuds = lignes*colonnes;
    int x,y;
    k_to_np(arrivee,&x,&y,colonnes);

    float * P1 = (float*) malloc(sizeof(float)*(nbr_noeuds)); // liste des distances du point initial par sommet (-1 pour ceux non traversé) 
    int * P2 = (int*) malloc(sizeof(int)*(nbr_noeuds)); // liste des prédécesseurs
    int i,j;
    for(i=0;i<nbr_noeuds;i++){
        P1[i]=-1;
    }
    k_to_np(origine,&i,&j,colonnes);
    P1[origine]=distance(x,i,y,j,dist);
    P2[origine]=origine;
    noeud_tas_t * a_mini = (noeud_tas_t *) malloc(sizeof(noeud_tas_t));
    noeud_tas_t * neu = (noeud_tas_t *) malloc(sizeof(noeud_tas_t));
   
    neu->noeud = origine;
    neu->poids = P1[origine];
    tasBis_t * monTas = init_tasBis(nbr_noeuds*nbr_noeuds+2);
    insere_tasBis(monTas,*neu);
    int n;
    int d;
    enemies_t * enemies_cour = NULL;
    while ((monTas ->taille >0) && (a_mini->noeud!=arrivee)){
        //affiche_tasBis(monTas);
        enemies_cour = liste_enemies;
        *a_mini = extraitminBis(monTas);
        k_to_np(a_mini->noeud,&i,&j,colonnes);
        //printf("%d %d\n",i,j);
        d = 1;
        while (enemies_cour != NULL){
            if ( 0.8 > distance2(enemies_cour->x,i,enemies_cour ->y,j)){
                d += nbr_noeuds;
                //printf("Enemie sir le chemin\n");
            }
            enemies_cour = enemies_cour->suivant;
        }
        if (!(laby[i][j]&EST_PASSE_A)){
            if ((!(laby[i][j] & EST))&&(!(laby[i+1][j] & EST_PASSE_A))){
                n=np_to_k(i+1,j,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +d)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+d+distance(x,i+1,y,j,dist)-distance(x,i,y,j,dist);
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            if ((!(laby[i][j] & OUEST))&&(!(laby[i-1][j] & EST_PASSE_A))){
                n=np_to_k(i-1,j,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +d)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+d+distance(x,i-1,y,j,dist)-distance(x,i,y,j,dist);
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            if ((!(laby[i][j] & SUD))&&(!(laby[i][j+1] & EST_PASSE_A))){
                n=np_to_k(i,j+1,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +d)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+d+distance(x,i,y,j+1,dist)-distance(x,i,y,j,dist);
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            if ((!(laby[i][j] & NORD))&&(!(laby[i][j-1] & EST_PASSE_A))){
                n=np_to_k(i,j-1,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +d)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+d+distance(x,i,y,j-1,dist)-distance(x,i,y,j,dist);
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            laby[i][j]+=EST_PASSE_A;
        }
    }
    //printf("%f\n",P1[arrivee]);
    * distance_chemin = P1[arrivee];
    if (monTas ->taille ==0) printf("VIDEEEEEE\n");
    if (P1[arrivee] >nbr_noeuds) {
        //printf("Il y a au moins un enemis\n");
    }
    free(a_mini);
    free(neu);
    free_tasBis(monTas);
    free(P1);
    
    nettoyage(laby,lignes,colonnes,&i,&j);
    int * copie = renverser_predesc(P2,nbr_noeuds,origine,arrivee,indice_noeud);
    free(P2);
    int noeud_suiv = copie[*indice_noeud+1];
    free(copie);
    return noeud_suiv;
}


int find_closest_coin(int **laby,int n,int p,int depart,int arrivee,enemies_t * liste_enemies){
    int i,j;
    int nb_pieces =1;
    int pos_coins[NBR_COINS+1];
    pos_coins[0] = depart;
    for(j=0;j<p;++j){
        for(i=0;i<n;++i){
            if (laby[i][j]& COINS){
                pos_coins[nb_pieces] = np_to_k(i,j,n);
                //printf("pos %d\n",pos_coins[nb_pieces]);
                nb_pieces++;           
            }
        }
    }
    if (nb_pieces ==1) {
        //printf("Plus de pices\n");
        pos_coins[1] = arrivee;
        nb_pieces ++;
    }
    float distance_pieces[NBR_COINS+1];
    int indice;
    float dist_tempo;
    for(i=1;i<nb_pieces;++i){
        astar_mat_ia(laby,p,n,depart,pos_coins[i],1,&indice,liste_enemies,&dist_tempo);
        distance_pieces[i] = dist_tempo;
        //printf("dist pices %f\n",distance_pieces[i]);
    }
    float dist_min = distance_pieces[1];
    int indice_min = 1;
    for(i=2;i<nb_pieces;++i){
        if (dist_min > distance_pieces[i]){
            dist_min = distance_pieces[i];
            indice_min = i;
        }
    }
    //printf("Piece la plus proche %d\n",pos_coins[indice_min]);
    return pos_coins[indice_min];
}



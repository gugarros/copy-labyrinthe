#ifndef __GARROS_HANOCQ_BOUDIN_COINS_H
#define __GARROS_HANOCQ_BOUDIN_COINS_H

#include <stdlib.h>

typedef struct coin {
    int x;
    int y;
    struct coin * suivant;
}coin_t;


void ajoute_coin(coin_t ** liste_coin, int x, int y);

void supprime_coin(coin_t ** liste_coin, int x, int y);

coin_t * genere_liste_coin(int n_coin, int n, int p);

void affiche_chaine(coin_t * liste_coin);

#endif

#include "coins.h"
#include "stdio.h"

void ajoute_coin(coin_t ** liste_coin, int x, int y){
    coin_t * nouveau_coin = malloc(sizeof(coin_t));
    nouveau_coin->x = x;
    nouveau_coin->y = y;
    nouveau_coin->suivant = *liste_coin;
    *liste_coin = nouveau_coin;
}

void supprime_coin(coin_t ** liste_coin, int x, int y){
    coin_t * cour = *liste_coin;
    coin_t ** prec = liste_coin;
    while(cour!=NULL){
        if (cour->x==x && cour->y==y){
            printf("he hop, c'est supprime!  %p\n",(*prec)->suivant);
            *prec=cour->suivant; 
            free(cour);
            cour=NULL;
        }
        else {
            prec = &(cour->suivant);
            cour = cour->suivant;
        }
    }
}

void affiche_chaine(coin_t * liste_coin){
    coin_t * cour = liste_coin;
    printf("voici la liste:\n");
    printf("%p\n",liste_coin);
    while(cour!=NULL){
        printf("%p  x=%d; y=%d\n",cour->suivant,cour->x,cour->y);
        cour=cour->suivant;
        }
}

coin_t * genere_liste_coin(int n_coin, int n, int p){
    int tab[n][p];
    coin_t * liste_coin = NULL;
    int i,j,k,position,position_cour;
    for(i=0;i<n;i++){
        for(j=0;j<p;j++){
            tab[i][j]=0;
        }
    }
    for(k=0;k<n_coin;k++){
        position=rand()%((n*p)-k);
        position_cour=0;
        i=0;
        j=0;
        while(position_cour<position){
            while (tab[i][j]==1){
                i++;
                if (i>=n){i=0;j++;}
            }
            position_cour++;
            i++;
            if (i>=n){i=0;j++;}
        }
        while (tab[i][j]==1){
                i++;
                if (i>=n){i=0;j++;}
            }
        tab[i][j]=1;
        //printf("%d  %d  %d  %d\n",position,position_cour,i,j);
        ajoute_coin(&liste_coin,i,j);
    }
    return liste_coin;
}

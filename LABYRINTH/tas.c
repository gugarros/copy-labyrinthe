#include <stdlib.h>
#include <stdio.h>
#include "tas.h"


tas_t*  init_tas(int tailleMax){
    tas_t * monTas = (tas_t *) malloc(sizeof(tas_t));
    if (monTas != NULL) {
        monTas -> tailleMax = tailleMax;
        monTas -> taille = 0;
        monTas -> tab = (int*) malloc(sizeof(int)*tailleMax);
        if (monTas -> tab != NULL) {
            //printf("ok alloc tas\n");
        } else {
            printf("Erreur alloc tas ( tab)\n");
            free(monTas);
        }
        
    } else {
        printf("Erreur alloc tas\n");
    }
    return monTas;
}

void echange_tas(tas_t * monTas,int i,int j){
    int temp = monTas -> tab[i];
    monTas -> tab[i] = monTas-> tab[j];
    monTas-> tab[j] = temp;
}


void insere_tas(tas_t * monTas, int x){
    //printf("insere : %d\n",x);
    if (monTas -> taille < monTas ->tailleMax){
        int k = monTas -> taille;
        monTas -> tab[k] = x;
        while ((k > 0) && (monTas -> tab[(k-1)/2] > monTas -> tab[k] )){
            echange_tas(monTas,(k-1)/2,k);
            k = (k-1)/2;
        }
        monTas -> taille = monTas -> taille + 1;
    } else {
        printf("Erreur tas trop petit lors insere %d\n",x);
    }
}


void entasse_tas(tas_t * monTas, int i){
    int fg = 2*i+1;
    int fd = fg + 1;
    int mini = i;
    if ((fg < monTas ->taille) && (monTas -> tab[fg] < monTas ->tab[i])){
        mini =fg;
    }
    if ((fd < monTas -> taille) && (monTas -> tab[fd] < monTas->tab[mini])){
        mini = fd;
    }

    if (i != mini){
        echange_tas(monTas,i,mini);
        entasse_tas(monTas,mini);
    }
}

int extraitmin(tas_t * monTas){
    int mini = monTas -> tab[0];
    if (monTas ->taille >0){
        echange_tas(monTas,0,monTas -> taille-1);
        monTas -> taille = monTas ->taille -1;
        entasse_tas(monTas,0);
    } else {
        printf("Erreur extrai min\n");
    }
    return mini;
}


void tri_par_tas(int * tab,int taille){
    tas_t * monTas = init_tas(taille);
    for(int i =0;i<taille;++i){
        insere_tas(monTas,tab[i]);
    }
    for(int i =0;i<taille;++i){
        tab[i] = extraitmin(monTas);
    }
    free(monTas);
}



void free_tas(tas_t * monTas){
    free(monTas -> tab);
    free(monTas);
}


void affiche_tas(tas_t * monTas){
    for(int i = 0;i<monTas->taille;++i){
        printf("%d ",monTas->tab[i]);
    }
    printf("\n");
}


void affiche_tab(int * tab, int taille){
    for(int i = 0;i<taille;++i){
        printf("%d ",tab[i]);
    }
    printf("\n");
}
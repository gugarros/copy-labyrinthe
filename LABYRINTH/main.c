#include "graph_liste.c"
#include "part.c"
#include "lch.c"
#include "lchbis.c"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>


#define NORD 1
#define OUEST 2
#define SUD 4
#define EST 8
#define DEPART 16
#define ARRIVEE 32

int ** laby_liste_to_laby_mat(int n, int p, graph_liste_t * mon_laby_liste){

    int i,j;

    int ** mon_laby_mat = (int **) malloc(sizeof(int*) *n);
        if (mon_laby_mat != NULL) {
            for(i=0;i<n;++i){
                mon_laby_mat [i] = (int *) malloc(sizeof(int) * p);
            }

        } else {
            printf("Erreur\n");
            free(mon_laby_mat);
            mon_laby_mat = NULL;
        }

    for(i=0;i<n;i++){
        for(j=0;j<p;j++){
            mon_laby_mat[i][j]=NORD+OUEST+SUD+EST;// tous les murs
        }
    }

    element_t * cour = mon_laby_liste->liste;
    int i1,i2,j1,j2;
    
    while (cour!=NULL){
        k_to_np((cour->couple).noeud1, &i1, &j1, n);
        k_to_np((cour->couple).noeud2, &i2, &j2, n);
        //on enleves les murs
        if (j1>j2){
            mon_laby_mat[i1][j1] = mon_laby_mat[i1][j1]- NORD;
            mon_laby_mat[i2][j2] = mon_laby_mat[i2][j2]- SUD;
        }
        if (i1<i2){
            mon_laby_mat[i1][j1] = mon_laby_mat[i1][j1]- OUEST;
            mon_laby_mat[i2][j2] = mon_laby_mat[i2][j2]- EST;
        }
        if (j1<j2){
            mon_laby_mat[i1][j1] = mon_laby_mat[i1][j1]- SUD;
            mon_laby_mat[i2][j2] = mon_laby_mat[i2][j2]- NORD;
        }
        if (i1>i2){
            mon_laby_mat[i1][j1] = mon_laby_mat[i1][j1]- EST;
            mon_laby_mat[i2][j2] = mon_laby_mat[i2][j2]- OUEST;
        }
        cour=cour->suivant;
    }
    return mon_laby_mat;
}

void afficher_laby(int ** mon_laby, int largeur, int longueur, int n, int p, float zoom, SDL_Texture* tuiles,SDL_Window* window,SDL_Renderer* renderer){

    SDL_Rect source = {0},                         // Rectangle définissant la zone de la texture à récupérer
            window_dimensions = {0},               // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
            destination = {0},                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
            dimension_source = {0};

    SDL_GetWindowSize(
        window, &window_dimensions.w,
        &window_dimensions.h);                    // Récupération des dimensions de la fenêtre

    SDL_QueryTexture(tuiles, NULL, NULL,
                    &dimension_source.w, &dimension_source.h);       // Récupération des dimensions de l'image
    
    printf("dim images %d %d\n",dimension_source.w,dimension_source.h);
    int i,j;

    for(i=0;i<n;i++){
        for(j=0;j<p;j++){
            source.x=mon_laby[i][j]*dimension_source.w/16;
            source.y=0;
            source.w=dimension_source.w/16;
            source.h=dimension_source.h;
            //printf("%d %d %d %d\n",source.x, source.y, source.w, source.h);
            destination.x=i*zoom*source.w + (largeur - n*zoom*source.w)/2;
            destination.y=j*zoom*source.h + (longueur - p*zoom*source.h)/2;
            destination.w=zoom*source.w;
            destination.h=zoom*source.h;
        
            SDL_RenderCopy(renderer, tuiles, &source, &destination);
        }
    }
}

void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}

int main(int argc, char **argv) {
    (void)argc;
    (void)argv;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;

    SDL_DisplayMode screen;
    //screen.refresh_rate = 1;
    
    /*********************************************************************************************************************/  
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w, screen.h);

    /* Création de la fenêtre */
    float largeur = screen.w * 0.8;
    float longueur = screen.h * 0.8;
    printf("Résolution fenetre\n\tw : %f\n\th : %f\n", largeur, longueur);
    window = SDL_CreateWindow("Premier dessin",
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED, largeur,
                                longueur,
                                SDL_WINDOW_OPENGL);//SDL_WINDOW_RESIZABLE
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);


    /* Inititalisation image */
    int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    int initted= 0;

    initted = IMG_Init(flags);

    if((initted&flags) != flags) 
    {
        printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
    }
    /*initialisation du laby */

    int n=5,p=3;//longueur / hauteur
    float zoom = 1;


    graph_liste_t * mon_laby_liste = init_laby_liste(n,p);
    
    graph_liste_t * copie = kruskal_graph(mon_laby_liste);
    kruskal(mon_laby_liste,"_kruskal",1);//a mettre en commentaire si trop grand
    dot_graph_liste(mon_laby_liste,"_origi",-1,1);//a mettre en commentaire si trop grand
    dot_graph_liste(copie,"_copie",-1,1);//a mettre en commentaire si trop grand
    int ** mon_laby=laby_liste_to_laby_mat(n,p,copie);



    /*********************************************************************************************************************/
    /*                                     On dessine dans le renderer                                                   */

    SDL_SetRenderDrawColor(renderer,255,0,0,255);
    SDL_Rect rectangle;
    rectangle.x=0;
    rectangle.y=0;
    rectangle.w=largeur;
    rectangle.h=longueur;
    SDL_RenderFillRect(renderer,&rectangle);
    SDL_RenderPresent(renderer);
    
    //SDL_Surface * tuiles_surface = IMG_Load("./img/TILES.png");
    //SDL_Texture * tuiles = SDL_CreateTextureFromSurface(renderer, tuiles_surface);
    SDL_Texture * tuiles = IMG_LoadTexture(renderer,"./img/TILES.png");
    if (tuiles == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);

    afficher_laby(mon_laby, largeur, longueur, n, p, zoom, tuiles, window, renderer);
    int i,j;
    for(j=0;j<p;j++){
        for(i=0;i<n;i++){
            printf("%d ",mon_laby[i][j]);
        }
        printf("\n");
    }
    for(int k = 0; k < n*p;++k){
        k_to_np(k,&i,&j,n);
        printf("%d ",mon_laby[i][j]);
    }
    printf("\n");
    SDL_RenderPresent(renderer);
    SDL_Delay(5000);                                  // Petite pause
    SDL_RenderClear(renderer);                    // Effacer la fenêtre*
    


    /*********************************************************************************************************************/
    /* on referme proprement la SDL */


    free(mon_laby);
    SDL_DestroyTexture(tuiles);
    TTF_Quit();
    IMG_Quit();                                // Si on charge une librairie SDL, il faut penser à la décharger
    end_sdl(1, "Normal ending", window, renderer);
    return EXIT_SUCCESS;
}

#include "labyrinth.h"

void affiche_tab(int * tab, int taille){
    for(int i = 0;i<taille;++i){
        printf("%d ",tab[i]);
    }
    printf("\n");
}

void nettoyage(int** laby, int lignes, int colonnes,int * numDij,int *numA){
    int i,j;
    for(i=0;i<colonnes;i++){
        for(j=0;j<lignes;j++){
            if(laby[i][j] & EST_PASSE_DIJ){
                laby[i][j]-=EST_PASSE_DIJ;
                *numDij = *numDij +1;
                //printf("suppr%d\n",*numDij);
            }    
            if(laby[i][j] & EST_PASSE_A){
                laby[i][j]-=EST_PASSE_A;
                *numA = *numA +1;
            }
        }
    }
}

int * renverser_predesc(int * tab,int taille,int depart, int arrive,int * indice_noeud){
    int * copie = (int*) malloc(sizeof(int)*taille);
    int i = taille -1;
    copie[i] = arrive;
    --i;
    while (depart != arrive){
        copie[i] = tab[arrive];
        arrive = tab[arrive];
        --i;
    }
    *indice_noeud= i+1;
    return copie;
}


int* dijkstra_mat(int ** laby, int lignes, int colonnes, int origine, int arrivee,int * indice_noeud_cour){
    int nbr_noeuds = lignes*colonnes;

    float * P1 = (float*) malloc(sizeof(float)*(nbr_noeuds)); // liste des distances du point initial par sommet (-1 pour ceux non traversé) 
    int * P2 = (int*) malloc(sizeof(int)*(nbr_noeuds)); // liste des prédécesseurs
    int i,j;
    for(i=0;i<nbr_noeuds;i++){
        P1[i]=-1;
    }
    P1[origine]=0;
    P2[origine]=origine;
    noeud_tas_t * a_mini = (noeud_tas_t *) malloc(sizeof(noeud_tas_t));
    noeud_tas_t * neu = (noeud_tas_t *) malloc(sizeof(noeud_tas_t));
    neu->noeud = origine;
    neu->poids = 0;
    tasBis_t * monTas = init_tasBis(nbr_noeuds*nbr_noeuds);
    insere_tasBis(monTas,*neu);
    int n;
    while (a_mini->noeud!=arrivee){
        *a_mini = extraitminBis(monTas);
        k_to_np(a_mini->noeud,&i,&j,colonnes);
        if (!(laby[i][j] & EST_PASSE_DIJ)){
            if ((!(laby[i][j] & EST))&&(!(laby[i+1][j] & EST_PASSE_DIJ))){
                n=np_to_k(i+1,j,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +1)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+1;
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            if ((!(laby[i][j] & OUEST))&&(!(laby[i-1][j] & EST_PASSE_DIJ))){
                n=np_to_k(i-1,j,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +1)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+1;
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            if ((!(laby[i][j] & SUD))&&(!(laby[i][j+1] & EST_PASSE_DIJ))){
                n=np_to_k(i,j+1,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +1)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+1;
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            if ((!(laby[i][j] & NORD))&&(!(laby[i][j-1] & EST_PASSE_DIJ))){
                n=np_to_k(i,j-1,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +1)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+1;
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            laby[i][j]+=EST_PASSE_DIJ;
        }
    }
    //affiche_tab(P2,G->n);
    free(a_mini);
    free(neu);
    free_tasBis(monTas);
    free(P1);
    //nettoyage(laby,lignes,colonnes);

    int * copie = renverser_predesc(P2,nbr_noeuds,origine,arrivee,indice_noeud_cour);
    free(P2);
    return copie;
}

int* astar_mat(int ** laby, int lignes, int colonnes, int origine, int arrivee, int dist,int *indice_noeud){
    int nbr_noeuds = lignes*colonnes;
    int x,y;
    k_to_np(arrivee,&x,&y,colonnes);

    float * P1 = (float*) malloc(sizeof(float)*(nbr_noeuds)); // liste des distances du point initial par sommet (-1 pour ceux non traversé) 
    int * P2 = (int*) malloc(sizeof(int)*(nbr_noeuds)); // liste des prédécesseurs
    int i,j;
    for(i=0;i<nbr_noeuds;i++){
        P1[i]=-1;
    }
    k_to_np(origine,&i,&j,colonnes);
    P1[origine]=distance(x,i,y,j,dist);
    P2[origine]=origine;
    noeud_tas_t * a_mini = (noeud_tas_t *) malloc(sizeof(noeud_tas_t));
    noeud_tas_t * neu = (noeud_tas_t *) malloc(sizeof(noeud_tas_t));
   
    neu->noeud = origine;
    neu->poids = P1[origine];
    tasBis_t * monTas = init_tasBis(nbr_noeuds*nbr_noeuds+2);
    insere_tasBis(monTas,*neu);
    int n;
    while (a_mini->noeud!=arrivee){
        //affiche_tasBis(monTas);
        *a_mini = extraitminBis(monTas);
        k_to_np(a_mini->noeud,&i,&j,colonnes);
        //printf("%d %d\n",i,j);
        if (!(laby[i][j]&EST_PASSE_A)){
            if ((!(laby[i][j] & EST))&&(!(laby[i+1][j] & EST_PASSE_A))){
                n=np_to_k(i+1,j,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +1)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+1+distance(x,i+1,y,j,dist)-distance(x,i,y,j,dist);
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            if ((!(laby[i][j] & OUEST))&&(!(laby[i-1][j] & EST_PASSE_A))){
                n=np_to_k(i-1,j,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +1)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+1+distance(x,i-1,y,j,dist)-distance(x,i,y,j,dist);
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            if ((!(laby[i][j] & SUD))&&(!(laby[i][j+1] & EST_PASSE_A))){
                n=np_to_k(i,j+1,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +1)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+1+distance(x,i,y,j+1,dist)-distance(x,i,y,j,dist);
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            if ((!(laby[i][j] & NORD))&&(!(laby[i][j-1] & EST_PASSE_A))){
                n=np_to_k(i,j-1,colonnes);
                if ((P1[n]==-1)||(P1[n]>a_mini->poids +1)){
                    neu->noeud = n;
                    neu->poids = P1[a_mini -> noeud]+1+distance(x,i,y,j-1,dist)-distance(x,i,y,j,dist);
                    insere_tasBis(monTas,*neu);
                    P1[n]=neu->poids;
                    P2[n]=a_mini->noeud;
                }
            }
            laby[i][j]+=EST_PASSE_A;
        }
    }
    //affiche_tab(P2,G->n);
    free(a_mini);
    free(neu);
    free_tasBis(monTas);
    free(P1);
    //nettoyage(laby,lignes,colonnes);
    
    int * copie = renverser_predesc(P2,nbr_noeuds,origine,arrivee,indice_noeud);
    free(P2);
    return copie;
}


int avancer_pos_final(int pos_final[2],int n,int noeud_courant,int noeud_futur,int * direction_bonhomme,int arrivee){
    int res = (noeud_courant != arrivee);
    if (res){
        int i1,j1,i2,j2;
        k_to_np(noeud_courant,&i1,&j1,n);
        k_to_np(noeud_futur,&i2,&j2,n);
        if (i2<i1){//LEFT
            pos_final[0] -= 1;
            *direction_bonhomme = 1;
        }
        if (i1<i2){//RIGHT
            pos_final[0] += 1;
            *direction_bonhomme = 3;
        }
        if (j2<j1){//UP
            pos_final[1] -= 1;
            *direction_bonhomme = 2;
        }
        if (j1<j2){//DOWN
            pos_final[1] += 1;
            *direction_bonhomme = 0;
        }
    }
    return res;
}




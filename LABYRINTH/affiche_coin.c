typedef struct coin = {
    int x;
    int y;
    struct coin * suivant;
}coin;

void affiche_coin(int frame_coin, coin * liste_coin, float pos_reel[2], int hauteur, int longueur, float zoom, SDL_Texture* coin, SDL_Renderer* renderer){
    SDL_Rect source = {0},                         // Rectangle définissant la zone de la texture à récupérer
            destination = {0},                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
            dimension_source = {0};

    SDL_QueryTexture(tuiles, NULL, NULL,
                    &dimension_source.w, &dimension_source.h);       // Récupération des dimensions de l'image

    source.x=frame_coin*dimension_source.w/4;
    source.y=frame_coin*dimension_source.h/4;
    source.w=dimension_source.w/6;
    source.h=dimension_source.h;

    coin cour = liste_coin;
    do {
        destination.x=(cour->x-pos_reel[0])*(zoom*source.w/)+(largeur-zoom*sour.w)/2;
        destination.y=(cour->x-pos_reel[0])*(zoom*source.w/)+(largeur-zoom*sour.w)/2;
        destination.w=zoom*source.w/6;
        destination.h=zoom*source.h;

        SDL_RenderCopy(renderer, coin, &source, &destination);

        cour=cour->suivant;

    }while(cour!=NULL);
}

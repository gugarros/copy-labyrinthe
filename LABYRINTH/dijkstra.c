#include "graph_liste.c"
#include "part.c"
#include "lch.c"
#include "lchbis.c"
#include "tasBis.c"
#include "tas.c"

int main(){
    int n = 15;
    int p = 12;
    int origine = 0;
    int arrivee = n*p-1;
    graph_liste_t * ma_grid = init_laby_liste(n, p);
    graph_liste_t * mon_laby = kruskal_graph(ma_grid);
    free_part( kruskal(ma_grid,"_kruskal",1));//a mettre en commentaire si trop grand
    dot_graph_liste(ma_grid,"_origi",-1,1);//a mettre en commentaire si trop grand
    dot_graph_liste(mon_laby,"_copie",-1,1);//a mettre en commentaire si trop grand
    
    graph_liste_t * dij = dijkstra(origine,arrivee,mon_laby);
    dot_graph_liste(dij,"_dij",origine,1);

    graph_liste_t * star = astar(origine,arrivee,mon_laby,n,1);
    dot_graph_liste(star,"_star",origine,1);
    
    free_graph_liste(ma_grid);
    free_graph_liste(mon_laby);
    free_graph_liste(dij);
    free_graph_liste(star);
    return 0;
}
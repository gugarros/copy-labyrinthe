//gcc main2.c -o prog -lSDL2 -lSDL2_gfx -lSDL2_image -lSDL2_ttf //-lSDL2_mixer -lSDL2_net

#include "labyrinth.c"
#include "graph_liste.c"
#include "part.c"
#include "lch.c"
#include "lchbis.c"
#include "tirer.c"
#include "enemies.c"
#include "tasBis.c"
//#include "tas.c"
#include "NP_complet.c"
#include "pile.c"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>



void afficherEcran(SDL_Renderer *renderer){
  SDL_RenderPresent(renderer);
}


void afficher_fond(int longueur, int hauteur,SDL_Texture* fond, SDL_Renderer* renderer){
  // fond rouge
  // SDL_SetRenderDrawColor(renderer,255,0,0,255);
  // SDL_Rect rectangle;
  // rectangle.x=0;
  // rectangle.y=0;
  // rectangle.w=longueur;
  // rectangle.h=hauteur;
  // SDL_RenderFillRect(renderer,&rectangle);
  SDL_Rect rect;
  rect.x = 0;
  rect.y = 0;
  rect.w = longueur;
  rect.h = hauteur;
  SDL_RenderCopy(renderer, fond, NULL, &rect);
}

void afficher_fog(int longueur,int hauteur,SDL_Texture* fog, SDL_Renderer* renderer){
  SDL_Rect rect;
  rect.x = 0;
  rect.y = 0;
  rect.w = longueur;
  rect.h = hauteur;
  SDL_RenderCopy(renderer, fog, NULL, &rect);
}

int ** laby_liste_to_laby_mat(int n, int p, graph_liste_t * mon_laby_liste,int n_coin,int depart,int arrivee){
    // proba : d'enlever un mur
    int i,j;
    int ** mon_laby_mat = (int **) malloc(sizeof(int*) *n);
    if (mon_laby_mat != NULL) {
        for(i=0;i<n;++i){
            mon_laby_mat [i] = (int *) malloc(sizeof(int) * p);
        }

    } else {
        printf("Erreur\n");
        free(mon_laby_mat);
        mon_laby_mat = NULL;
    }
    for(i=0;i<n;i++){
        for(j=0;j<p;j++){
            mon_laby_mat[i][j] = NORD + OUEST + SUD + EST;// met tous les murs
        }
    }
    int position,position_cour,k;
    for(k=0;k<n_coin;k++){
        position=rand()%((n*p)-k);
        position_cour=0;
        i=0;
        j=0;
        while(position_cour<position){
            while (mon_laby_mat[i][j] & COINS){
                i++;
                if (i>=n){i=0;j++;}
            }
            position_cour++;
            i++;
            if (i>=n){i=0;j++;}
        }
        while (mon_laby_mat[i][j] & COINS){
                i++;
                if (i>=n){i=0;j++;}
            }
        mon_laby_mat[i][j] += COINS;
    }
    k_to_np(depart,&i,&j,n);
    mon_laby_mat[i][j] += DEPART;
    k_to_np(arrivee,&i,&j,n);
    mon_laby_mat[i][j] += ARRIVEE;
    //mon_laby_mat[pos_final[0]][pos_final[1]] += BONHOMME;

    element_t * cour = mon_laby_liste->liste;
    int i1,i2,j1,j2;
    
    while (cour!=NULL){
        k_to_np((cour->couple).noeud1, &i1, &j1, n);
        k_to_np((cour->couple).noeud2, &i2, &j2, n);
        //on enleves les murs
        if (j1>j2){
            mon_laby_mat[i1][j1] = mon_laby_mat[i1][j1]- NORD;
            mon_laby_mat[i2][j2] = mon_laby_mat[i2][j2]- SUD;
        }
        if (i1<i2){
            mon_laby_mat[i1][j1] = mon_laby_mat[i1][j1]- EST; 
            mon_laby_mat[i2][j2] = mon_laby_mat[i2][j2]- OUEST;
        }
        if (j1<j2){
            mon_laby_mat[i1][j1] = mon_laby_mat[i1][j1]- SUD;
            mon_laby_mat[i2][j2] = mon_laby_mat[i2][j2]- NORD;
        }
        if (i1>i2){
            mon_laby_mat[i1][j1] = mon_laby_mat[i1][j1]- OUEST;
            mon_laby_mat[i2][j2] = mon_laby_mat[i2][j2]- EST; 
        }
        cour=cour->suivant;
    }
    // for(i=0;i<n;++i){
    //   for(j=0;j<p-1;++j){
    //     if ((proba > ( ((float) rand()) / (float) RAND_MAX) )&&(mon_laby_mat[i][j] & SUD)){
    //       mon_laby_mat[i][j] = mon_laby_mat[i][j] - SUD;
    //       mon_laby_mat[i][j+1] =mon_laby_mat[i][j+1] - NORD;
    //     }
    //   }
    // }
    // for(i=0;i<n-1;++i){
    //   for(j=0;j<p;++j){
    //     if ((proba > ( ((float) rand()) / (float) RAND_MAX) ) && (mon_laby_mat[i][j] & EST )){
    //       mon_laby_mat[i][j] = mon_laby_mat[i][j] - EST;
    //       mon_laby_mat[i+1][j] = mon_laby_mat[i+1][j] - OUEST;
    //     }
    //   }
    // }
    return mon_laby_mat;
}

void afficher_laby(int ** mon_laby, int longueur, int hauteur, int n, int p,float pos_reel[2], float zoom, SDL_Texture* tuiles, SDL_Texture* tuiles_empty, SDL_Renderer* renderer,int path, int corner){

    SDL_Rect source = {0},                         // Rectangle définissant la zone de la texture à récupérer
            destination = {0},                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
            dimension_source = {0},
            dimension_source_empty = {0};
    

    

    SDL_QueryTexture(tuiles, NULL, NULL,
                    &dimension_source.w, &dimension_source.h);       // Récupération des dimensions de l'image
    
    SDL_QueryTexture(tuiles_empty, NULL, NULL,
                    &dimension_source_empty.w, &dimension_source_empty.h);       // Récupération des dimensions de l'image
    
    //printf("dim images %d %d\n",dimension_source.w,dimension_source.h);
    int i,j;
    source.y=0;
    source.w=dimension_source.w/16;
    source.h=dimension_source.h;

    destination.w=zoom*source.w;
    destination.h=zoom*source.h;

    float destination0x = - pos_reel[0]*zoom*source.w + (longueur - zoom*source.w)/2;
    float destination0y = - pos_reel[1]*zoom*source.h + (hauteur - zoom*source.h)/2;
    for(i=0;i<n;i++){
        for(j=0;j<p;j++){
            if (path || mon_laby[i][j] & EST_PASSE){
                source.x=(mon_laby[i][j]%16)*dimension_source.w/16;

                destination.x=destination0x + i * destination.w;
                destination.y=destination0y + j * destination.h;
                // destination.x=(i-pos_reel[0])*zoom*source.w + (longueur - zoom*source.w)/2;
                // destination.y=(j-pos_reel[1])*zoom*source.h + (hauteur - zoom*source.h)/2;
                SDL_RenderCopy(renderer, tuiles, &source, &destination);
            }
        }
    }
    source.y=0;
    source.w=dimension_source_empty.w/4;
    source.h=dimension_source_empty.h;

    destination.w=zoom*source.w;
    destination.h=zoom*source.h;
    if (corner == 1){
      for(i=0;i<n;i++){
        for(j=0;j<p;j++){
          if (path || mon_laby[i][j] & EST_PASSE){
            destination.x=(i-pos_reel[0])*zoom*source.w + (longueur - zoom*source.w)/2;
            destination.y=(j-pos_reel[1])*zoom*source.h + (hauteur - zoom*source.h)/2;
            if (! ((mon_laby[i][j] & NORD) || (mon_laby[i][j] & OUEST))) {
              source.x=0*dimension_source_empty.w/4;
              SDL_RenderCopy(renderer, tuiles_empty, &source, &destination);
            }
            if (! ((mon_laby[i][j] & OUEST) || (mon_laby[i][j] & SUD))) {
              source.x=1*dimension_source_empty.w/4;
              SDL_RenderCopy(renderer, tuiles_empty, &source, &destination);
            }
            if (! ((mon_laby[i][j] & SUD) || (mon_laby[i][j] & EST))) {
              source.x=2*dimension_source_empty.w/4;
              SDL_RenderCopy(renderer, tuiles_empty, &source, &destination);
            }
            if (! ((mon_laby[i][j] & EST) || (mon_laby[i][j] & NORD))) {
              source.x=3*dimension_source_empty.w/4;
              SDL_RenderCopy(renderer, tuiles_empty, &source, &destination);
            }
          }
        }
      }
    } else {
      if (corner == 2){
        for(i=0;i<n;i++){
          for(j=0;j<p;j++){
            if (path || mon_laby[i][j] & EST_PASSE){
              destination.x=(i-pos_reel[0])*zoom*source.w + (longueur - zoom*source.w)/2;
              destination.y=(j-pos_reel[1])*zoom*source.h + (hauteur - zoom*source.h)/2;

              if (! ((mon_laby[i][j] & NORD) || (mon_laby[i][j] & OUEST))){
                if (((i>0) && (mon_laby[i-1][j] & NORD)) || ((j>0) && (mon_laby[i][j-1] & OUEST))){
                  source.x=0*dimension_source_empty.w/4;
                  SDL_RenderCopy(renderer, tuiles_empty, &source, &destination);
                }
              }
              if (! ((mon_laby[i][j] & OUEST) || (mon_laby[i][j] & SUD))){
                if (((i>0) && (mon_laby[i-1][j] & SUD)) || ((j<n-1) && (mon_laby[i][j+1] & OUEST))){
                  source.x=1*dimension_source_empty.w/4;
                  SDL_RenderCopy(renderer, tuiles_empty, &source, &destination);
                }
              }
              if (! ((mon_laby[i][j] & SUD) || (mon_laby[i][j] & EST))){
                if (((i<n-1) && (mon_laby[i+1][j] & SUD)) || ((j<p-1) && (mon_laby[i][j+1] & EST))){
                  source.x=2*dimension_source_empty.w/4;
                  SDL_RenderCopy(renderer, tuiles_empty, &source, &destination);
                }
              }
              if (! ((mon_laby[i][j] & EST) || (mon_laby[i][j] & NORD))){
                if (((i<n-1) && (mon_laby[i+1][j] & NORD)) || ((j>0) && (mon_laby[i][j-1] & EST))){
                  source.x=3*dimension_source_empty.w/4;
                  SDL_RenderCopy(renderer, tuiles_empty, &source, &destination);
                }
              }
            }
          }
        }
      } 
    }
}


void afficher_bonhomme(float frame_bonhomme, int direction_bonhomme,int hauteur, int longueur, float zoom_bonhomme, SDL_Texture* bonhomme,SDL_Renderer* renderer){
    SDL_Rect source = {0},                         // Rectangle définissant la zone de la texture à récupérer
            destination = {0},                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
            dimension_source = {0};
    SDL_QueryTexture(bonhomme, NULL, NULL,
                    &dimension_source.w, &dimension_source.h);       // Récupération des dimensions de l'image

    source.x=direction_bonhomme*dimension_source.w/4;
    source.y=((int) frame_bonhomme)*dimension_source.h/4;
    source.w=dimension_source.w/4;
    source.h=dimension_source.h/4;

    destination.x=(-zoom_bonhomme*source.w +  longueur)/2;
    destination.y=(-zoom_bonhomme*source.h +  hauteur)/2;
    destination.w=zoom_bonhomme*source.w;
    destination.h=zoom_bonhomme*source.h;

    SDL_RenderCopy(renderer, bonhomme,
                    &source,
                    &destination); 
}

void supprime_coin(int ** mon_laby,int pos_final[2]){
  mon_laby[pos_final[0]][pos_final[1]] -= COINS;
}

void afficher_coin(int ** mon_laby,int n,int p,float frame_coin, float pos_reel[2], int hauteur, int longueur, float zoom, int taille_case[2],SDL_Texture* coin, SDL_Renderer* renderer){
    SDL_Rect source = {0},                         // Rectangle définissant la zone de la texture à récupérer
            destination = {0},                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
            dimension_source = {0};

    SDL_QueryTexture(coin, NULL, NULL,
                    &dimension_source.w, &dimension_source.h);       // Récupération des dimensions de l'image

    source.x=((int) frame_coin)*dimension_source.w/6;
    source.y=0;
    source.w=dimension_source.w/6;
    source.h=dimension_source.h;

    
    int i,j;
    for (i=0;i<n;++i){
      for(j=0;j<p;++j){
        if (mon_laby[i][j] & COINS) {
          destination.x=(i-pos_reel[0])*taille_case[0] + (longueur)/2 -zoom*source.w/2;
          destination.y=(j-pos_reel[1])*taille_case[1] + (hauteur)/2 -zoom*source.h/2;
          destination.w=zoom*source.w;
          destination.h=zoom*source.h;
          SDL_RenderCopy(renderer, coin, &source, &destination);
        }
      }
    }      
}

void afficher_flags(int ** mon_laby,float frame_flags, float pos_reel[2], int hauteur, int longueur,int n,int p, float zoom, int taille_case[2],SDL_Texture* flags, SDL_Renderer* renderer){
    SDL_Rect source_depart = {0},                         // Rectangle définissant la zone de la texture à récupérer
            source_arrivee = {0},
            destination = {0},                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
            dimension_source = {0};

    SDL_QueryTexture(flags, NULL, NULL,
                    &dimension_source.w, &dimension_source.h);       // Récupération des dimensions de l'image

    source_depart.x=((int) frame_flags)*dimension_source.w/4;
    source_depart.y=0;
    source_depart.w=dimension_source.w/4;
    source_depart.h=dimension_source.h/3;

    source_arrivee.x=((int) frame_flags)*dimension_source.w/4;
    source_arrivee.y=dimension_source.h/2;
    source_arrivee.w=dimension_source.w/4;
    source_arrivee.h=dimension_source.h/3;

    int i,j;

    for(i=0;i<n;i++){
      for(j=0;j<p;j++){
        if (mon_laby[i][j] & ARRIVEE){
          destination.x=(i-pos_reel[0])*taille_case[0] + (longueur)/2 -zoom*source_arrivee.w/2;
          destination.y=(j-pos_reel[1])*taille_case[1] + (hauteur)/2 -zoom*source_arrivee.h/2;
          destination.w=zoom*source_arrivee.w;
          destination.h=zoom*source_arrivee.h;
          SDL_RenderCopy(renderer, flags, &source_arrivee, &destination);
        } else {
          if (mon_laby[i][j] & DEPART){
            destination.x=(i-pos_reel[0])*taille_case[0] + (longueur)/2 -zoom*source_depart.w/2;
            destination.y=(j-pos_reel[1])*taille_case[1] + (hauteur)/2 -zoom*source_depart.h/2;
            destination.w=zoom*source_depart.w;
            destination.h=zoom*source_depart.h;
            SDL_RenderCopy(renderer, flags, &source_depart, &destination);
          }
        }
      }
    }
}

void afficher_coffre(int position, int colonnes, float zoom, int taille_case[2],
                    int longueur, int hauteur, int presence, float pos_reel[2],
                    SDL_Texture * coffre, SDL_Renderer * renderer){
    if (presence){
        int i,j;
        k_to_np(position,&i,&j,colonnes);
        SDL_Rect destination = {0},                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
                source = {0};

        SDL_QueryTexture(coffre, NULL, NULL,
                        &source.w, &source.h);       // Récupération des dimensions de l'image

        source.x=0;
        source.y=0;
        destination.x=(i-pos_reel[0])*taille_case[0] + (longueur)/2 -zoom*source.w/2;
        destination.y=(j-pos_reel[1])*taille_case[1] + (hauteur)/2 -zoom*source.h/2;
        destination.w=zoom*source.w;
        destination.h=zoom*source.h;
        SDL_RenderCopy(renderer, coffre, &source, &destination);
    }
}

void tresor(int ** laby, int depart, int arrivee, int lignes, int colonnes){
    int k;
    int i,j;
    int * chemin = astar_mat(laby,lignes,colonnes,depart,arrivee,1,&k);
    while(chemin[k]!=arrivee){
        k_to_np(chemin[k],&i,&j,colonnes);
        if ((!(laby[i][j] & COINS))&&(!(laby[i][j] & DEPART))){
            laby[i][j]+=COINS;
        }
        k++;
    }
    free(chemin);
    nettoyage(laby, lignes, colonnes,&i,&j);
}

int collision_coffre(int pos_final[2], int position_coffre,
                     int colonnes, int * presence){
    int res = 0;
    int x,y;
    k_to_np(position_coffre,&x,&y,colonnes);
    if ((pos_final[0]==x)&&(pos_final[1]==y)){
        res=1;
        *presence=0;
    }
    return res;
}

void generation_coffre(int ** laby, int * position, int lignes, int colonnes){
    *position=rand()%((colonnes*lignes)-1);
    int i,j;
    k_to_np(*position,&i,&j,colonnes);
    while ((laby[i][j] & ARRIVEE)||(laby[i][j] & DEPART)||(laby[i][j] & COINS)){
        *position=rand()%((colonnes*lignes)-1);
        k_to_np(*position,&i,&j,colonnes);
    }
}

void afficher_parcours(int ** mon_laby, int longueur, int hauteur, int n, int p, float pos_reel[2],int taille_case[2], SDL_Renderer* renderer,int mode){
    
    if (mode & EST_PASSE_DIJ) SDL_SetRenderDrawColor(renderer,255,255,255,125);
    if (mode & EST_PASSE_A) SDL_SetRenderDrawColor(renderer,255,0,0,125);
    SDL_SetRenderDrawBlendMode(renderer,SDL_BLENDMODE_BLEND);
    SDL_Rect rectangle;
    rectangle.x=0;
    rectangle.y=0;
    rectangle.w=taille_case[0];
    rectangle.h=taille_case[1];
    int i,j;
    for (i=0;i<n;++i){
      for(j=0;j<p;++j){
        if (mon_laby[i][j] & mode) {
          rectangle.x=(i-pos_reel[0])*taille_case[0] + (longueur)/2 -rectangle.w/2;
          rectangle.y=(j-pos_reel[1])*taille_case[1] + (hauteur)/2 -rectangle.h/2;
          
          SDL_RenderFillRect(renderer, &rectangle);
        }
      }
    }      
}

void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  TTF_Font * font,
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);
  if (font != NULL) TTF_CloseFont(font);
  IMG_Quit();
  TTF_Quit();
  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}


void affichage_score(int score, TTF_Font * font, SDL_Window* window,SDL_Renderer* renderer){

    char coins_get[35];
    char score_text[15];

    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre

    SDL_Color color = {255,255,255,255};
    SDL_Surface* text_surface1 = NULL; 
    SDL_Surface* text_surface2 = NULL;                                    // la surface  (uniquement transitoire)
    
    sprintf(coins_get,"pieces obtenues : %d / %d",(score/10),NBR_COINS);
    text_surface1 = TTF_RenderText_Blended(font, coins_get, color); // création du texte dans la surface 
    if (text_surface1 == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);

    sprintf(score_text,"score : %d",score);
    text_surface2 = TTF_RenderText_Blended(font, score_text, color);
    if (text_surface2 == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);

    SDL_Texture* text_texture1 = NULL;                                    // la texture qui contient le texte
    SDL_Texture* text_texture2 = NULL;

    text_texture1 = SDL_CreateTextureFromSurface(renderer, text_surface1); // transfert de la surface à la texture
    if (text_texture1 == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
    SDL_FreeSurface(text_surface1);                                       // la texture ne sert plus à rien

    text_texture2 = SDL_CreateTextureFromSurface(renderer, text_surface2);
    if (text_texture2 == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
    SDL_FreeSurface(text_surface2);

    SDL_Rect pos1 = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_Rect pos2 = {0, 0, 0, 0};
    SDL_QueryTexture(text_texture1, NULL, NULL, &pos1.w, &pos1.h);          // récupération de la taille (w, h) du texte 
    SDL_QueryTexture(text_texture2, NULL, NULL, &pos2.w, &pos2.h);
    pos1.x = 0.05*window_dimensions.w;
    pos1.y = 0.05*window_dimensions.h;
    pos2.x = 0.05*window_dimensions.w;
    pos2.y = 0.05*window_dimensions.h+pos1.h;
    SDL_RenderCopy(renderer, text_texture1, NULL, &pos1);                  // Ecriture du texte dans le renderer   
    SDL_RenderCopy(renderer, text_texture2, NULL, &pos2);
    SDL_DestroyTexture(text_texture1);                                    // On n'a plus besoin de la texture avec le texte
    SDL_DestroyTexture(text_texture2);
}

int mise_a_jour_pos(float pos_reel[2],int pos_final[2],float *frame_bonhomme,float *frame_coin ,float *frame_flags){
    float pas = 0.07;
    float prec = 0.04;
    float vitesse_bonhomme = 0.2;
    float vitesse_coins = 0.1;
    float vitesse_flags = 0.15;
    int bouge = 0;
    if (pos_reel[0] > pos_final[0]+prec){
        pos_reel[0] -= pas;
        //*direction_bonhomme = 1;
        bouge =1;
    }
    if (pos_reel[0] < pos_final[0]-prec){
        pos_reel[0] += pas;
        //*direction_bonhomme = 3;
        bouge =1;
    }
    if (pos_reel[1] > pos_final[1]+prec){
        pos_reel[1] -= pas;
        //*direction_bonhomme =2;
        bouge =1;
    }
    if (pos_reel[1] < pos_final[1]-prec){
        pos_reel[1] += pas;
        //*direction_bonhomme =0;
        bouge =1;
    }
    *frame_coin = *frame_coin + vitesse_coins;
    if (*frame_coin > 6) *frame_coin = *frame_coin -6;

    *frame_flags = *frame_flags + vitesse_flags;
    if (*frame_flags > 4) *frame_flags = *frame_flags -4;

    if (bouge) {
     *frame_bonhomme = *frame_bonhomme +vitesse_bonhomme;
     if (*frame_bonhomme > 4) *frame_bonhomme = *frame_bonhomme -4;
    } else {
      *frame_bonhomme = 0;
    }
    return bouge;
}



void deep_first_search(int ** laby, int n, int p,int origine){
    int nbr_noeuds = n*p;
    pile_t * maPile = initPile(nbr_noeuds*2+2);
    element_pile_t val;
    val.nbr = origine;
    int code;
    int i,j;
    int nouv;
    empiler(maPile,val,&code);
    k_to_np(val.nbr,&i,&j,n);
    printf("origine : %d %d %d\n",val.nbr,i,j);
    while (!estVidePile(maPile)){
      nouv = 0;
      depiler(maPile,&val,&code);
      empiler(maPile,val,&code);
      k_to_np(val.nbr,&i,&j,n);
      
      printf("%d|%d| %d %d\n",val.nbr,maPile->sommet, i,j);
      if (!(laby[i][j] & EST_PASSE_PROFONDEUR)) {
        laby[i][j] += EST_PASSE_PROFONDEUR;
      }
      if (1){
        if ((!(laby[i][j] & EST))&&(!(laby[i+1][j] & EST_PASSE_PROFONDEUR))){
          //printf("EST %d\n",laby[i][j]);
          val.nbr = np_to_k(i+1,j,n);
          empiler(maPile,val,&code);
          nouv =1;
        } else {
        if ((!(laby[i][j] & OUEST))&&(!(laby[i-1][j] & EST_PASSE_PROFONDEUR))){
          //printf("OUEST %d\n",laby[i][j]);
          val.nbr = np_to_k(i-1,j,n);
          empiler(maPile,val,&code);
          nouv=1;
        } else {
        if ((!(laby[i][j] & SUD))&&(!(laby[i][j+1] & EST_PASSE_PROFONDEUR))){
          //printf("SUD %d\n",laby[i][j]);
          val.nbr = np_to_k(i,j+1,n);
          empiler(maPile,val,&code);
          nouv=1;
        } else {
        if ((!(laby[i][j] & NORD))&&(!(laby[i][j-1] & EST_PASSE_PROFONDEUR))){
          //printf("NORD %d\n",laby[i][j]);
          val.nbr = np_to_k(i,j-1,n);
          empiler(maPile,val,&code);
          nouv=1;
        }}}}
        if (!nouv){
          depiler(maPile,&val,&code);
          printf("back\n");
          // if ((!(laby[i][j] & EST))&&(laby[i+1][j] & EST_PASSE_PROFONDEUR)){
          //   //printf("EST %d\n",laby[i][j]);
          //   val.nbr = np_to_k(i+1,j,n);
          //   empiler(maPile,val,&code);
          //   nouv =0;
          // } 
          // if ((!(laby[i][j] & OUEST))&&(laby[i-1][j] & EST_PASSE_PROFONDEUR)){
          //   //printf("OUEST %d\n",laby[i][j]);
          //   val.nbr = np_to_k(i-1,j,n);
          //   empiler(maPile,val,&code);
          //   nouv=0;
          // } 
          // if ((!(laby[i][j] & SUD))&&(laby[i][j+1] & EST_PASSE_PROFONDEUR)){
          //   //printf("SUD %d\n",laby[i][j]);
          //   val.nbr = np_to_k(i,j+1,n);
          //   empiler(maPile,val,&code);
          //   nouv=0;
          // } 
          // if ((!(laby[i][j] & NORD))&&(laby[i][j-1] & EST_PASSE_PROFONDEUR)){
          //   //printf("NORD %d\n",laby[i][j]);
          //   val.nbr = np_to_k(i,j-1,n);
          //   empiler(maPile,val,&code);
          //   nouv=0;
          // } 
          // if (nouv==1) printf("wtf\n");
        }
      } else{
          printf("alors ?\n");
      }
      printf("code : %d\n",code);
    }
}

void lose(SDL_Renderer * renderer,TTF_Font * font,int largeur, int hauteur){
  int i;
  for (i = 0;i<20;++i){
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    SDL_Color color = {rand()%256, rand()%256, rand()%256, 255};                                  // la couleur du texte
    SDL_Surface* text_surface = NULL;                                    // la surface  (uniquement transitoire)
    text_surface = TTF_RenderText_Blended(font, "LOSE !", color); // création du texte dans la surface 
    //if (text_surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    //if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

    SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    pos.x = largeur/2- pos.w/2;
    pos.y = hauteur/2 - pos.h/2;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
    afficherEcran(renderer);
    SDL_Delay(50);
  }
}


int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  time_t seed = time(0);
  printf("%ld\n",seed);
  srand(seed);

  int largeur =  1200;
  int hauteur = 800;
  SDL_Window *window = NULL;               // Future fenêtre
  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
  }

  /* Création de la fenêtre */
  window = SDL_CreateWindow(
      "Labyby",                    // codage en utf8, donc accents possibles
      SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,                                  // coin haut gauche en haut gauche de l'écran
      largeur, hauteur,                              // largeur , hauteur
      SDL_WINDOW_RESIZABLE);                 // redimensionnable
  if (window == NULL) {
    SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
    SDL_Quit();
    exit(EXIT_FAILURE);
  }

  if (TTF_Init() != 0){
    fprintf(stderr, "Erreur d'initialisation TTF : %s\n", TTF_GetError()); 
  }
  
  //TTF_Font * font = NULL;
  
  //font = TTF_OpenFont("./img1/Pacifico.ttf", 65 );
  //TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);           // en italique, gras
  

  int flags=IMG_INIT_JPG|IMG_INIT_PNG;
  int initted= 0;

  initted = IMG_Init(flags);

  if((initted&flags) != flags) 
  {
      printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
      printf("IMG_Init: %s\n", IMG_GetError());
  }
  
  //------------------------------------------------------------------
  /* Création du renderer */
  SDL_Renderer* renderer = NULL;
  renderer = SDL_CreateRenderer(
           window, -1, 0);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", NULL, window, renderer);








  /*initialisation du laby */

  int n=N_LABY,p=P_LABY;//largeur / hauteur
  float zoom = ZOOM;
  int noeud_dep = NOEUD_DEPART;
  if (noeud_dep==-1) noeud_dep = rand()%(N_LABY*P_LABY);
  int noeud_arr = NOEUD_ARRIVEE;
  if (noeud_arr==-1) while ((noeud_arr = rand()%(N_LABY*P_LABY)) == noeud_dep) ;
  int pos_final[2];
  k_to_np(noeud_dep,&pos_final[0],&pos_final[1],n);
  float pos_reel[2] = {pos_final[0],pos_final[1]};
  int * tab_path = NULL; // pour les dijkstra et a star


  graph_liste_t * mon_laby_liste = init_laby_liste(n,p);
  
  graph_liste_t * copie = kruskal_graph(mon_laby_liste,PROBA);
  // kruskal(mon_laby_liste,"_kruskal",0);//a mettre en commentaire si trop grand
  // dot_graph_liste(mon_laby_liste,"_origi",-1,0);//a mettre en commentaire si trop grand
  // dot_graph_liste(copie,"_copie",-1,0);//a mettre en commentaire si trop grand
  int ** mon_laby=laby_liste_to_laby_mat(n,p,copie,NBR_COINS,noeud_dep,noeud_arr);
  enemies_t * liste_enemies = genere_liste_enemies(NBR_ENNEMIS,n,p,mon_laby);

  free_graph_liste(copie);
  free_graph_liste(mon_laby_liste);
  
  int parcour_en_cour =0;
  int indice_noeud_cour;
  int numDij;
  int numA;
  int * pos_deep;

  /* FOND LABYRINTH */
  SDL_Texture * fond = IMG_LoadTexture(renderer,"./img/forest.jpg");
  if (fond == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", NULL, window, renderer);

  SDL_Texture * fog = NULL;

  int fog_int =0;

  //https://mapledev.tumblr.com/tagged/game
  int img = 0;
  SDL_Texture * tuiles = IMG_LoadTexture(renderer,"./img/TILES.png");
  if (tuiles == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", NULL, window, renderer);
  int taille_case[2];
  SDL_QueryTexture(tuiles, NULL, NULL,&taille_case[0], &taille_case[1]);
  taille_case[0] = taille_case[0]/16;
  taille_case[0] = taille_case[0] * zoom;
  taille_case[1] = taille_case[1] * zoom;
  printf("Taille tuiles %d %d\n",taille_case[0],taille_case[1]);
  SDL_Texture * tuiles_empty = IMG_LoadTexture(renderer,"./img/tileset_empty_corner.png");
  if (tuiles_empty == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", NULL, window, renderer);
  int corner = 2;
  int path = 1; 
  // path == 1 -> affiche tout
  // path == 0 -> affiche que le chemin parcouru

  /* BONHOMME */
  int perso = 0;
  int bouge = 0;
  float frame_bonhomme = 0;
  int direction_bonhomme = 0;
  float zoom_bonhomme = 1 * zoom;
  SDL_Texture * bonhomme = IMG_LoadTexture(renderer,"./img/Yves_jean.png");
  if (bonhomme == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", NULL, window, renderer);
  //https://opengameart.org/content/alternate-lpc-character-sprites-george



  /* PIECES */
  int score = 0;
  float frame_coin = 0;
  float zoom_coin = 1 *zoom;
  SDL_Texture * coin = IMG_LoadTexture(renderer,"./img/spin_coin_big_upscale_strip6.png");
  TTF_Font * font = NULL;  
  font = TTF_OpenFont("./img/Pacifico.ttf", 40);
  if (coin == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", font, window, renderer);
  //coin_t * liste_coin = genere_liste_coin(n*p,n,p);
  

  /* DEPART ET ARRIVEE (DRAPEAUX) */
  //https://opengameart.org/content/flag-animation-sheet
  float frame_flags = 0;
  float zoom_flags = 0.2 *zoom;
  SDL_Texture * flags_da = IMG_LoadTexture(renderer,"./img/flag_blue_purple.png");
  if (flags_da == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", font, window, renderer);
  


  /* TIRS */
  float zoom_tirs = 0.8*zoom;
  tirs_t * mes_tirs = init_tirs();
  SDL_Texture * tirs = IMG_LoadTexture(renderer,"./img/tile_lasers_mirror.png");
  if (tirs == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", font, window, renderer);
  int gun = 0;


  /* ENEMIES */
  float zoom_enemies = 1.3 * zoom;
  SDL_Texture * enemies = IMG_LoadTexture(renderer,"./img/enemies.png");
  if (enemies == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", font, window, renderer);

  /* EXPLOSION */
  float zoom_explosion = 2 * zoom;
  SDL_Texture * explosion = IMG_LoadTexture(renderer,"./img/explo_tile.png");
  if (explosion == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", font, window, renderer);
  explosion_t * liste_explosion = NULL;
  
  /* TRESOR */
  int position_coffre;
  int presence_coffre = 1;
  generation_coffre(mon_laby,&position_coffre,p,n);
  float zoom_coffre = 1.3 * zoom;
  SDL_Texture * coffre = IMG_LoadTexture(renderer,"./img/chest.png");
  if (coffre == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", font, window, renderer);
  

  //------------------------------------------------------------------
  /* DESCRIPTION PROGAMME */
  printf("\n\nDescription du programme :\n");
  printf("Fleches directionnelles pour faire déplacer le bonhomme\n");
  printf("  f pour changer le fond des tuiles\n");
  printf("  n pour changer de personnage\n");
  printf("  v pour toggle le chemin parcouru\n");
  printf("  b pour toggle le brouillard\n");
  printf("  c pour toggle les corners\n");
  printf("  d pour activer le dijkstra\n");
  printf("  a pour activer le a star\n");
  printf("  z pour recuperer toutes les pieces\n");
  printf("  w pour faire le parcour en profondeur\n");
  printf("  g pour changer de pistolet laser\n");
  printf("  k pour slay la game\n");
  printf("  x pour activer l'ia, elle rammasse toutes les pieces en evitant au max les chemins ou il y a des ennemies\net les tues sans pitié sinon\n");
  


  printf("\n\n");




  SDL_bool
  program_on = SDL_TRUE,                          // Booléen pour dire que le programme doit continuer
  paused = SDL_FALSE;                             // Booléen pour dire que le programme est en pause
  while (program_on) {                              // La boucle des évènements
    SDL_Event event;                                // Evènement à traiter

    while (program_on && SDL_PollEvent(&event)) {   // Tant que la file des évènements stockés n'est pas vide et qu'on n'a pas
                                                    // terminé le programme Défiler l'élément en tête de file dans 'event'
      switch (event.type) {                         // En fonction de la valeur du type de cet évènement
      case SDL_QUIT:                                // Un évènement simple, on a cliqué sur la x de la // fenêtre
        program_on = SDL_FALSE;                     // Il est temps d'arrêter le programme
        break;
      case SDL_KEYDOWN:                             // Le type de event est : une touche appuyée
                                                    // comme la valeur du type est SDL_Keydown, dans la pratie 'union' de
                                                    // l'event, plusieurs champs deviennent pertinents   
        switch (event.key.keysym.sym) {             // la touche appuyée est ...
        case SDLK_p:                                // 'p'
          paused = !paused;
          //printf("paused\n");
          break;
        case SDLK_v://toggle l'affichage du chemin parcouru
          path = !path;
          break;
        case SDLK_c://toggle l'affichage des corners
          corner = (corner+1) % 3;
          break;
        case SDLK_b://toggle l'affichage du brouillard
          fog_int = (fog_int+1)% 4;
          SDL_DestroyTexture(fog);
          switch (fog_int){
            case 1:
              fog = IMG_LoadTexture(renderer,"./img/fog_test-tile_diamond.png");
              break;
            case 2:
              fog = IMG_LoadTexture(renderer,"./img/Nebula_Blue_test.png");
              break;
            case 3:
              fog = IMG_LoadTexture(renderer,"./img/Nebula_Blue_etape2xcf.png");
              break;
          }
          if (fog == NULL) {
            end_sdl(0, "Echec du chargement de l'image dans la texture", font, window, renderer);
            program_on = SDL_FALSE;
          } 
          break;
        case SDLK_g://changement de gun
          gun = (gun+1)% 8;
          break;
        case SDLK_n://change de perso;
          perso = (perso +1 )%4;
          SDL_DestroyTexture(bonhomme);
          switch (perso){
            case 0:
              bonhomme = IMG_LoadTexture(renderer,"./img/Yves_jean.png");
              break;
            case 1:
              bonhomme = IMG_LoadTexture(renderer,"./img/Betty.png");
              break;
            case 2:
              bonhomme = IMG_LoadTexture(renderer,"./img/Chricyan.png");
              break;
            case 3:
              bonhomme = IMG_LoadTexture(renderer,"./img/Rainbow.png");
              break;
          }
          if (bonhomme == NULL) {
            end_sdl(0, "Echec du chargement de l'image dans la texture", font, window, renderer);
            program_on = SDL_FALSE;
          } 
          break;
        case SDLK_ESCAPE:                           // 'ESCAPE'  
        case SDLK_q:                                // 'q'
          program_on = 0;                           // 'escape' ou 'q', d'autres façons de quitter le programme                                     
          break;
        case SDLK_LEFT:
          if ((!bouge) && (pos_final[0] > 0) && (!(mon_laby[pos_final[0]][pos_final[1]] & OUEST))&& (direction_bonhomme!=3)){
            pos_final[0] -= 1;
          }
          direction_bonhomme = 1;
          break;
        case SDLK_RIGHT:
          if ((!bouge) && (pos_final[0] < n-1) && (!(mon_laby[pos_final[0]][pos_final[1]] & EST)) && (direction_bonhomme!=1)){
            pos_final[0] += 1;
          }
          direction_bonhomme = 3;
          break;
        case SDLK_UP:
          if ((!bouge) && (pos_final[1] > 0) && (!(mon_laby[pos_final[0]][pos_final[1]] & NORD)) && (direction_bonhomme!=0)){
            pos_final[1] -= 1;
          }
          direction_bonhomme = 2;
          break;
        case SDLK_DOWN:
          if ((!bouge) && (pos_final[1] < p-1) && (!(mon_laby[pos_final[0]][pos_final[1]] & SUD)) && (direction_bonhomme!=2)){
            pos_final[1] += 1;
          }
          direction_bonhomme = 0;
          break;
        case SDLK_k:
          nuke_enemies(&liste_enemies,&liste_explosion,pos_final,n,p);
          break;
        case SDLK_f:
          img = (img +1) %2;
          SDL_DestroyTexture(tuiles);
          SDL_DestroyTexture(tuiles_empty);
          switch(img){
            case 0:
              tuiles = IMG_LoadTexture(renderer,"./img/TILES.png");
              tuiles_empty = IMG_LoadTexture(renderer,"./img/tileset_empty_corner.png");
              break;
            case 1:
              tuiles = IMG_LoadTexture(renderer,"./img/TILES_PURPLE.png");
              tuiles_empty = IMG_LoadTexture(renderer,"./img/tileset_empty_corner_purple.png");
              break;
          }
          if (tuiles == NULL || tuiles_empty ==NULL) {
            end_sdl(0, "Echec du chargement de l'image dans la texture", font, window, renderer);
            program_on = SDL_FALSE;
          } 
          //printf("font change\n");
          break;
        case SDLK_d://DIJKSTRA c'est juste prouff of concept;
           parcour_en_cour =1;
           //printf("ok\n");
           libere_enemies(&liste_enemies);
           tab_path = dijkstra_mat(mon_laby,p,n,np_to_k(pos_final[0],pos_final[1],n),noeud_arr,&indice_noeud_cour);
           //printf("okkk2 : indice %d\n",indice_noeud_cour);
           //affiche_tab(tab_path,n*p);
           //printf("okkk3\n");
           break;
        case SDLK_a://A etoile c'est juste prouff of concept;
           parcour_en_cour =1;
           libere_enemies(&liste_enemies);
           //printf("ok\n");
           tab_path = astar_mat(mon_laby,p,n,np_to_k(pos_final[0],pos_final[1],n),noeud_arr,1,&indice_noeud_cour);
           //printf("okkk2 : indice %d\n",indice_noeud_cour);
           //affiche_tab(tab_path,n*p);
           //printf("okkk3\n");
           break;
        case SDLK_x://x comme xdddddddddddddddddddddd
          printf("ok?\n");
          zoom_explosion *=2;
          enemies_t * enemies_cour = NULL;
          int noeud_suiv = noeud_dep;
          float distance_temp =0.;
          while ((program_on) && ((score != NBR_COINS*10) || (noeud_arr != np_to_k(pos_final[0],pos_final[1],n)))) {
            noeud_suiv = find_closest_coin(mon_laby,n,p,np_to_k(pos_final[0],pos_final[1],n),noeud_arr,liste_enemies);
            distance_temp = 0;
            numDij = 0;
            noeud_suiv = astar_mat_ia(mon_laby,p,n,np_to_k(pos_final[0],pos_final[1],n),noeud_suiv,1,&numDij,liste_enemies,&distance_temp);
            //printf("noud suiv %d\n",noeud_suiv);
            bouge =1;
            while( (program_on) && (bouge)){
              bouge = mise_a_jour_pos(pos_reel,pos_final,&frame_bonhomme,&frame_coin,&frame_flags);

              enemies_cour = liste_enemies;
              while (enemies_cour != NULL){
                if ( 0.5 > distance2(enemies_cour->x_reel,pos_reel[0],enemies_cour ->y_reel,pos_reel[1])){
                  nouveau_tir(mes_tirs,direction_bonhomme,(perso==0)*BLUE + (perso==1)*RED + (perso ==2)*GREEN + (perso == 3)*RAINBOW,gun,0.2,pos_reel[0],pos_reel[1]);
                }
                enemies_cour = enemies_cour->suivant;
              }

              if (bouge ==0) {
                avancer_pos_final(pos_final,n,np_to_k(pos_final[0],pos_final[1],n),noeud_suiv,&direction_bonhomme,-1);
                if (mon_laby[pos_final[0]][pos_final[1]] & COINS){
                  supprime_coin(mon_laby,pos_final);
                  score+=10;
                }
              } 
              if (!(mon_laby[pos_final[0]][pos_final[1]] & EST_PASSE )){
                mon_laby[pos_final[0]][pos_final[1]] += EST_PASSE;
              }
              avancer_tirs(mon_laby,n,p,mes_tirs);
              
              afficher_fond(largeur,hauteur,fond,renderer);
              afficher_laby(mon_laby,largeur,hauteur,n,p, pos_reel,zoom,tuiles,tuiles_empty, renderer,path,corner);
              afficher_coin(mon_laby,n,p,frame_coin,pos_reel,hauteur,largeur,zoom_coin,taille_case,coin,renderer);
              afficher_flags(mon_laby,frame_flags,pos_reel,hauteur,largeur,n,p,zoom_flags,taille_case,flags_da,renderer);
              afficher_coffre(position_coffre,n,zoom_coffre,taille_case,largeur,hauteur,presence_coffre,pos_reel,coffre,renderer);
              afficher_explosion(liste_explosion,zoom_explosion,pos_reel,taille_case,hauteur,largeur,explosion,renderer);
              afficher_tir(hauteur,largeur,pos_reel,taille_case,zoom_tirs,mes_tirs,tirs,renderer);
              if (mise_a_jour_enemies(liste_enemies,pos_final,pos_reel,n,p, mon_laby)){
                program_on = SDL_FALSE;
                lose(renderer,font,largeur,hauteur);
              }
              afficher_enemies(liste_enemies,zoom_enemies,pos_reel,taille_case,hauteur,largeur,enemies,renderer);
              afficher_bonhomme(frame_bonhomme,direction_bonhomme,hauteur,largeur,zoom_bonhomme,bonhomme,renderer);
              
              affichage_score(score,font,window,renderer);
              //mise_a_jour_explosion(&liste_explosion);
              tire_laser_detruit_enemies(&liste_enemies,&liste_explosion,mes_tirs);
              afficherEcran(renderer);
              SDL_Delay(10);

            }

          }
          
          break;
        case SDLK_z:
           libere_enemies(&liste_enemies);
           int nb;
           pos_deep = find_all_coins(mon_laby,n,p,np_to_k(pos_final[0],pos_final[1],n),noeud_arr,&nb);
           int indice_coins=0;
           while (indice_coins+1<nb){
             tab_path = astar_mat(mon_laby,p,n,pos_deep[indice_coins],pos_deep[indice_coins+1],1,&indice_noeud_cour);
             while ((indice_noeud_cour +1< n*p) && (tab_path[indice_noeud_cour] != pos_deep[indice_noeud_cour+1])){
              bouge =1;
              while(bouge){
                bouge = mise_a_jour_pos(pos_reel,pos_final,&frame_bonhomme,&frame_coin,&frame_flags);
                if (bouge ==0) {
                  avancer_pos_final(pos_final,n,tab_path[indice_noeud_cour],tab_path[indice_noeud_cour+1],&direction_bonhomme,-1);
                  if (mon_laby[pos_final[0]][pos_final[1]] & COINS){
                    supprime_coin(mon_laby,pos_final);
                    score+=10;
                  }
                } 
                if (!(mon_laby[pos_final[0]][pos_final[1]] & EST_PASSE )){
                  mon_laby[pos_final[0]][pos_final[1]] += EST_PASSE;
                }
                
                afficher_fond(largeur,hauteur,fond,renderer);
                afficher_laby(mon_laby,largeur,hauteur,n,p, pos_reel,zoom,tuiles,tuiles_empty, renderer,path,corner);
                afficher_coin(mon_laby,n,p,frame_coin,pos_reel,hauteur,largeur,zoom_coin,taille_case,coin,renderer);
                afficher_flags(mon_laby,frame_flags,pos_reel,hauteur,largeur,n,p,zoom_flags,taille_case,flags_da,renderer);
                afficher_coffre(position_coffre,n,zoom_coffre,taille_case,largeur,hauteur,presence_coffre,pos_reel,coffre,renderer);
                afficher_bonhomme(frame_bonhomme,direction_bonhomme,hauteur,largeur,zoom_bonhomme,bonhomme,renderer);
                affichage_score(score,font,window,renderer);
                afficherEcran(renderer);
                SDL_Delay(10);

              }
              indice_noeud_cour ++;
            }
            free(tab_path);
            tab_path = NULL;
            nettoyage(mon_laby,p,n,&numDij,&numA);
            numDij = 0;
            numA =0;
            indice_coins++;
           }
          //  event.type = SDL_KEYDOWN;
          //  event.key.keysym.sym = SDLK_a;
          //  SDL_PushEvent(&event);
           break;
        case SDLK_w://Parcour en profondeur
           program_on = SDL_FALSE;
          //  printf("ok\n");
          //  deep_first_search(mon_laby,n,p,np_to_k(pos_final[0],pos_final[1],n));
          //  printf("okkk2\n");
          //parcour_en_cour = 1;

          int nbr_noeuds = n*p;
          pile_t * maPile = initPile(nbr_noeuds+2);
          element_pile_t val;
          val.nbr = np_to_k(pos_final[0],pos_final[1],n);
          int code;
          int i,j;
          int nouv;
          int bouge;
          int ancien;
          empiler(maPile,val,&code);
          k_to_np(val.nbr,&i,&j,n);
          //printf("origine : %d %d %d\n",val.nbr,i,j);
          while (!estVidePile(maPile)){
            nouv = 0;
            depiler(maPile,&val,&code);
            empiler(maPile,val,&code);
            k_to_np(val.nbr,&i,&j,n);
            bouge =1;
            ancien = np_to_k(pos_final[0],pos_final[1],n);
            //printf("De %d vers %d\n",ancien,val.nbr);
            avancer_pos_final(pos_final,n,ancien,val.nbr,&direction_bonhomme,-1);
            while(bouge){
              bouge = mise_a_jour_pos(pos_reel,pos_final,&frame_bonhomme,&frame_coin,&frame_flags);
              if (bouge ==0) {
                if (mon_laby[pos_final[0]][pos_final[1]] & COINS){
                  supprime_coin(mon_laby,pos_final);
                  score+=10;
                }
              } 
              if (!(mon_laby[pos_final[0]][pos_final[1]] & EST_PASSE )){
                mon_laby[pos_final[0]][pos_final[1]] += EST_PASSE;
              }
              
              afficher_fond(largeur,hauteur,fond,renderer);
              afficher_laby(mon_laby,largeur,hauteur,n,p, pos_reel,zoom,tuiles,tuiles_empty, renderer,path,corner);
              afficher_coin(mon_laby,n,p,frame_coin,pos_reel,hauteur,largeur,zoom_coin,taille_case,coin,renderer);
              afficher_flags(mon_laby,frame_flags,pos_reel,hauteur,largeur,n,p,zoom_flags,taille_case,flags_da,renderer);
              afficher_coffre(position_coffre,n,zoom_coffre,taille_case,largeur,hauteur,presence_coffre,pos_reel,coffre,renderer);
              afficher_bonhomme(frame_bonhomme,direction_bonhomme,hauteur,largeur,zoom_bonhomme,bonhomme,renderer);
              affichage_score(score,font,window,renderer);
              afficherEcran(renderer);
              SDL_Delay(2);

            }
            //printf("%d|%d| %d %d\n",val.nbr,maPile->sommet, i,j);
            if (!(mon_laby[i][j] & EST_PASSE_PROFONDEUR)) {
              mon_laby[i][j] += EST_PASSE_PROFONDEUR;
            }
            if ((!(mon_laby[i][j] & EST))&&(!(mon_laby[i+1][j] & EST_PASSE_PROFONDEUR))){
              //printf("EST %d\n",laby[i][j]);
              val.nbr = np_to_k(i+1,j,n);
              empiler(maPile,val,&code);
              nouv =1;
            } else {
            if ((!(mon_laby[i][j] & OUEST))&&(!(mon_laby[i-1][j] & EST_PASSE_PROFONDEUR))){
              //printf("OUEST %d\n",laby[i][j]);
              val.nbr = np_to_k(i-1,j,n);
              empiler(maPile,val,&code);
              nouv=1;
            } else {
            if ((!(mon_laby[i][j] & SUD))&&(!(mon_laby[i][j+1] & EST_PASSE_PROFONDEUR))){
              //printf("SUD %d\n",laby[i][j]);
              val.nbr = np_to_k(i,j+1,n);
              empiler(maPile,val,&code);
              nouv=1;
            } else {
            if ((!(mon_laby[i][j] & NORD))&&(!(mon_laby[i][j-1] & EST_PASSE_PROFONDEUR))){
              //printf("NORD %d\n",laby[i][j]);
              val.nbr = np_to_k(i,j-1,n);
              empiler(maPile,val,&code);
              nouv=1;
            }}}}
            if (!nouv){
                depiler(maPile,&val,&code);
                //printf("back\n");
            }
            //printf("code : %d\n",code);
          }
          printf("Fin frero\n");
          libererPile(maPile);
          break;
        case SDLK_SPACE:
           nouveau_tir(mes_tirs,direction_bonhomme,(perso==0)*BLUE + (perso==1)*RED + (perso ==2)*GREEN + (perso == 3)*RAINBOW,gun,0.2,pos_reel[0],pos_reel[1]);
           break;
        default:                                    // Une touche appuyée qu'on ne traite pas
          break;
        }
      case SDL_MOUSEBUTTONDOWN:                     // Click souris
        if (SDL_GetMouseState(NULL, NULL) & 
            SDL_BUTTON(SDL_BUTTON_LEFT) ) {         // Si c'est un click gauche
        //   //printf("clickgauche\n");
        //   tirer(tab_tirs,&nombre_actuel_tirs,position,0.8*hauteur - (383/2)*zoom_vaisseau);
        }
        break;
      default:                                      // Les évènements qu'on n'a pas envisagé
        
        break;
      }
    }
    //action
    if (!paused) {                                  // Si on n'est pas en pause
        do {
          if ((parcour_en_cour) && (bouge==0)){
            //SDL_Delay(100); 
            if (indice_noeud_cour < (N_LABY * P_LABY) -1){
              parcour_en_cour = avancer_pos_final(pos_final,n,tab_path[indice_noeud_cour],tab_path[indice_noeud_cour+1],&direction_bonhomme,noeud_arr);
              //printf("\n\n%d\n%d\n",indice_noeud_cour,parcour_en_cour);
              SDL_Delay(2);
              ++indice_noeud_cour;
            } else {
              parcour_en_cour =0;
            }
            
          }
          bouge = mise_a_jour_pos(pos_reel,pos_final,&frame_bonhomme,&frame_coin,&frame_flags);
          if (bouge ==0) {
            if (mon_laby[pos_final[0]][pos_final[1]] & COINS){
              supprime_coin(mon_laby,pos_final);
              score+=10;
            }
            if ((!(parcour_en_cour))&&(presence_coffre)&&(collision_coffre(pos_final,position_coffre,n,&presence_coffre))){
                  printf("Le roi des pirates\n");
                  tresor(mon_laby,position_coffre,noeud_arr,p,n);
            }
            if (mon_laby[pos_final[0]][pos_final[1]] & ARRIVEE){
                      program_on = SDL_FALSE;
            }
          }
          if (!(mon_laby[pos_final[0]][pos_final[1]] & EST_PASSE )){
            mon_laby[pos_final[0]][pos_final[1]] += EST_PASSE;
          }
          avancer_tirs(mon_laby,n,p,mes_tirs);
          tire_laser_detruit_enemies(&liste_enemies,&liste_explosion,mes_tirs);
          afficher_fond(largeur,hauteur,fond,renderer);
          afficher_laby(mon_laby,largeur,hauteur,n,p, pos_reel,zoom,tuiles,tuiles_empty, renderer,path,corner);
          afficher_coin(mon_laby,n,p,frame_coin,pos_reel,hauteur,largeur,zoom_coin,taille_case,coin,renderer);
          afficher_flags(mon_laby,frame_flags,pos_reel,hauteur,largeur,n,p,zoom_flags,taille_case,flags_da,renderer);
          afficher_coffre(position_coffre,n,zoom_coffre,taille_case,largeur,hauteur,presence_coffre,pos_reel,coffre,renderer);
          afficher_tir(hauteur,largeur,pos_reel,taille_case,zoom_tirs,mes_tirs,tirs,renderer);
          if (mise_a_jour_enemies(liste_enemies,pos_final,pos_reel,n,p, mon_laby)){
            program_on = SDL_FALSE;
            lose(renderer,font,largeur,hauteur);
          }
          afficher_enemies(liste_enemies,zoom_enemies,pos_reel,taille_case,hauteur,largeur,enemies,renderer);
          afficher_explosion(liste_explosion,zoom_explosion,pos_reel,taille_case,hauteur,largeur,explosion,renderer);
          afficher_bonhomme(frame_bonhomme,direction_bonhomme,hauteur,largeur,zoom_bonhomme,bonhomme,renderer);
          if (parcour_en_cour) afficher_parcours(mon_laby,largeur,hauteur,n,p,pos_reel,taille_case,renderer,EST_PASSE_DIJ);
          if (parcour_en_cour) afficher_parcours(mon_laby,largeur,hauteur,n,p,pos_reel,taille_case,renderer,EST_PASSE_A);


          if (fog_int) afficher_fog(largeur,hauteur,fog,renderer);

          affichage_score(score, font, window, renderer);
          mise_a_jour_explosion(&liste_explosion);
          afficherEcran(renderer);

        } while (parcour_en_cour);
        
        if (tab_path != NULL){
          SDL_Delay(1000);
          // exit(0);
          nettoyage(mon_laby,p,n,&numDij,&numA);
          numDij=0;
          numA=0;
          free(tab_path);

          tab_path = dijkstra_mat(mon_laby,p,n,noeud_dep,noeud_arr,&indice_noeud_cour);
          free(tab_path);
          tab_path = astar_mat(mon_laby,p,n,noeud_dep,noeud_arr,1,&indice_noeud_cour);
          free(tab_path);
          nettoyage(mon_laby,p,n,&numDij,&numA);
          printf("Nombre de case visite:\nDij : %d\nA : %d\n",numDij, numA );
          printf("Rapports :\nDij : %f\nA : %f\n",1., ((float) numA)/(float) numDij );
          tab_path = NULL;
          printf("ENNNNND\n");
          program_on = SDL_FALSE;
        }
    }
    SDL_Delay(16);                                  // Petite pause
  }
  
  int indice_de_free;
  for(indice_de_free=0;indice_de_free<n;indice_de_free++){
    free(mon_laby[indice_de_free]);
  }
  free(mon_laby);
  SDL_DestroyTexture(explosion);
  SDL_DestroyTexture(coffre);
  SDL_DestroyTexture(fog);
  SDL_DestroyTexture(fond);
  SDL_DestroyTexture(enemies);
  SDL_DestroyTexture(tirs);
  SDL_DestroyTexture(bonhomme);
  SDL_DestroyTexture(tuiles);
  SDL_DestroyTexture(flags_da);
  SDL_DestroyTexture(tuiles_empty);
  
  free_tirs(mes_tirs);
  libere_enemies(&liste_enemies);
  printf("Milieu\n");
  end_sdl(1, "Normal ending", font, window, renderer);//renderer
  SDL_Quit();
  printf("Real end\n");
  return 0;
}

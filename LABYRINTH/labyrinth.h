#ifndef __GARROS_HANOCQ_BOUDIN_LABYRINTH_H
#define __GARROS_HANOCQ_BOUDIN_LABYRINTH_H

#define N_LABY 20
#define P_LABY 16
#define ZOOM 1
#define PROBA 0.2
#define NOEUD_DEPART ( -1 )//(N_LABY+1)
#define NOEUD_ARRIVEE ( -1 )//(N_LABY*P_LABY -1)
#define NBR_COINS 20
#define NBR_ENNEMIS 10 //(N_LABY * P_LABY -5)
//si ca vaut -1 alors ca sera random

#define NORD 1
#define OUEST 2
#define SUD 4
#define EST 8
#define DEPART 16
#define ARRIVEE 32
#define COINS 64
#define EST_PASSE 128
#define EST_PASSE_DIJ 256
#define EST_PASSE_A 512
#define EST_PASSE_PROFONDEUR 1024


#include "graph_liste.h"
#include "distance.c"


void affiche_tab(int * tab, int taille);

void nettoyage(int** laby, int lignes, int colonnes,int * numDij,int *numA);

int * renverser_predesc(int * tab,int taille,int depart, int arrive,int * indice_noeud);

int* dijkstra_mat(int ** laby, int lignes, int colonnes, int origine, int arrivee,int * indice_noeud_cour);

int* astar_mat(int ** laby, int lignes, int colonnes, int origine, int arrivee, int dist,int *indice_noeud);

int avancer_pos_final(int pos_final[2],int n,int noeud_courant,int noeud_futur,int * direction_bonhomme,int arrivee);


#endif
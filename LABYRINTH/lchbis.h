#ifndef LISTECHAINNEBIS_H
#define LISTECHAINNEBIS_H
#include <stdlib.h>
#include <stdio.h>

typedef struct couple
{
    int noeud1;
    int noeud2;
    float val;
}couple_t;

typedef struct element{
    couple_t couple ;
    struct element * suivant;
}element_t;

/* les operations*/

void insercellule_bis(element_t **prec, element_t *nouv);

void suppcellule_bis(element_t **prec);

element_t ** rechprec_bis(element_t **adtete, int val);

void insertion_trie_bis(element_t **l,element_t * cour);

element_t * creCellule_bis(int n1, int n2, float v);

void afficheliste_bis(element_t * tete);

void freeliste_bis(element_t ** l);

int longueur_bis(element_t * liste);

element_t ** va_chercher(element_t ** liste, int n);
//ele_t ** rechusine( ele_t ** adtete, int usine);
//void sauv ( ele_t * tete, char * nomfic);
#endif

#ifndef __GARROS_HANOCQ_PILE_H
#define __GARROS_HANOCQ_PILE_H

#include <stdlib.h>
#include <stdio.h>

typedef struct element_pile {
    int nbr;
} element_pile_t;

typedef struct pile {
    int taille;
    int sommet;
    element_pile_t * base;
} pile_t;



pile_t * initPile(int taille);

int estVidePile(pile_t * p);

void empiler(pile_t * p, element_pile_t val ,int * code);

void depiler (pile_t * p, element_pile_t *val ,int * code);

void libererPile(pile_t * p);

void afficherPile(pile_t *p);

#endif

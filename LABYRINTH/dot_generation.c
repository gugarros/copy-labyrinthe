#ifndef __GARROS_HANOCQ_BOUDIN_DOT_GENERATION_H
#define __GARROS_HANOCQ_BOUDIN_DOT_GENERATION_H


#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

void generation_dot(char * nom){

    char * nom_png = (char *) malloc(sizeof(char)* (strlen(nom)+4));
    strcpy(nom_png,nom);
    strcat(nom_png,".png");
    
    char * nom_dot = (char *) malloc(sizeof(char)* (strlen(nom)+4));
    strcpy(nom_dot,nom);
    strcat(nom_dot,".dot");


    char *progName = "dot";
    char *args[] = {progName, "-Tpng", nom_dot, "-o", nom_png, NULL};
    

    int state;
    pid_t process_id = fork();
    if (process_id == -1) { //when process id is negative, there is an error, unable to fork
        printf("can't fork, error occured\n");
        exit(EXIT_FAILURE);
    } else if (process_id == 0) {
        execvp(progName,args);
        exit(0);
    } else { //for the parent process
        if (waitpid(process_id, &state, 0) > 0) { //wait untill the process change its state
            if (WIFEXITED(state) && !WEXITSTATUS(state))
                printf("Generation successfully\n");
            else if (WIFEXITED(state) && WEXITSTATUS(state)) {
                if (WEXITSTATUS(state) == 127) {
                    printf("Execution failed\n");
                } else
                     printf("program terminated with non-zero status\n");
            } else
                 printf("program didn't terminate normally\n");
        } else 
            printf("waitpid() function failed\n");
    }
    //execlp("rm","rm",nom_dot,NULL); // peut se mettre en commentaire si besoin
    
    printf("Generation du dot : %s terminée\n",nom_png);

    free(nom_dot);
    free(nom_png);
}
#endif
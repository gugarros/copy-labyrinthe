﻿#ifndef LISTECHAINNE_H
#define LISTECHAINNE_H
#include <stdlib.h>
#include <stdio.h>


typedef struct ele{
    int cout;
    struct ele *suivant ;
}ele_t;

/* les operations*/

void insercellule (ele_t **prec, ele_t *nouv);
void suppcellule (ele_t **prec);
ele_t ** rechprec (ele_t **adtete, int val);
ele_t * creCellule (int v);
void afficheliste(ele_t * tete);
void freeliste(ele_t ** l);
//ele_t ** rechusine( ele_t ** adtete, int usine);
//void sauv ( ele_t * tete, char * nomfic);
#endif

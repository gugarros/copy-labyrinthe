#include "tirer.h"

tirs_t * init_tirs(){
    tirs_t * mes_tirs = (tirs_t * ) malloc(sizeof(tirs_t));
    if (mes_tirs != NULL){
        mes_tirs -> droite = 0;
        //mes_tirs -> gauche = 0;
        //mes_tirs -> nb_elements = 0;
        mes_tirs -> tab = (tir_t*) malloc(sizeof(tir_t)*TIRS_MAX);
        if (mes_tirs ->tab == NULL){
            free(mes_tirs);
            mes_tirs = NULL;
        }
    }
    return mes_tirs;
}
int est_plein_tirs(tirs_t* mes_tirs){
    return (mes_tirs ->droite == TIRS_MAX);
}


void nouveau_tir(tirs_t * mes_tirs,int direction, int couleur, int type, float vitesse,float posx,float posy){
    //couleurs : R G B RAINBOW
    //type entre 0 et 7 ( 8 types differents)
    if (!est_plein_tirs(mes_tirs)){
        (mes_tirs ->tab)[mes_tirs ->droite].type = T_LARGE*(type%2) + T_FULL*((type/2)%2) + T_SMALL*((type/4)%4)+ couleur;
        if (direction % 2==0) (mes_tirs ->tab)[mes_tirs ->droite].type += VERTICAL;
        if ((direction ==1) || (direction ==2)) (mes_tirs ->tab)[mes_tirs ->droite].type += SENS;//GAUUCHE ET HAUT
        (mes_tirs ->tab)[mes_tirs ->droite].vitesse = vitesse;
        (mes_tirs ->tab)[mes_tirs ->droite].posx = posx;
        (mes_tirs ->tab)[mes_tirs ->droite].posy = posy;
        mes_tirs -> droite +=1;
    }
}

void supprime_tir(tirs_t * mes_tirs, int indice){
    if (mes_tirs -> droite != 0){
        mes_tirs -> droite -= 1;
        (mes_tirs ->tab)[indice].type = (mes_tirs ->tab)[mes_tirs ->droite].type;
        (mes_tirs ->tab)[indice].vitesse = (mes_tirs ->tab)[mes_tirs ->droite].vitesse;
        (mes_tirs ->tab)[indice].posx = (mes_tirs ->tab)[mes_tirs ->droite].posx;
        (mes_tirs ->tab)[indice].posy = (mes_tirs ->tab)[mes_tirs ->droite].posy;
    } else {
        printf("demande de suppr de tirs alors que c'est vide\n");
    }
}

void avancer_tirs(int **laby, int n,int p,tirs_t * mes_tirs){
    int x,y;
    for(int i = 0;i<mes_tirs ->droite;++i){
        x =  arrondir(mes_tirs ->tab[i].posx);
        y =  arrondir(mes_tirs ->tab[i].posy);
        //printf("||%d %f\n%d %f\n",x,mes_tirs ->tab[i].posx,y,mes_tirs ->tab[i].posy);
        if ((x>=0)&&(x<n)&&(y>=0)&&(y<p)) {//dans le laby
            if (mes_tirs ->tab[i].type & VERTICAL){//VERTICAL
                if (mes_tirs ->tab[i].type & SENS){//HAUT
                    if (laby[x][y] & NORD){
                        supprime_tir(mes_tirs,i);
                        //--i;
                    } else {
                        mes_tirs ->tab[i].posy -= mes_tirs ->tab[i].vitesse;
                    }
                } else {//BAS
                    if (laby[x][y] & SUD){
                        supprime_tir(mes_tirs,i);
                        //--i;
                    } else {
                        mes_tirs ->tab[i].posy += mes_tirs ->tab[i].vitesse;
                    }
                }
            } else {//HORIZONTAL
                if (mes_tirs ->tab[i].type & SENS){//GAUCHE
                    if (laby[x][y] & OUEST){
                        supprime_tir(mes_tirs,i);
                        //--i;
                    } else {
                        mes_tirs ->tab[i].posx -= mes_tirs ->tab[i].vitesse;
                    }
                } else {//DROITE
                    if (laby[x][y] & EST){
                        supprime_tir(mes_tirs,i);
                        //--i;
                    } else {
                        mes_tirs ->tab[i].posx += mes_tirs ->tab[i].vitesse;
                    }
                }
            }
        } else {// dehors, a suppr
            supprime_tir(mes_tirs,i);
            //--i;
        }
    }   
}

void free_tirs( tirs_t * mes_tirs){
    free(mes_tirs ->tab);
    free(mes_tirs);
}

void afficher_tir(int hauteur, int longueur, float pos_reel[2], int taille_case[2],float zoom_tirs, tirs_t * mes_tirs,SDL_Texture * tirs, SDL_Renderer* renderer){
    SDL_Rect source = {0},                         // Rectangle définissant la zone de la texture à récupérer
            destination = {0};                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
    
    source.w=57;
    source.h=57;

    destination.w=zoom_tirs * source.w;
    destination.h=zoom_tirs * source.h;
    for(int i = 0;i < mes_tirs -> droite ;++i){
        source.x = source.w *((mes_tirs->tab[i].type & VERTICAL) + (mes_tirs->tab[i].type &( T_SMALL | T_FULL | T_LARGE))/ T_LARGE);
        if (mes_tirs->tab[i].type & RAINBOW){
            source.y = source.h *((rand() % RAINBOW )-1);
        } else {
            source.y = source.h *((mes_tirs->tab[i].type % RAINBOW )-1);
        }
        destination.x=(mes_tirs->tab[i].posx - pos_reel[0])*taille_case[0] + (longueur)/2 -zoom_tirs * source.w/2;
        destination.y=(mes_tirs->tab[i].posy - pos_reel[1])*taille_case[1] + (hauteur)/2 -zoom_tirs * source.h/2;
        
        SDL_RenderCopy(renderer, tirs, &source, &destination);
    }    
}
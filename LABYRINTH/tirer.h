#ifndef __GARROS_HANOCQ_BOUDIN_TIRER_H
#define __GARROS_HANOCQ_BOUDIN_TIRER_H

#include "labyrinth.h"
#include "distance.c"
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#define TIRS_MAX 42
#define RED 3
#define GREEN 2
#define BLUE 1
#define RAINBOW 4
#define VERTICAL 8
#define SENS 16
// HORIZONTAL == ! VERTICAL
// SENS -> haut ou bas XOR gauche ou droite ( en fct de vertical )
#define T_LARGE 32 // != NOT LARGE
#define T_FULL 64 // != WHITE inside
#define T_SMALL 128 // != LONG


typedef struct tir {
    int type;
    float vitesse;
    float posx;
    float posy;
} tir_t;

typedef struct tirs {
    tir_t * tab;
    //int gauche;
    int droite;
}tirs_t;

tirs_t * init_tirs();

int est_plein_tirs(tirs_t* mes_tirs);

void nouveau_tir(tirs_t * mes_tirs,int direction, int couleur, int type, float vitesse,float posx,float posy);

void supprime_tir(tirs_t * mes_tirs, int indice);

void avancer_tirs(int **laby, int n,int p,tirs_t * mes_tirs);

void free_tirs( tirs_t * mes_tirs);

void afficher_tir(int hauteur, int longueur, float pos_reel[2], int taille_case[2],float zoom_tirs, tirs_t * mes_tirs,SDL_Texture * tirs, SDL_Renderer* renderer);



#endif
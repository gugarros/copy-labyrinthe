#ifndef __GARROS_HANOCQ_BOUDIN_TAS_H
#define __GARROS_HANOCQ_BOUDIN_TAS_H

#include <stdlib.h>

typedef struct tas
{
    int tailleMax;
    int taille;
    int * tab;
}tas_t;


tas_t*  init_tas(int tailleMax);

void echange_tas(tas_t * monTas,int i,int j);


void insere_tas(tas_t * monTas, int x);

void entasse_tas(tas_t * monTas, int i);

int extraitmin(tas_t * monTas);

void affiche_tas(tas_t * monTas);


void tri_par_tas(int * tab,int taille);


void free_tas(tas_t * monTas);

void affiche_tab(int * tab, int taille);


#endif

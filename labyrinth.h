#ifndef __GARROS_HANOCQ_BOUDIN_LABYRINTH_H
#define __GARROS_HANOCQ_BOUDIN_LABYRINTH_H

#include <stdlib.h>
#include "lch.h"

typedef struct couple
{
    int noeud;
    ele_t liste;
}couple_t;


typedef struct labyrinth
{
    int n; //nb de noeud
    couple_t * tab; //graphe 
} labyrinth_t;


#endif

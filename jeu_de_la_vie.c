#include <SDL2/SDL.h>
#include <stdio.h>

//gcc jeu_de_la_vie.c -o jeu_de_la_vie -lSDL2

int modulo(int a, int b){
    if (a>=0){
        while (a>=b){
            a=a-b;
        }
    }
    else{
        while (a<0){
            a+=b;
        }
    }
    return a;
}

void creer_tableau(int ** tab, int ligne,int colonne){
    int i,j;
    for (i=0;i<ligne;i++){
        for (j=0;j<colonne;j++){
            tab[i][j]=0;
        }
    }
}

void copie_tab(int ** tab, int ** copie, int ligne, int colonne){
    int i,j;
    for (i=0;i<ligne;i++){
        for (j=0;j<colonne;j++){
            copie[i][j]=tab[i][j];
        }
    }
}

int nmbr_voisins(int ** tab,int ligne,int colonne, int i, int j){
    int s = 0;
    int k,m;
    for (k=i-1; k<=i+1; k++){
        for (m=j-1; m<=j+1; m++){
            if (((m!=j)||(k!=i))&&(tab[modulo(k,ligne)][modulo(m,colonne)]>0)){
                s+=1;
            }
        }
    }
    return s;
}

void etape_suivante(int ** tab, int ligne, int colonne){
    int i,j;
    int ** copie = (int **) malloc(ligne*sizeof(int *));
    if (copie != NULL){
        for (i=0; i<ligne; i++){
            copie[i] = (int *) malloc(colonne*sizeof(int));
        }
    }
    copie_tab(tab,copie,ligne,colonne);
    int n;
    for (i=0; i<ligne; i++){
        for (j=0; j<colonne; j++){
            n=nmbr_voisins(copie,ligne,colonne,i,j);
            if ((n==3)||((copie[i][j]==1)&&(n==2))) {
                tab[i][j]=1;
            }
            else{
                tab[i][j]=0;
            }
        }
    }
}











void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}                                                                 

void draw(SDL_Renderer* renderer, int ** tab, int ligne, int colonne) {                                   // Je pense que vous allez faire moins laid :)
  SDL_Rect rectangle;                                             

  SDL_SetRenderDrawColor(renderer,                                
                              255, 255, 255,                               // mode Red, Green, Blue (tous dans 0..255)
                              255);                                   // 0 = transparent ; 255 = opaque
  rectangle.x = 0;                                                    // x haut gauche du rectangle
  rectangle.y = 0;                                                    // y haut gauche du rectangle
  rectangle.w = 10*colonne;                                                  // sa largeur (w = width)
  rectangle.h = 10*ligne;                                                  // sa hauteur (h = height)

  SDL_RenderFillRect(renderer, &rectangle);

  int i,j;
  for(i=0;i<ligne;i++){
      for (j=0;j<colonne;j++){
          if (tab[i][j]==1){
              SDL_Rect rect;
              SDL_SetRenderDrawColor(renderer,0,0,0,255);
              rect.x=10*j;
              rect.y=10*i;
              rect.w=10;
              rect.h=10;
              SDL_RenderFillRect(renderer,&rect);
          }
      }
  }
}

int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  int ligne=30;
  int colonne=50;

  SDL_Window *window = NULL;               // Future fenêtre

  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
  }

  /* Création de la fenêtre */
  window = SDL_CreateWindow(
      "Jeu de la vie",                    // codage en utf8, donc accents possibles
      0, 0,                                  // coin haut gauche en haut gauche de l'écran
      colonne*10, ligne*10,                              // largeur = 10*colonne, hauteur = 10*ligne
      SDL_WINDOW_RESIZABLE);                 // redimensionnable

  if (window == NULL) {
    SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
    SDL_Quit();
    exit(EXIT_FAILURE);
  }

  /* Création du renderer */
  SDL_Renderer* renderer;
  renderer = SDL_CreateRenderer(
           window, -1, 0);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);


  int ** tab = (int **) malloc(ligne*sizeof(int *));
  if (tab != NULL){
      int k;
      for (k=0;k<ligne;k++){
          tab[k]= (int *) malloc(colonne*sizeof(int));
      }
  }
  creer_tableau(tab,ligne,colonne);
  //On place un vaisseau dans le tableau
  tab[ligne-1][colonne-1]=1;
  tab[ligne-1][colonne-2]=1;
  tab[ligne-2][colonne-3]=1;
  tab[ligne-2][colonne-1]=1;
  tab[ligne-3][colonne-1]=1;
  
  int i;
  for (i=0; i<=100; i++){
    /*********************************************************************************************************************/
    /*                                     On dessine dans le renderer                                                   */
    draw(renderer,tab,ligne,colonne);                                                     // appel de la fonction qui crée l'image  
    SDL_RenderPresent(renderer);                                                          // affichage
    etape_suivante(tab,ligne, colonne);
    SDL_Delay(100);                           // Pause exprimée  en ms
  }
  /*********************************************************************************************************************/
  /* on referme proprement la SDL */
  end_sdl(1, "Normal ending", window, renderer);

  SDL_Quit();
  return 0;
}

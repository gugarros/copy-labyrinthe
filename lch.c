/*******************************************************/
/* Gestion de la liste chainee: operations elementaires*/
/*******************************************************/

#include "lch.h"
#include <stdio.h>
/* adjonction d'une cellule*/
void insercellule (ele_t **prec, ele_t *nouv)
{nouv->suivant=*prec;
  *prec=nouv;
}

/*suppression d'une cellule*/
void suppcellule (ele_t **prec)
{ele_t *cour=*prec;
  *prec=(*prec)->suivant; 
  free(cour);
}


/* recherche du precedent sur le cout pour liste triee en decroissant*/
ele_t ** rechprec (ele_t **adtete, int val)
{ ele_t *cour, **prec;
  cour=*adtete;
  prec=adtete;
  while (cour!=NULL && cour->cout > val)
    {prec=&cour->suivant;
     cour=*prec;
    }
  return prec;
}

/*allocation d'une cellule et initialisation*/
ele_t * creCellule (int v)
{
	ele_t * nouv=(ele_t*) malloc(sizeof(ele_t));
	if (nouv)
	{
	nouv->cout=v;
	nouv->suivant=NULL;
	}
	return nouv;
}


/* affichage liste chainee*/

void afficheliste(ele_t * tete)
{ele_t * cour;
  cour=tete;
  if (cour!=NULL)
    {
      while(cour!=NULL)
	{
	  printf("%d\n",cour->cout);
	  cour=cour->suivant;
	}
	  printf("fin de la liste\n");
    }
  else printf("liste vide\n");
}




void freeliste(ele_t ** l){
  ele_t * tmp;
  while ((*l) != NULL)
  { 
    tmp = (*l) -> suivant;
    free(*l);
    *l = tmp;
  }
}

/*recherche d un element sans critere de tri (ici pour l'usine)*/

/*
ele_t ** rechusine( ele_t ** adtete, int usine)
{ ele_t *cour, **prec;
  cour=*adtete;
  prec=adtete;
  while (cour!=NULL && cour->usine != usine)
    {prec=&cour->suivant;
      cour=*prec;
    }
  return prec;
}
*/

/* suvegarde dans un fichier*/
/*
void sauv ( ele_t * tete, char * nomfic)
{
ele_t * cour;
FILE * fic;
fic = fopen(nomfic, "w");
cour=tete;
while (cour!= NULL)
{
fprintf(fic,"%d %d %d\n",cour->cout, cour->usine, cour->periode);
cour=cour->suivant;
}
fclose(fic);
}
*/
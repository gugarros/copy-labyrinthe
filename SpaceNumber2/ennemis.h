#ifndef __GARROS_HANOCQ_BOUDIN_ENEMIE_H
#define __GARROS_HANOCQ_BOUDIN_ENEMIE_H

#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#include <time.h>

#define ENEMIE_N 10
#define ENEMIE_VAGUE 3

int mouv_ennemis(int tab_ennemis[][2], int taille,SDL_Window* window);

void affiche_ennemis(SDL_Texture* my_texture,SDL_Renderer* renderer,int tab[][2],int taille,float zoom);

void generer_ennemis(int tab_ennemis[][2], float zoom_vaisseau, SDL_Texture* vaisseau, SDL_Window* window);

int collision(int n, int tab_ennemis[][2],float zoom_ennemis, int position, float zoom_vaisseau,SDL_Texture* image_ennemie, SDL_Texture* image_vaisseau, SDL_Window* window);

#endif
 

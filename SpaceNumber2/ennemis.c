#include "ennemis.h"

int mouv_ennemis(int tab_ennemis[][2], int taille,SDL_Window* window){
    int i;
    int over = 0;
    SDL_Rect window_dimensions = {0};
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    for(i=0;i<taille;i++){
        tab_ennemis[i][1]+=2;
        if (tab_ennemis[i][1] >= 0.9*window_dimensions.h) over = 1;
    }
    return over;
}

void affiche_ennemis(SDL_Texture* my_texture,
                            SDL_Renderer* renderer,
                            int tab[][2],
                            int taille,
                            float zoom) {
     SDL_Rect 
           source = {0},                      // Rectangle définissant la zone de la texture à récupérer
           destination = {0};                 // Rectangle définissant où la zone_source doit être déposée dans le renderer

     
     SDL_QueryTexture(my_texture, NULL, NULL,
                      &source.w, &source.h);  // Récupération des dimensions de l'image

                             // Facteur de zoom à appliquer   
    int i;
    for (i=0;i<taille;++i){ 
        destination.w = (int) source.w * zoom;         // La destination est un zoom de la source
        destination.h = (int) source.h * zoom;         // La destination est un zoom de la source
        destination.x = tab[i][0] - destination.w/2;     // La destination est au milieu de la largeur de la fenêtre
        destination.y = tab[i][1] + destination.h / 2;  // La destination est au milieu de la hauteur de la fenêtre

        SDL_RenderCopy(renderer, my_texture,     // Préparation de l'affichage  
                    &source,
                    &destination); 
    }           
}

void generer_ennemis(int tab_ennemis[][2], float zoom_vaisseau, SDL_Texture* vaisseau, SDL_Window* window){
    time_t seed = time(0);
    srand(seed);
    int i;
    int x,y;
    SDL_Rect window_dimensions = {0}, dimension_vaisseau = {0};
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(vaisseau, NULL, NULL,&dimension_vaisseau.w, &dimension_vaisseau.h);  // Récupération des dimensions de l'image
    float Largeur_vaisseau =dimension_vaisseau.w * zoom_vaisseau/9;  //Largeur de l'image de l'enemie a l'ecran
    for (i=0;i<ENEMIE_N;i++){

        x = (int) (((window_dimensions.w - Largeur_vaisseau) * (rand() / (float) RAND_MAX))+Largeur_vaisseau/2);
        y = (int) -(window_dimensions.h/2 * (rand() / (float) RAND_MAX)) ;
        tab_ennemis[i][0]=x;
        tab_ennemis[i][1]=y;
    }
}

int collision(int n, int tab_ennemis[][2],float zoom_ennemis, int position, float zoom_vaisseau,SDL_Texture* image_ennemie, SDL_Texture* image_vaisseau, SDL_Window* window){
    
    int i;
    int collision = 0;

    SDL_Rect window_dimensions = {0}, Haut_vaisseau = {0}, Bas_vaisseau = {0}, Ennemie = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);

    int largeur_vaisseau, hauteur_vaisseau, largeur_ennemie, hauteur_ennemie;
    SDL_QueryTexture(image_vaisseau, NULL, NULL, &largeur_vaisseau, &hauteur_vaisseau);
    SDL_QueryTexture(image_ennemie, NULL, NULL,&largeur_ennemie, &hauteur_ennemie);  // Récupération des dimensions de l'image
    largeur_vaisseau = largeur_vaisseau/9;

    Haut_vaisseau.x = position - zoom_vaisseau*largeur_vaisseau/6;
    Haut_vaisseau.y = window_dimensions.h*0.8;
    Haut_vaisseau.w = zoom_vaisseau*largeur_vaisseau/3;
    Haut_vaisseau.h = zoom_vaisseau*hauteur_vaisseau/2;

    Bas_vaisseau.x = position - zoom_vaisseau*largeur_vaisseau/2;
    Bas_vaisseau.y = Haut_vaisseau.y +zoom_vaisseau*hauteur_vaisseau/2;
    Bas_vaisseau.w = zoom_vaisseau*largeur_vaisseau;
    Bas_vaisseau.h = zoom_vaisseau*hauteur_vaisseau/2;


    for (i=0; i<n; i++){

        Ennemie.x = tab_ennemis[i][0] - zoom_ennemis*largeur_ennemie/2;
        Ennemie.y = tab_ennemis[i][1] + zoom_ennemis*hauteur_ennemie/2;
        Ennemie.w = zoom_ennemis * largeur_ennemie;
        Ennemie.h = zoom_ennemis * hauteur_ennemie;

        if (SDL_HasIntersection(&Ennemie,&Haut_vaisseau) || SDL_HasIntersection(&Ennemie,&Bas_vaisseau)){
            collision = 1;
            break;
        }
    }
    return collision;
}
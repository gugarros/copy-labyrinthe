#ifndef __GARROS_HANOCQ_BOUDIN_TIRER_H
#define __GARROS_HANOCQ_BOUDIN_TIRER_H

#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>



#define TIRS_MAX 100


void suppr(int tab[][2], int taille, int i);

int colision_ennemi(int tab[][2], int x, int y, float zoom_tir, float zoom_ennemis, int taille, SDL_Rect tir, SDL_Rect ennemis);

void charge_tirs(SDL_Texture* my_texture,
                            SDL_Renderer* renderer,
                            int tab[][2],
                            int taille,
                            float zoom);

void tirer(int tab[][2], int *taille,int posx,int posy);

void avance_tirs(int tab[][2], int tab_ennemis[][2],
                float zoom_tir, float zoom_ennemis,
                int *taille, int *taille_ennemis,int *score,
                SDL_Rect tir, SDL_Rect ennemis,int * combo);

#endif
#include "tirer.h"




void suppr(int tab[][2], int taille, int i){
    int j;
    for (j=i;j<taille-1;j++){
        tab[j][0]=tab[j+1][0];
        tab[j][1]=tab[j+1][1];
    }
}

int colision_ennemi(int tab[][2], int x, int y, float zoom_tir, float zoom_ennemis, int taille, SDL_Rect tir, SDL_Rect ennemis){
    int i;
    int res=0;
    for(i=0;((i<taille)&&(res==0));i++){
        int var_y=(y+zoom_tir*tir.h/2>tab[i][1]-zoom_ennemis*ennemis.h/2)&&(y-zoom_tir*tir.h/2<tab[i][1]+zoom_ennemis*ennemis.h/2);
        int var_x=(x+zoom_tir*tir.w/2>tab[i][0]-zoom_ennemis*ennemis.w/2)&&(x-zoom_tir*tir.x/2<tab[i][0]+zoom_ennemis*ennemis.w/2);
        if (var_x&&var_y){
            
            suppr(tab,taille,i);
            res=1;
        }
    }
    return res;
}

void charge_tirs(SDL_Texture* my_texture,
                            SDL_Renderer* renderer,
                            int tab[][2],
                            int taille,
                            float zoom) {
     SDL_Rect 
           source = {0},                      // Rectangle définissant la zone de la texture à récupérer
           destination = {0};                 // Rectangle définissant où la zone_source doit être déposée dans le renderer

     
     SDL_QueryTexture(my_texture, NULL, NULL,
                      &source.w, &source.h);  // Récupération des dimensions de l'image

                             // Facteur de zoom à appliquer   
    int i;
    for (i=0;i<taille;++i){ 
        destination.w = (int) source.w * zoom;         // La destination est un zoom de la source
        destination.h = (int) source.h * zoom;         // La destination est un zoom de la source
        destination.x = tab[i][0] - destination.w/2;     // La destination est au milieu de la largeur de la fenêtre
        destination.y = tab[i][1] + destination.h / 2;  // La destination est au milieu de la hauteur de la fenêtre

        SDL_RenderCopy(renderer, my_texture,     // Préparation de l'affichage  
                    &source,
                    &destination); 
    }           
}

void tirer(int tab[][2], int *taille,int posx,int posy){
    if (*taille<TIRS_MAX){
        tab[*taille][0]=posx;
        tab[*taille][1]=posy;
        *taille+=1;
    }
}

void avance_tirs(int tab[][2], int tab_ennemis[][2],
                float zoom_tir, float zoom_ennemis,
                int *taille, int *taille_ennemis,int *score,
                SDL_Rect tir, SDL_Rect ennemis,int * combo){
    int i;
    for (i=0;i<*taille;i++){
        if (colision_ennemi(tab_ennemis,tab[i][0],tab[i][1],zoom_tir,zoom_ennemis,*taille_ennemis,tir,ennemis)){
            suppr(tab,*taille, i);
            *taille-=1;
            *taille_ennemis-=1;
            *score +=20;
            *combo +=20;
        }
        else { 
            if (tab[i][1]<  -10){ //bizarre
                suppr(tab,*taille,i);
                *taille-=1;
            }
            else{
                tab[i][1]-=10; //1 à modifier pour se déplacer plus vite
            }
        }
        //play_with_texture_2(my_texture, window, renderer, tab[i][0], tab[i][1],zoom_tir);
    }
    /* A déplacer dans le main */

    // SDL_RenderPresent(renderer);             
    // SDL_Delay(1000);                         
    // SDL_RenderClear(renderer);               // Effacer la fenêtre
}
#include <stdio.h>
#include <stdlib.h>
//#include "labyrinth.c"
#include "part.c"
//#include "tas.c"
#include "lch.c"
#include "graph_mat.c"
#include <time.h>

/*
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
*/


int main(int argc, char **argv) {

    (void)argc;
    (void)argv;

    // SDL_version nb;
    // SDL_VERSION(&nb);

    // printf("Version de la SDL : %d.%d.%d\n", nb.major, nb.minor, nb.patch);


    graph_mat_t * monGraph = init_graph_mat_alea(10,0.2);

    affiche_graph_mat(monGraph);
    dot_graph_mat(monGraph,"mon_dot",-1,1);
    part_t * maPart = composante_connexe_graph_mat(monGraph);
    printf("Affichage de la partition\n");
    afficher_partition(maPart);
    printf("Generation des partitions\n");
    dot_partition(maPart,"mes_dot",1);
    free(maPart);

    dot_composante_connexe_graph_mat(monGraph,1);
    free_graph_mat(monGraph);
}
#include "graph_mat.h"
#include "lch.h"
#include "part.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


graph_mat_t * init_empty_graph_mat(int n){
    graph_mat_t * monGraph = (graph_mat_t * ) malloc(sizeof(graph_mat_t));
    int i;
    int j;
    if (monGraph != NULL) {
        monGraph -> n = n;

        monGraph -> tab = (int **) malloc(sizeof(int*) *n);
        if (monGraph -> tab != NULL) {
            for(i=0;i<n;++i){
                monGraph -> tab [i] = (int *) malloc(sizeof(int) * n);
            }
            
            for(i=0;i<n;++i){
                for(j=i;j<n;j++){
                        monGraph ->tab[i][j] = 0;
                    }
                }
            }

        } else {
            printf("Erreur\n");
            free(monGraph);
            monGraph = NULL;
        }
    return monGraph;
}

graph_mat_t * init_graph_mat_alea(int n, float p){
    graph_mat_t * monGraph = init_empty_graph_mat(n);
    int i,j;
    for(i=0;i<n;++i){
        for(j=i;j<n;j++){
            if (i==j) {
                monGraph ->tab[i][j] = 0;
            } else {
                monGraph ->tab[i][j] = ((((float) rand()) / ((float) RAND_MAX)) <p);
                monGraph ->tab[j][i] = monGraph->tab [i][j];
            }
        }
    }
    return monGraph;
}

void affiche_graph_mat(graph_mat_t * monGraph){
    int i,j;
    for(i=0;i<monGraph -> n;++i){
        for(j=0;j<monGraph -> n;++j){
            printf("%d ", monGraph ->tab[i][j]);
            
        }
        printf("\n");
    }
}

void dot_graph_mat(graph_mat_t * monGraph,char *nom,int connexe,int generation){
    //connexe = - 1 affiche tout
    //connexe = i >= 0 affiche la composante si besoin
    //generation == 1 -> genere automatique les dot.png
    //generation == 0 -> affiche la commande
    char * nom_dot = (char *) malloc(sizeof(char)* (strlen(nom)+4));
    strcpy(nom_dot,nom);
    strcat(nom_dot,".dot");
    FILE * fichier = fopen(nom_dot,"w");
    fprintf(fichier,"graph {\n");
    int i,j;
    for(i=0;i<monGraph -> n;++i){
        if ((connexe<0) || (connexe ==i)) fprintf(fichier, "  %d;\n",i);
        for(j=i+1;j<monGraph -> n;++j){
            if (monGraph ->tab[i][j]) fprintf(fichier, "  %d -- %d;\n",i,j);
        }
    }
    fprintf(fichier,"}");
   
    fclose(fichier);

    if (generation){
        generation_dot(nom);
    } else {
        printf("Faite :\ndot -Tpng %s.dot -o %s.png\n",nom,nom);
    }
    free(nom_dot);
}

part_t * composante_connexe_graph_mat(graph_mat_t * monGraph){
    part_t * maPart = init_part(monGraph->n);
    int i,j;
    for(i=0;i<monGraph -> n;++i){
        for(j=i+1;j<monGraph -> n;++j){
            if (monGraph ->tab[i][j]) fusioner_part(maPart,i,j);
        }
    }
    return maPart;
}


void copie_graph_mat(graph_mat_t * original, graph_mat_t * copie, ele_t * liste){
    int i,j;
    for(i=0;i< original ->n;++i){
        for(j=0;j< original ->n;++j){
            copie -> tab[i][j] = 0;
        }
    }
    while(liste != NULL){
        for(i=0;i<original ->n;++i){
            copie ->tab[i][liste->cout] = original->tab[i][liste->cout];
            copie ->tab[liste->cout][i] = original->tab[liste->cout][i];
        }
        liste = liste ->suivant;
    }
}

void dot_composante_connexe_graph_mat(graph_mat_t *monGraph,int generation){
    //generation == 1 -> genere automatique les dot.png
    //generation == 0 -> affiche la commande
    int k;
    part_t * maPart = composante_connexe_graph_mat(monGraph);
    ele_t ** maListe = lister_partition(maPart,&k);
    char vrai_nom[] = "mesPart";
    char nom[12];
    strcpy(nom,vrai_nom);
    char nombre[4];
    graph_mat_t * copie = init_empty_graph_mat(monGraph->n);
    for(k=0;k<monGraph->n;++k){
        if (maListe[k] !=NULL){
            copie_graph_mat(monGraph,copie,maListe[k]);
            sprintf(nombre, "%d", maListe[k]->cout);
            strcpy(nom,vrai_nom);
            strcat(nom,nombre);
            dot_graph_mat(copie,nom,k,generation);
        }
    }
}

void free_graph_mat(graph_mat_t * monGraph){
    for(int i=0;i<monGraph ->n;++i){
        free(monGraph ->tab[i]);
    }
    free(monGraph);
}
#include <SDL2/SDL.h>
#include <stdio.h>

int modulo(int a, int b){
    if (a>=0){
        while (a>=b){
            a=a-b;
        }
    }
    else{
        while (a<0){
            a+=b;
        }
    }
    return a;
}

void creer_tableau(int ** tab, int ligne,int colonne){
    int i,j;
    for (i=0;i<ligne;i++){
        for (j=0;j<colonne;j++){
            tab[i][j]=0;
        }
    }
}

void copie_tab(int ** tab, int ** copie, int ligne, int colonne){
    int i,j;
    for (i=0;i<ligne;i++){
        for (j=0;j<colonne;j++){
            copie[i][j]=tab[i][j];
        }
    }
}

int nmbr_voisins(int ** tab,int ligne,int colonne, int i, int j){
    int s = 0;
    int k,m;
    for (k=i-1; k<=i+1; k++){
        for (m=j-1; m<=j+1; m++){
            if (((m!=j)||(k!=i))&&(tab[modulo(k,ligne)][modulo(m,colonne)]>0)){
                s+=1;
            }
        }
    }
    return s;
}

void etape_suivante(int ** tab, int ligne, int colonne){
    int i,j;
    int ** copie = (int **) malloc(ligne*sizeof(int *));
    if (copie != NULL){
        for (i=0; i<ligne; i++){
            copie[i] = (int *) malloc(colonne*sizeof(int));
        }
    }
    copie_tab(tab,copie,ligne,colonne);
    int n;
    for (i=0; i<ligne; i++){
        for (j=0; j<colonne; j++){
            n=nmbr_voisins(copie,ligne,colonne,i,j);
            if ((n==3)||((copie[i][j]==1)&&(n==2))) {
                tab[i][j]=1;
            }
            else{
                tab[i][j]=0;
            }
        }
    }
    for (i=0;i<ligne;i++){
      free(copie[i]);
    }
    free(copie);
}

void change_state(int ** tab, int dg, int x, int y, int zoom){
    int j=x/zoom,i=y/zoom;
    if (dg==1) {
      tab[i][j]=1;
    }
    else{
      tab[i][j]=0;
    }
}









void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}                                                                 

void draw(SDL_Renderer* renderer, int ** tab, int ligne, int colonne, int zoom) {                                   // Je pense que vous allez faire moins laid :)
  SDL_Rect rectangle;                                             

  SDL_SetRenderDrawColor(renderer,                                
                              255, 255, 255,                               // mode Red, Green, Blue (tous dans 0..255)
                              255);                                   // 0 = transparent ; 255 = opaque
  rectangle.x = 0;                                                    // x haut gauche du rectangle
  rectangle.y = 0;                                                    // y haut gauche du rectangle
  rectangle.w = zoom*colonne;                                                  // sa largeur (w = width)
  rectangle.h = zoom*ligne;                                                  // sa hauteur (h = height)

  SDL_RenderFillRect(renderer, &rectangle);

  int i,j;
  for(i=0;i<ligne;i++){
      for (j=0;j<colonne;j++){
          if (tab[i][j]==1){
              SDL_Rect rect;
              SDL_SetRenderDrawColor(renderer,0,0,0,255);
              rect.x=zoom*j;
              rect.y=zoom*i;
              rect.w=zoom;
              rect.h=zoom;
              SDL_RenderFillRect(renderer,&rect);
          }
      }
  }
}

int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  int ligne=30;
  int colonne=50;
  int zoom=10;

  SDL_Window *window = NULL;               // Future fenêtre
  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
  }

  /* Création de la fenêtre */
  window = SDL_CreateWindow(
      "Fenêtre à gauche",                    // codage en utf8, donc accents possibles
      0, 0,                                  // coin haut gauche en haut gauche de l'écran
      colonne*zoom, ligne*zoom,                              // largeur = 400, hauteur = 300
      SDL_WINDOW_RESIZABLE);                 // redimensionnable
  if (window == NULL) {
    SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
    SDL_Quit();
    exit(EXIT_FAILURE);
  }

  /* Création du renderer */
  SDL_Renderer* renderer;
  renderer = SDL_CreateRenderer(
           window, -1, 0);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

  /*********************************************************************************************************************/
  /*                                     On dessine dans le renderer                                                   */
  int ** tab = (int **) malloc(ligne*sizeof(int *));
  if (tab != NULL){
      int k;
      for (k=0;k<ligne;k++){
          tab[k]= (int *) malloc(colonne*sizeof(int));
      }
  }
  creer_tableau(tab,ligne,colonne);
  //On place un vaisseau dans le tableau
  tab[ligne-1][colonne-1]=1;
  tab[ligne-1][colonne-2]=1;
  tab[ligne-2][colonne-3]=1;
  tab[ligne-2][colonne-1]=1;
  tab[ligne-3][colonne-1]=1;
  
  SDL_bool
  program_on = SDL_TRUE,                          // Booléen pour dire que le programme doit continuer
  paused = SDL_FALSE;                             // Booléen pour dire que le programme est en pause
while (program_on) {                              // La boucle des évènements
  SDL_Event event;                                // Evènement à traiter

  while (program_on && SDL_PollEvent(&event)) {   // Tant que la file des évènements stockés n'est pas vide et qu'on n'a pas
                                                  // terminé le programme Défiler l'élément en tête de file dans 'event'
    switch (event.type) {                         // En fonction de la valeur du type de cet évènement
    case SDL_QUIT:                                // Un évènement simple, on a cliqué sur la x de la // fenêtre
      program_on = SDL_FALSE;                     // Il est temps d'arrêter le programme
      break;
    case SDL_KEYDOWN:                             // Le type de event est : une touche appuyée
                                                  // comme la valeur du type est SDL_Keydown, dans la pratie 'union' de
                                                  // l'event, plusieurs champs deviennent pertinents   
      switch (event.key.keysym.sym) {             // la touche appuyée est ...
      case SDLK_p:                                // 'p'
      case SDLK_SPACE:                            // 'SPC'
        paused = !paused;                         // basculement pause/unpause
        break;
      case SDLK_ESCAPE:                           // 'ESCAPE'  
      case SDLK_q:                                // 'q'
        program_on = 0;                           // 'escape' ou 'q', d'autres façons de quitter le programme                                     
        break;
      default:                                    // Une touche appuyée qu'on ne traite pas
        break;
      }
    case SDL_MOUSEBUTTONDOWN:                     // Click souris
      if (SDL_GetMouseState(NULL, NULL) & 
          SDL_BUTTON(SDL_BUTTON_LEFT) ) {         // Si c'est un click gauche
        int x,y;
        SDL_GetMouseState(&x, &y);   
        change_state(tab, 1, x, y, zoom);           // Fonction à éxécuter lors d'un click gauche
      } else if (SDL_GetMouseState(NULL, NULL) & 
                 SDL_BUTTON(SDL_BUTTON_RIGHT) ) { // Si c'est un click droit
        int x,y;
        SDL_GetMouseState(&x, &y);   
        change_state(tab, 2, x, y, zoom);           // Fonction à éxécuter lors d'un click droit
      }
      break;
    default:                                      // Les évènements qu'on n'a pas envisagé
      break;
    }
  }
  draw(renderer,tab,ligne,colonne,zoom);          // On redessine
  SDL_RenderPresent(renderer);
  if (!paused) {                                  // Si on n'est pas en pause
    etape_suivante(tab,ligne, colonne);             // la vie continue... 
  }
  SDL_Delay(50);                                  // Petite pause
 }

  /*********************************************************************************************************************/
  /* on referme proprement la SDL */
  end_sdl(1, "Normal ending", window, renderer);
  SDL_Quit();
  int i;
  for(i=0;i<ligne;i++){
    free(tab[i]);
  }
  free(tab);
  return 0;
}
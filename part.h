#ifndef __GARROS_HANOCQ_BOUDIN_PART_H
#define __GARROS_HANOCQ_BOUDIN_PART_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lch.h"
#include "dot_generation.c"


typedef struct part
{
    int taille;
    int * tab;
    int * haut;
}part_t;


part_t * init_part(int taille);

void free_part(part_t * maPart);

int racine_part(part_t * maPart, int i);

void fusioner_part(part_t *maPart,int i,int j);

void dot_partition(part_t * maPart,char * nom,int generation);

void afficher_partition(part_t *maPart);

ele_t * lister_classe(part_t *maPart,int x);

ele_t ** lister_partition(part_t *maPart,int *n);

#endif

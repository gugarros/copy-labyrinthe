#ifndef __GARROS_HANOCQ_BOUDIN_GRAPH_LISTE_H
#define __GARROS_HANOCQ_BOUDIN_GRAPH_LISTE_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "lchbis.h"
#include <time.h>
#include "dot_generation.c"
#include "part.h"
#include "tas.h"
#include "tasBis.h"

typedef struct graph_liste
{
    int n; //nb de noeud
    element_t * liste; //liste d'arêtes
} graph_liste_t;

graph_liste_t * init_graph_liste_alea(int n, float p);

void dot_graph_liste(graph_liste_t * monGraph,char *nom,int connexe,int generation);

part_t * composante_connexe_graph_liste(graph_liste_t * monGraph);

//void copie_graph_liste(graph_liste_t * original, graph_liste_t ** copie, ele_t * liste);

//void dot_composante_connexe_graph_liste(graph_liste_t *monGraph,int generation);

void free_graph_mat(graph_liste_t * monGraph);

part_t * kruskal(graph_liste_t * monGraph,char *nom,int generation);

#endif

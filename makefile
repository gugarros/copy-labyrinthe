#options
CFLAGS = -Wall -Wextra -g
#regle de production finale:
prog:main.c lch.o tas.o part.o graph_mat.o #labyrinth.o
	gcc main.c -o prog $(CFLAGS)
#regle de production pour chaque fichier
tas.o:tas.c tas.h
	gcc -c tas.c $(CFLAGS)
lch.o:lch.c lch.h
	gcc -c lch.c $(CFLAGS)
#file.o:file.c file.h
#	gcc -c file.c $(CFLAGS)
tas_file.o:tas_file.c tas_file.h
	gcc -c tas_file.c $(CFLAGS)
lchbis.o:lchbis.c lchbis.h
	gcc -c lchbis.c $(CFLAGS)
part.o:part.c part.h lch.o dot_generation.c
	gcc -c part.c $(CFLAGS)
graph_mat.o:graph_mat.c graph_mat.h part.o dot_generation.c
	gcc -c graph_mat.c $(CFLAGS)
graph_liste.o:graph_liste.c graph_liste.h lchbis.o part.o dot_generation.c
	gcc -c graph_liste.c $(CFLAGS)
labyrinth.o:labyrinth.c labyrinth.h lch.o
	gcc -c labyrinth.c $(CFLAGS)
clean:
	rm -f *.o
	rm -rf prog.dSYM/
serpent:
	gcc Serpent.c -o prog $(CFLAGS) -lSDL2 -lm
vie:
	gcc jdv2.c -o prog $(CFLAGS) -lSDL2
tas:main_tas_test.c tas.o
	gcc main_tas_test.c -o prog $(CFLAGS)
part:main_part_test.c part.o lch.o
	gcc main_part_test.c -o prog $(CFLAGS)
graphmat:main_graph_mat_test.c graph_mat.o part.o lch.o
	gcc main_graph_mat_test.c -o prog $(CFLAGS)
graphliste:main_graph_liste_test.c graph_liste.o part.o lchbis.o tas_file.o
	gcc main_graph_liste_test.c -o prog $(CFLAGS)
#include <SDL2/SDL.h>
#include <stdio.h>

//gcc animation.c -o animation -lSDL2

/************************************/
/*  exemple de création de fenêtres */
/************************************/

void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}                                                                 

void draw_anim(SDL_Renderer* renderer, int width1, int width2, int width3, int width4, int width5) {                                   // Je pense que vous allez faire moins laid :)
  SDL_Rect rectangle;                                             
  int width = 400;
  int height = 400;
  SDL_SetRenderDrawColor(renderer,                                
                              0, 0, 0,                               // mode Red, Green, Blue (tous dans 0..255)
                              255);                                   // 0 = transparent ; 255 = opaque
  rectangle.x = 0;                                                    // x haut gauche du rectangle
  rectangle.y = 0;                                                    // y haut gauche du rectangle
  rectangle.w = width;                                                  // sa largeur (w = width)
  rectangle.h = height;                                                  // sa hauteur (h = height)

  SDL_RenderFillRect(renderer, &rectangle);

  SDL_Rect rect1;

  SDL_SetRenderDrawColor(renderer,255,255,255,255);

  rect1.x = 0;
  rect1.y = 0;
  rect1.w = width1 % width;
  rect1.h = height/5;

  SDL_RenderFillRect(renderer, &rect1);

  SDL_Rect rect2;

  SDL_SetRenderDrawColor(renderer,255,0,0,255);

  rect2.x = 0;
  rect2.y = 1*height/5;
  rect2.w = width2 % width;
  rect2.h = height/5;

  SDL_RenderFillRect(renderer, &rect2);

  SDL_Rect rect3;

  SDL_SetRenderDrawColor(renderer,0,255,0,255);

  rect3.x = 0;
  rect3.y = 2*height/5;
  rect3.w = width3 % width;
  rect3.h = height/5;

  SDL_RenderFillRect(renderer, &rect3);

  SDL_Rect rect4;

  SDL_SetRenderDrawColor(renderer,0,0,255,255);

  rect4.x = 0;
  rect4.y = 3*height/5;
  rect4.w = width4 % width;
  rect4.h = height/5;

  SDL_RenderFillRect(renderer, &rect4);

  SDL_Rect rect5;

  SDL_SetRenderDrawColor(renderer,50,50,100,255);

  int w5 = width5;
  while (w5>400) {
      w5 = w5 - 400;
  } 

  rect5.x = 0;
  rect5.y = 4*height/5;
  rect5.w = width5 % width;
  rect5.h = height/5;

  SDL_RenderFillRect(renderer, &rect5);

//   SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
//   SDL_RenderDrawLine(renderer,
//                           0, 0,                                       // x,y du point de la première extrémité
//                           width, 400);                                  // x,y seconde extrémité


//   /* tracer un cercle n'est en fait pas trivial, voilà le résultat sans algo intelligent ... */
//   for (float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000) {      
//          SDL_SetRenderDrawColor(renderer,
//                                 (cos(angle * 2) + 1) * 255 / 2,       // quantité de Rouge      
//                                 (cos(angle * 5) + 1) * 255 / 2,       //          de vert 
//                                 (cos(angle) + 1) * 255 / 2,           //          de bleu
//                                 255);                                 // opaque
//          SDL_RenderDrawPoint(renderer, 
//                              200 + 100 * cos(angle),                  // coordonnée en x
//                              200 + 150 * sin(angle));                 //            en y   
//   }
}

int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  SDL_Window *window = NULL;               // Future fenêtre

  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
  }

  /* Création de la fenêtre */
  window = SDL_CreateWindow(
      "Fenêtre à gauche",                    // codage en utf8, donc accents possibles
      0, 0,                                  // coin haut gauche en haut gauche de l'écran
      400, 400,                              // largeur = 400, hauteur = 300
      SDL_WINDOW_RESIZABLE);                 // redimensionnable

  if (window == NULL) {
    SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
    SDL_Quit();
    exit(EXIT_FAILURE);
  }

  /* Création du renderer */
  SDL_Renderer* renderer;
  renderer = SDL_CreateRenderer(
           window, -1, 0);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

  /*********************************************************************************************************************/
  /*                                     On dessine dans le renderer                                                   */
  int i;
  for (i=0; i<=2200; i++){
  
    draw_anim(renderer,i,i/2,2*i,3*i,i/3);                                                     // appel de la fonction qui crée l'image  
    SDL_RenderPresent(renderer);                                        // affichage

    SDL_Delay(5);                           // Pause exprimée  en ms
  }
  /*********************************************************************************************************************/
  /* on referme proprement la SDL */
  end_sdl(1, "Normal ending", window, renderer);

  SDL_Quit();
  return 0;
}
